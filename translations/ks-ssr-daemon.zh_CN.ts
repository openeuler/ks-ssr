<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AccessControlPage</name>
    <message>
        <location filename="../src/ui/tool-box/access-control/access-control-page.ui" line="14"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_access-control-page.h" line="105"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/access-control/access-control-page.ui" line="46"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_access-control-page.h" line="106"/>
        <source>Access permissions prevent users from illegally operating system resources, engance system security and integrity.</source>
        <translation>访问权限 防止用户对系统资源进行非法操作，增强系统安全性和完整性</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/access-control/access-control-page.ui" line="98"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_access-control-page.h" line="107"/>
        <source>selinux swich(Reboot after restart)</source>
        <translation>selinux开关（重启后生效）</translation>
    </message>
    <message>
        <source>swich(Reboot after restart)</source>
        <translation type="vanished">开关 (重启后生效)</translation>
    </message>
    <message>
        <source>swich(After activation, the selinux effect)</source>
        <translation type="vanished">开关（开启后三权生效）</translation>
    </message>
</context>
<context>
    <name>Activation</name>
    <message>
        <location filename="../src/ui/license/activation.ui" line="26"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_activation.h" line="166"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/license/activation.ui" line="74"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_activation.h" line="167"/>
        <source>Expired time</source>
        <translation>质保期</translation>
    </message>
    <message>
        <location filename="../src/ui/license/activation.ui" line="105"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_activation.h" line="168"/>
        <source>Machine code</source>
        <translation>机器码</translation>
    </message>
    <message>
        <location filename="../src/ui/license/activation.ui" line="135"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_activation.h" line="169"/>
        <source>Activation code</source>
        <translation>激活码</translation>
    </message>
    <message>
        <location filename="../src/ui/license/activation.ui" line="206"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_activation.h" line="170"/>
        <source>Activate</source>
        <translation>激活</translation>
    </message>
    <message>
        <location filename="../src/ui/license/activation.ui" line="225"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_activation.h" line="171"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>AddUserDialog</name>
    <message>
        <location filename="../src/ui/tool-box/file-sign/add-user-dialog.ui" line="14"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_add-user-dialog.h" line="107"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-sign/add-user-dialog.ui" line="68"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_add-user-dialog.h" line="108"/>
        <source>please input user name(Separated by semicolons):</source>
        <translation>请输入用户名（分号分隔）：</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-sign/add-user-dialog.ui" line="112"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_add-user-dialog.h" line="109"/>
        <source>ok</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-sign/add-user-dialog.ui" line="131"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_add-user-dialog.h" line="110"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>BaselineReinforcement</name>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.ui" line="14"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_baseline-reinforcement.h" line="226"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>Reinforcement strategy:</source>
        <translation type="vanished">加固策略：</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.ui" line="25"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_baseline-reinforcement.h" line="227"/>
        <source>Custom strategy:</source>
        <translation>自定义策略：</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.ui" line="43"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_baseline-reinforcement.h" line="228"/>
        <source>Import strategy</source>
        <translation>导入策略</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.ui" line="56"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_baseline-reinforcement.h" line="229"/>
        <source>Export strategy</source>
        <translation>导出策略</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.ui" line="66"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_baseline-reinforcement.h" line="230"/>
        <source>Reset reinforce args</source>
        <translation>重置加固配置</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.ui" line="92"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_baseline-reinforcement.h" line="231"/>
        <source>Scheduled scan time (hours/time, 0 indicates off):</source>
        <translation>定时扫描时间（小时/次， 0表示关闭）：</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.ui" line="106"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_baseline-reinforcement.h" line="232"/>
        <source>Resource monitor:</source>
        <translation>资源监控：</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.ui" line="122"/>
        <location filename="../src/ui/settings/baseline-reinforcement.ui" line="162"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_baseline-reinforcement.h" line="234"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_baseline-reinforcement.h" line="238"/>
        <source>open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.ui" line="132"/>
        <location filename="../src/ui/settings/baseline-reinforcement.ui" line="172"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_baseline-reinforcement.h" line="235"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_baseline-reinforcement.h" line="239"/>
        <source>close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.ui" line="146"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_baseline-reinforcement.h" line="236"/>
        <source>Bubble notification</source>
        <translation>气泡通知：</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.ui" line="186"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_baseline-reinforcement.h" line="240"/>
        <source>Reinforcement fallback:</source>
        <translation>加固回退：</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.ui" line="204"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_baseline-reinforcement.h" line="241"/>
        <source>Fallback to initial state</source>
        <translation>回退到初始状态</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.ui" line="214"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_baseline-reinforcement.h" line="242"/>
        <source>Fallback to the pre reforce status</source>
        <translation>回退到加固前状态</translation>
    </message>
    <message>
        <source>Fallback to previous time</source>
        <translation type="vanished">回退到上一次</translation>
    </message>
</context>
<context>
    <name>BoxCreation</name>
    <message>
        <location filename="../src/ui/private-box/box-creation.ui" line="14"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_box-creation.h" line="199"/>
        <source>Create security box</source>
        <translation>创建保险箱</translation>
    </message>
    <message>
        <location filename="../src/ui/private-box/box-creation.ui" line="74"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_box-creation.h" line="200"/>
        <source>Name:</source>
        <translation>名称：</translation>
    </message>
    <message>
        <location filename="../src/ui/private-box/box-creation.ui" line="100"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_box-creation.h" line="201"/>
        <source>Password:</source>
        <translation>密码：</translation>
    </message>
    <message>
        <location filename="../src/ui/private-box/box-creation.ui" line="113"/>
        <location filename="../src/ui/private-box/box-creation.ui" line="170"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_box-creation.h" line="203"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_box-creation.h" line="208"/>
        <source>The password must contain two types of lowercase letters, uppercase letters, numbers, and special characters, with a length of 8-16.</source>
        <translation>密码必须包含大写字母、小写字母、数字、特殊字符中的其中两种，且长度为8-16位。</translation>
    </message>
    <message>
        <location filename="../src/ui/private-box/box-creation.ui" line="157"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_box-creation.h" line="206"/>
        <source>Confirm password:</source>
        <translation>确认密码：</translation>
    </message>
    <message>
        <location filename="../src/ui/private-box/box-creation.ui" line="234"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_box-creation.h" line="211"/>
        <source>OK</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../src/ui/private-box/box-creation.ui" line="253"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_box-creation.h" line="212"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>BoxPage</name>
    <message>
        <location filename="../src/ui/private-box/box-page.ui" line="14"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_box-page.h" line="115"/>
        <source>Box manager</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/private-box/box-page.ui" line="71"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_box-page.h" line="116"/>
        <source>Provides data isolation, hiding and encryption protection functions</source>
        <translation>提供数据隔离、隐藏和加密保护功能</translation>
    </message>
    <message>
        <location filename="../src/ui/private-box/box-page.ui" line="129"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_box-page.h" line="117"/>
        <source>New</source>
        <translation>新建</translation>
    </message>
</context>
<context>
    <name>BoxPasswordChecked</name>
    <message>
        <location filename="../src/ui/private-box/box-password-checked.ui" line="14"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_box-password-checked.h" line="107"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/private-box/box-password-checked.ui" line="68"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_box-password-checked.h" line="108"/>
        <source>please input password:</source>
        <translation>请输入密码：</translation>
    </message>
    <message>
        <location filename="../src/ui/private-box/box-password-checked.ui" line="112"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_box-password-checked.h" line="109"/>
        <source>ok</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../src/ui/private-box/box-password-checked.ui" line="131"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_box-password-checked.h" line="110"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>BoxPasswordModification</name>
    <message>
        <source>Modify password</source>
        <translation type="vanished">修改密码</translation>
    </message>
    <message>
        <source>Current password:</source>
        <translation type="vanished">当前密码：</translation>
    </message>
    <message>
        <source>New password:       </source>
        <translation type="vanished">新密码：</translation>
    </message>
    <message>
        <source>Confirm password:</source>
        <translation type="vanished">确认密码：</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">确认</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
</context>
<context>
    <name>BoxPasswordRetrieve</name>
    <message>
        <location filename="../src/ui/private-box/box-password-retrieve.ui" line="14"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_box-password-retrieve.h" line="114"/>
        <source>Create security box</source>
        <translation>创建保险箱</translation>
    </message>
    <message>
        <location filename="../src/ui/private-box/box-password-retrieve.ui" line="62"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_box-password-retrieve.h" line="115"/>
        <source>Please input passphrase:</source>
        <translation>请输入口令：</translation>
    </message>
    <message>
        <location filename="../src/ui/private-box/box-password-retrieve.ui" line="108"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_box-password-retrieve.h" line="116"/>
        <source>OK</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../src/ui/private-box/box-password-retrieve.ui" line="127"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_box-password-retrieve.h" line="117"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>CustomArgs</name>
    <message>
        <source>reset</source>
        <translation type="vanished">重置</translation>
    </message>
    <message>
        <source>ok</source>
        <translation type="vanished">确认</translation>
    </message>
</context>
<context>
    <name>DeleteNotify</name>
    <message>
        <source>ok</source>
        <translation type="obsolete">确认</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
</context>
<context>
    <name>DeviceList</name>
    <message>
        <source>Please enter keyword search</source>
        <translation type="vanished">请输入关键字搜索</translation>
    </message>
</context>
<context>
    <name>DeviceListPage</name>
    <message>
        <location filename="../src/ui/dm/device-list-page.ui" line="14"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_device-list-page.h" line="102"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-list-page.ui" line="105"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_device-list-page.h" line="105"/>
        <source>Please enter keyword search</source>
        <translation>请输入关键字搜索</translation>
    </message>
</context>
<context>
    <name>DeviceLog</name>
    <message>
        <source>Please enter keyword search</source>
        <translation type="vanished">请输入关键字搜索</translation>
    </message>
</context>
<context>
    <name>DeviceLogPage</name>
    <message>
        <location filename="../src/ui/dm/device-log-page.ui" line="14"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_device-log-page.h" line="102"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-log-page.ui" line="105"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_device-log-page.h" line="105"/>
        <source>Please enter keyword search</source>
        <translation>请输入关键字搜索</translation>
    </message>
</context>
<context>
    <name>DevicePermission</name>
    <message>
        <location filename="../src/ui/dm/device-permission.ui" line="32"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_device-permission.h" line="168"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-permission.ui" line="76"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_device-permission.h" line="169"/>
        <source>Device status:</source>
        <translation>设备状态：</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-permission.ui" line="122"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_device-permission.h" line="170"/>
        <source>Permission control:</source>
        <translation>权限控制：</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-permission.ui" line="141"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_device-permission.h" line="171"/>
        <source>read</source>
        <translation>读</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-permission.ui" line="160"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_device-permission.h" line="172"/>
        <source>write</source>
        <translation>写</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-permission.ui" line="179"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_device-permission.h" line="173"/>
        <source>exec</source>
        <translation>执行</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-permission.ui" line="235"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_device-permission.h" line="174"/>
        <source>Confirm</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-permission.ui" line="254"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_device-permission.h" line="175"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../src/ui/settings/dialog.ui" line="26"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_dialog.h" line="71"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ExecuteProtected</name>
    <message>
        <source>System core component integrity protection,protection and implementation environment safety</source>
        <translation type="vanished">系统核心组件的完整性保护，保护执行环境安全</translation>
    </message>
    <message>
        <source>Please enter keyword search</source>
        <translation type="vanished">请输入关键字搜索</translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="vanished">添加</translation>
    </message>
    <message>
        <source>Recertification</source>
        <translation type="vanished">重新认证</translation>
    </message>
    <message>
        <source>Unprotect</source>
        <translation type="vanished">移除</translation>
    </message>
</context>
<context>
    <name>ExecuteProtectedPage</name>
    <message>
        <location filename="../src/ui/tp/execute-protected-page.ui" line="14"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_execute-protected-page.h" line="143"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/tp/execute-protected-page.ui" line="46"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_execute-protected-page.h" line="144"/>
        <source>System core component integrity protection,protection and implementation environment safety</source>
        <translation>系统可执行文件的完整性校验，保护执行环境安全</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/execute-protected-page.ui" line="66"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_execute-protected-page.h" line="145"/>
        <source>0 records in total</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/tp/execute-protected-page.ui" line="111"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_execute-protected-page.h" line="146"/>
        <source>Please enter keyword search</source>
        <translation>请输入关键字搜索</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/execute-protected-page.ui" line="143"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_execute-protected-page.h" line="147"/>
        <source>Add</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/execute-protected-page.ui" line="162"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_execute-protected-page.h" line="148"/>
        <source>Recertification</source>
        <translation>重新认证</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/execute-protected-page.ui" line="181"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_execute-protected-page.h" line="149"/>
        <source>Unprotect</source>
        <translation>移除</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/execute-protected-page.ui" line="197"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_execute-protected-page.h" line="150"/>
        <source>...</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>FPPage</name>
    <message>
        <source>Protect critical file paths from malicious tampering and deletion</source>
        <translation type="vanished">保护关键文件路径，防止恶意篡改、删除</translation>
    </message>
    <message>
        <source>Please enter keyword search</source>
        <translation type="vanished">请输入关键字搜索</translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="vanished">添加</translation>
    </message>
    <message>
        <source>Unprotect</source>
        <translation type="vanished">移除</translation>
    </message>
</context>
<context>
    <name>FileProtection</name>
    <message>
        <source>Protect critical file paths from malicious tampering and deletion</source>
        <translation type="vanished">保护关键文件路径，防止恶意篡改、删除</translation>
    </message>
    <message>
        <source>Please enter keyword search</source>
        <translation type="vanished">请输入关键字搜索</translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="vanished">添加</translation>
    </message>
    <message>
        <source>Unprotect</source>
        <translation type="vanished">移除</translation>
    </message>
</context>
<context>
    <name>FileProtectionPage</name>
    <message>
        <location filename="../src/ui/fp/file-protection-page.ui" line="14"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_file-protection-page.h" line="125"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/fp/file-protection-page.ui" line="46"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_file-protection-page.h" line="126"/>
        <source>Protect critical file paths from malicious tampering and deletion</source>
        <translation>保护关键文件，防止恶意篡改、删除</translation>
    </message>
    <message>
        <location filename="../src/ui/fp/file-protection-page.ui" line="66"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_file-protection-page.h" line="127"/>
        <source>0 records in total</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/fp/file-protection-page.ui" line="111"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_file-protection-page.h" line="128"/>
        <source>Please enter keyword search</source>
        <translation>请输入关键字搜索</translation>
    </message>
    <message>
        <location filename="../src/ui/fp/file-protection-page.ui" line="143"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_file-protection-page.h" line="129"/>
        <source>Add</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="../src/ui/fp/file-protection-page.ui" line="162"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_file-protection-page.h" line="130"/>
        <source>Unprotect</source>
        <translation>解除保护</translation>
    </message>
</context>
<context>
    <name>FileShredPage</name>
    <message>
        <location filename="../src/ui/tool-box/file-shred/file-shred-page.ui" line="14"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_file-shred-page.h" line="134"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-shred/file-shred-page.ui" line="46"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_file-shred-page.h" line="135"/>
        <source>Completely crushing and unable to delete stubborn files and dirs</source>
        <translation>彻底粉碎无法删除的顽固文件及目录</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-shred/file-shred-page.ui" line="66"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_file-shred-page.h" line="136"/>
        <source>0 records in total</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-shred/file-shred-page.ui" line="111"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_file-shred-page.h" line="137"/>
        <source>Please enter keyword search</source>
        <translation>请输入关键字搜索</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-shred/file-shred-page.ui" line="143"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_file-shred-page.h" line="138"/>
        <source>add</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-shred/file-shred-page.ui" line="162"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_file-shred-page.h" line="139"/>
        <source>remove</source>
        <translation>移除</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-shred/file-shred-page.ui" line="181"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_file-shred-page.h" line="140"/>
        <source>shred</source>
        <translation>粉碎</translation>
    </message>
</context>
<context>
    <name>FileSignPage</name>
    <message>
        <location filename="../src/ui/tool-box/file-sign/file-sign-page.ui" line="14"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_file-sign-page.h" line="143"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>View file context information and integrity labels</source>
        <translation type="vanished">查看文件上下文信息和完整性标签</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-sign/file-sign-page.ui" line="46"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_file-sign-page.h" line="144"/>
        <source>View context information and integrity labels</source>
        <translation>查看上下文信息和完整性标签</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-sign/file-sign-page.ui" line="66"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_file-sign-page.h" line="145"/>
        <source>0 records in total</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-sign/file-sign-page.ui" line="111"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_file-sign-page.h" line="146"/>
        <source>Please enter keyword search</source>
        <translation>请输入关键字搜索</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-sign/file-sign-page.ui" line="143"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_file-sign-page.h" line="147"/>
        <source>select users</source>
        <translation>选择用户</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-sign/file-sign-page.ui" line="162"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_file-sign-page.h" line="148"/>
        <source>select files</source>
        <translation>选择文件</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-sign/file-sign-page.ui" line="181"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_file-sign-page.h" line="149"/>
        <source>clean</source>
        <translation>清除</translation>
    </message>
    <message>
        <source>select</source>
        <translation type="vanished">选择文件</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-sign/file-sign-page.ui" line="197"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_file-sign-page.h" line="150"/>
        <source>...</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Home</name>
    <message>
        <location filename="../src/ui/br/home.ui" line="14"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_home.h" line="161"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/br/home.ui" line="69"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_home.h" line="163"/>
        <source>Security reinforcement is protecting your computer</source>
        <translation>安全加固正在保护您的电脑</translation>
    </message>
    <message>
        <location filename="../src/ui/br/home.ui" line="76"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_home.h" line="164"/>
        <source>KylinSec Host Security Reinforcement Software Detects Risks in Advance to Ensure Asset Security</source>
        <translation>麒麟信安主机安全加固软件提前发现风险，保障资产安全</translation>
    </message>
    <message>
        <location filename="../src/ui/br/home.ui" line="85"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_home.h" line="165"/>
        <source>Latest reinforcement time:</source>
        <translation>上次加固时间：</translation>
    </message>
    <message>
        <location filename="../src/ui/br/home.ui" line="92"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_home.h" line="166"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/br/home.ui" line="164"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_home.h" line="168"/>
        <source>Quick Scan</source>
        <translation>快速扫描</translation>
    </message>
</context>
<context>
    <name>IdentityAuthentication</name>
    <message>
        <location filename="../src/ui/settings/identity-authentication.ui" line="14"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_identity-authentication.h" line="97"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/settings/identity-authentication.ui" line="31"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_identity-authentication.h" line="98"/>
        <source>Two-factor authentication(Reboot after restart)</source>
        <translation>双因子认证（重启后生效）</translation>
    </message>
    <message>
        <source>Two-factor authentication</source>
        <translation type="vanished">双因子认证</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/identity-authentication.ui" line="68"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_identity-authentication.h" line="100"/>
        <source>unique identification</source>
        <translation>唯一性标识</translation>
    </message>
</context>
<context>
    <name>KS::About</name>
    <message>
        <location filename="../src/ui/about.cpp" line="44"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../src/ui/about.cpp" line="50"/>
        <source>KylinSec Security reinforcement sofware V1</source>
        <translation>麒麟信安主机安全加固软件V1</translation>
    </message>
    <message>
        <location filename="../src/ui/about.cpp" line="51"/>
        <source>Version : V1.2</source>
        <translation>版本号：V1.2</translation>
    </message>
    <message>
        <source>Security reinforcement sofware V1.2</source>
        <translation type="vanished">主机安全加固软件V1.2</translation>
    </message>
    <message>
        <source>Security reinforcement sofware V1.0</source>
        <translation type="vanished">安全加固软件V1.0</translation>
    </message>
</context>
<context>
    <name>KS::Account::Login</name>
    <message>
        <source>Login</source>
        <translation type="vanished">登入</translation>
    </message>
    <message>
        <location filename="../src/ui/account/login.cpp" line="80"/>
        <source>Security reinforcement</source>
        <translation>主机安全加固</translation>
    </message>
    <message>
        <location filename="../src/ui/account/login.cpp" line="89"/>
        <source>Unactivated</source>
        <translation>未激活</translation>
    </message>
</context>
<context>
    <name>KS::Account::Manager</name>
    <message>
        <location filename="../src/ui/account/manager.cpp" line="88"/>
        <source>User is not login.</source>
        <translation>用户未登录。</translation>
    </message>
    <message>
        <location filename="../src/ui/account/manager.cpp" line="148"/>
        <source>Currently using the initial password to login, please change it as soon as possible!</source>
        <translation>当前使用初始密码登录，请尽快修改密码！</translation>
    </message>
    <message>
        <location filename="../src/daemon/account/manager.cpp" line="135"/>
        <source>Enable uid reuse</source>
        <translation>启用 Uid 唯一性标识功能， Uid 不可复用。</translation>
    </message>
    <message>
        <location filename="../src/daemon/account/manager.cpp" line="135"/>
        <source>Disable uid reuse</source>
        <translation>禁用 Uid 唯一性标识功能， Uid 可复用。</translation>
    </message>
    <message>
        <location filename="../src/daemon/account/manager.cpp" line="322"/>
        <source>Enable Multi-Factor Authentication</source>
        <translation>启用多因子认证功能。</translation>
    </message>
    <message>
        <location filename="../src/daemon/account/manager.cpp" line="322"/>
        <source>Disable Multi-Factor Authentication</source>
        <translation>关闭多因子认证功能。</translation>
    </message>
    <message>
        <location filename="../src/daemon/account/manager.cpp" line="419"/>
        <source>Failed to change %1&apos;s passphrase, unique name: %2, actor role: %3</source>
        <translation>修改 %1的密码失败， 标识名为%2，用户权限为%3</translation>
    </message>
    <message>
        <location filename="../src/daemon/account/manager.cpp" line="429"/>
        <location filename="../src/daemon/account/manager.cpp" line="445"/>
        <source>Change password</source>
        <translation>修改密码失败</translation>
    </message>
    <message>
        <location filename="../src/daemon/account/manager.cpp" line="434"/>
        <source>Change password failed.</source>
        <translation>修改密码失败。</translation>
    </message>
    <message>
        <location filename="../src/daemon/account/manager.cpp" line="473"/>
        <source>Failed to login, because this account has been freeze</source>
        <translation>登录失败，此用户已被冻结</translation>
    </message>
    <message>
        <location filename="../src/daemon/account/manager.cpp" line="482"/>
        <source>Failed to login, Passwd error</source>
        <translation>登录失败，密码错误</translation>
    </message>
    <message>
        <location filename="../src/daemon/account/manager.cpp" line="491"/>
        <source>Login</source>
        <translation>登录</translation>
    </message>
    <message>
        <location filename="../src/daemon/account/manager.cpp" line="513"/>
        <source>Logout</source>
        <translation>注销</translation>
    </message>
</context>
<context>
    <name>KS::Activation</name>
    <message>
        <source>Activation</source>
        <translation type="vanished">软件激活</translation>
    </message>
    <message>
        <source>Activate app successful!</source>
        <translation type="vanished">成功激活应用程序！</translation>
    </message>
    <message>
        <source>Scan QR code to get %1</source>
        <translation type="vanished">扫描获取%1</translation>
    </message>
</context>
<context>
    <name>KS::Activation::Activation</name>
    <message>
        <location filename="../src/ui/license/activation.cpp" line="66"/>
        <source>Activation</source>
        <translation>软件激活</translation>
    </message>
    <message>
        <location filename="../src/ui/license/activation.cpp" line="106"/>
        <source>Activate app successful!</source>
        <translation>成功激活应用程序！</translation>
    </message>
    <message>
        <location filename="../src/ui/license/activation.cpp" line="117"/>
        <source>Scan QR code to get %1</source>
        <translation>扫描获取%1</translation>
    </message>
</context>
<context>
    <name>KS::Activation::QRCodeDialog</name>
    <message>
        <location filename="../src/ui/license/qrcode-dialog.cpp" line="38"/>
        <source>QR code</source>
        <translation>二维码</translation>
    </message>
</context>
<context>
    <name>KS::BOX::Box</name>
    <message>
        <source>Lock</source>
        <translation type="vanished">上锁</translation>
    </message>
    <message>
        <source>Unlock</source>
        <translation type="vanished">解锁</translation>
    </message>
    <message>
        <source>Modify password</source>
        <translation type="vanished">修改密码</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">删除</translation>
    </message>
    <message>
        <source>Retrieve the password</source>
        <translation type="vanished">找回密码</translation>
    </message>
    <message>
        <source>Please confirm whether the password is consistent.</source>
        <translation type="vanished">请确认两次密码是否一致。</translation>
    </message>
    <message>
        <source>The input cannot be empty, please improve the information.</source>
        <translation type="vanished">输入不能为空，请输入正确的信息。</translation>
    </message>
    <message>
        <source>Del box</source>
        <translation type="vanished">删除保险箱</translation>
    </message>
    <message>
        <source>Retrieve password</source>
        <translation type="vanished">找回密码</translation>
    </message>
    <message>
        <source>Modify success!</source>
        <translation type="vanished">修改成功！</translation>
    </message>
    <message>
        <source>Your box password is %1</source>
        <translation type="vanished">您的保险箱密码为：%1</translation>
    </message>
    <message>
        <source>Unlock success!</source>
        <translation type="vanished">解锁成功！</translation>
    </message>
    <message>
        <source>Delete success!</source>
        <translation type="vanished">删除成功！</translation>
    </message>
</context>
<context>
    <name>KS::BOX::BoxPage</name>
    <message>
        <source>Private box</source>
        <translation type="vanished">私密保险箱</translation>
    </message>
    <message>
        <source>Create box</source>
        <translation type="vanished">创建保险箱</translation>
    </message>
    <message>
        <source>Please confirm whether the password is consistent.</source>
        <translation type="vanished">请确认两次密码是否一致。</translation>
    </message>
    <message>
        <source>The input cannot be empty, please improve the information.</source>
        <translation type="vanished">输入不能为空，请输入正确的信息。</translation>
    </message>
    <message>
        <source>Please remember this box passphrase : %1, Can be used to retrieve passwords.</source>
        <translation type="vanished">请记住您的保险箱口令：%1，可用于找回密码。</translation>
    </message>
</context>
<context>
    <name>KS::BR::ArgHandle</name>
    <message>
        <location filename="../src/ui/br/reinforcement-items/arghandle.cpp" line="159"/>
        <location filename="../src/ui/br/reinforcement-items/arghandle.cpp" line="273"/>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reinforcement-items/arghandle.cpp" line="160"/>
        <location filename="../src/ui/br/reinforcement-items/arghandle.cpp" line="274"/>
        <source>No</source>
        <translation>否</translation>
    </message>
</context>
<context>
    <name>KS::BR::BRPage</name>
    <message>
        <location filename="../src/ui/br/br-page.cpp" line="35"/>
        <source>Baseline reinforcement</source>
        <translation>基线加固</translation>
    </message>
    <message>
        <location filename="../src/ui/br/br-page.cpp" line="61"/>
        <source>Reset success!</source>
        <translation>重置成功！</translation>
    </message>
</context>
<context>
    <name>KS::BR::Home</name>
    <message>
        <location filename="../src/ui/br/home.cpp" line="52"/>
        <location filename="../src/ui/br/home.cpp" line="61"/>
        <source>Custom scan</source>
        <translation>自定义扫描</translation>
    </message>
    <message>
        <location filename="../src/ui/br/home.cpp" line="54"/>
        <location filename="../src/ui/br/home.cpp" line="70"/>
        <source>System strategy</source>
        <translation>系统策略</translation>
    </message>
    <message>
        <location filename="../src/ui/br/home.cpp" line="54"/>
        <location filename="../src/ui/br/home.cpp" line="66"/>
        <source>Custom strategy</source>
        <translation>自定义策略</translation>
    </message>
    <message>
        <location filename="../src/ui/br/home.cpp" line="52"/>
        <location filename="../src/ui/br/home.cpp" line="61"/>
        <source>Quick scan</source>
        <translation>快速扫描</translation>
    </message>
</context>
<context>
    <name>KS::BR::ItemTable</name>
    <message>
        <location filename="../src/ui/br/item-table.cpp" line="86"/>
        <source>Reinforcement Item</source>
        <translation>加固项</translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="vanished">说明</translation>
    </message>
    <message>
        <location filename="../src/ui/br/item-table.cpp" line="87"/>
        <source>Info(Double click this column to modify the reinforcement parameters)</source>
        <translation>说明（双击修改加固项参数）</translation>
    </message>
    <message>
        <location filename="../src/ui/br/item-table.cpp" line="88"/>
        <source>State</source>
        <translation>状态</translation>
    </message>
    <message>
        <location filename="../src/ui/br/item-table.cpp" line="516"/>
        <source>Double click this column to modify the reinforcement parameters</source>
        <translation>双击此列修改加固参数</translation>
    </message>
</context>
<context>
    <name>KS::BR::Plugins::ArgHandle</name>
    <message>
        <source>Yes</source>
        <translation type="vanished">是</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="vanished">否</translation>
    </message>
</context>
<context>
    <name>KS::BR::Plugins::CustomArgs</name>
    <message>
        <source>Set reinforment args</source>
        <translation type="vanished">加固参数设置</translation>
    </message>
    <message>
        <source>%1 not less than %2</source>
        <translation type="vanished">%1不小于%2</translation>
    </message>
    <message>
        <source>%1 input format is incorrect, please input the correct content according to the prompt</source>
        <translation type="vanished">%1输入格式不正确，请根据提示输入正确的内容</translation>
    </message>
</context>
<context>
    <name>KS::BR::Progress</name>
    <message>
        <location filename="../src/ui/br/progress.cpp" line="45"/>
        <source>Security reinforcement is protecting your computer</source>
        <translation>安全加固正在保护您的电脑</translation>
    </message>
    <message>
        <location filename="../src/ui/br/progress.cpp" line="46"/>
        <source>KylinSec Host Security Reinforcement Software Detects Risks in Advance to Ensure Asset Security</source>
        <translation>麒麟信安主机安全加固软件提前发现风险，保障资产安全</translation>
    </message>
    <message>
        <location filename="../src/ui/br/progress.cpp" line="48"/>
        <source>Strat scan</source>
        <translation>开始扫描</translation>
    </message>
    <message>
        <location filename="../src/ui/br/progress.cpp" line="52"/>
        <location filename="../src/ui/br/progress.cpp" line="148"/>
        <location filename="../src/ui/br/progress.cpp" line="156"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../src/ui/br/progress.cpp" line="83"/>
        <source>In %1, please wait...</source>
        <translation>正在%1中，请等待...</translation>
    </message>
    <message>
        <location filename="../src/ui/br/progress.cpp" line="85"/>
        <source>Scan</source>
        <translation>扫描</translation>
    </message>
    <message>
        <location filename="../src/ui/br/progress.cpp" line="86"/>
        <location filename="../src/ui/br/progress.cpp" line="147"/>
        <source>Reinforcement</source>
        <translation>加固</translation>
    </message>
    <message>
        <location filename="../src/ui/br/progress.cpp" line="87"/>
        <source>Start time: %1 elapsed time: 00:00:00 progress: 0%</source>
        <translation>开始扫描时间：%1 用时：00:00:00 进度： 0%</translation>
    </message>
    <message>
        <location filename="../src/ui/br/progress.cpp" line="91"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../src/ui/br/progress.cpp" line="104"/>
        <location filename="../src/ui/br/progress.cpp" line="179"/>
        <source>Start time: %1 elapsed time: %2 progress: %3%</source>
        <translation>开始扫描时间：%1 用时：%2 进度：%3%</translation>
    </message>
    <message>
        <location filename="../src/ui/br/progress.cpp" line="120"/>
        <source>Scanned %1, %2 conform!</source>
        <translation>扫描完成%1项，%2项符合！</translation>
    </message>
    <message>
        <location filename="../src/ui/br/progress.cpp" line="121"/>
        <source>Reinforcement completed %1, successfully reinforced %2!</source>
        <translation>加固完成%1项，加固成功%2项！</translation>
    </message>
    <message>
        <location filename="../src/ui/br/progress.cpp" line="128"/>
        <source>Scanned %1, %2 conform, %3 inconform!</source>
        <translation>扫描完成%1项，%2项符合，%3项不符合！</translation>
    </message>
    <message>
        <location filename="../src/ui/br/progress.cpp" line="132"/>
        <source>Reinforcement completed %1, successfully reinforced %2, failed %3!</source>
        <translation>加固完成%1项，加固成功%2项，失败%3项！</translation>
    </message>
    <message>
        <location filename="../src/ui/br/progress.cpp" line="155"/>
        <source>GenerateReport</source>
        <translation>生成报表</translation>
    </message>
    <message>
        <location filename="../src/ui/br/progress.cpp" line="164"/>
        <source>Start time: %1 elapsed time: %2</source>
        <translation>开始时间：%1 用时：%2</translation>
    </message>
</context>
<context>
    <name>KS::BR::ReinforcementArgsDialog</name>
    <message>
        <location filename="../src/ui/br/reinforcement-items/reinforcement-args-dialog.cpp" line="70"/>
        <source>Set reinforment args</source>
        <translation>加固参数设置</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reinforcement-items/reinforcement-args-dialog.cpp" line="139"/>
        <source>%1 not less than %2</source>
        <translation>%1不小于%2</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reinforcement-items/reinforcement-args-dialog.cpp" line="143"/>
        <source>%1 input format is incorrect, please input the correct content according to the prompt</source>
        <translation>%1输入格式不正确，请根据提示输入正确的内容</translation>
    </message>
</context>
<context>
    <name>KS::BR::Reports::Result</name>
    <message>
        <source>Unknown</source>
        <translation type="vanished">未知</translation>
    </message>
    <message>
        <source>Conformity</source>
        <translation type="vanished">符合</translation>
    </message>
    <message>
        <source>Inconformity</source>
        <translation type="vanished">不符合</translation>
    </message>
    <message>
        <source>Not Scanned</source>
        <translation type="vanished">未扫描</translation>
    </message>
    <message>
        <source>Scannig</source>
        <translation type="vanished">正在扫描...</translation>
    </message>
    <message>
        <source>Scan Failed</source>
        <translation type="vanished">扫描失败</translation>
    </message>
    <message>
        <source>Scan Complete</source>
        <translation type="vanished">扫描完成</translation>
    </message>
    <message>
        <source>Unreinforcement</source>
        <translation type="vanished">未加固</translation>
    </message>
    <message>
        <source>Reinforcing</source>
        <translation type="vanished">正在加固...</translation>
    </message>
    <message>
        <source>Reinforcement Failure</source>
        <translation type="vanished">加固失败</translation>
    </message>
    <message>
        <source>Reinforced</source>
        <translation type="vanished">加固完成</translation>
    </message>
    <message>
        <source>No master file</source>
        <translation type="vanished">无属主文件</translation>
    </message>
    <message>
        <source>Files with 777 permissions</source>
        <translation type="vanished">文件具有777权限</translation>
    </message>
    <message>
        <source>Files with sgid permission</source>
        <translation type="vanished">文件具有SGID权限</translation>
    </message>
    <message>
        <source>Files with suid permission</source>
        <translation type="vanished">文件具有SUID权限</translation>
    </message>
    <message>
        <source>Unactivated</source>
        <translation type="vanished">未激活</translation>
    </message>
    <message>
        <source>Activated</source>
        <translation type="vanished">已激活</translation>
    </message>
    <message>
        <source>Vulnerability exists in version %1</source>
        <translation type="vanished">版本号%1存在漏洞</translation>
    </message>
    <message>
        <source>KylinSecHostReinforcementReport_%1_%2.pdf</source>
        <translation type="vanished">麒麟信安主机安全加固报告_%1_%2.pdf</translation>
    </message>
    <message>
        <source>Open File</source>
        <translation type="vanished">打开文件</translation>
    </message>
</context>
<context>
    <name>KS::BR::Reports::RoundProgressBar</name>
    <message>
        <source>Total: %1 conform: %2 inconform: %3</source>
        <translation type="vanished">总计：%1 符合：%2 不符合：%3</translation>
    </message>
</context>
<context>
    <name>KS::BR::Reports::Table</name>
    <message>
        <source>Scan Item</source>
        <translation type="vanished">扫描项</translation>
    </message>
    <message>
        <source>Scan Type</source>
        <translation type="vanished">扫描类型</translation>
    </message>
    <message>
        <source>Remarks</source>
        <translation type="vanished">备注</translation>
    </message>
    <message>
        <source>Scan rpm name</source>
        <translation type="vanished">rpm包名</translation>
    </message>
    <message>
        <source>Scan results</source>
        <translation type="vanished">扫描结果</translation>
    </message>
    <message>
        <source>Test Item</source>
        <translation type="vanished">检测项</translation>
    </message>
    <message>
        <source>Before reinforcement result</source>
        <translation type="vanished">加固前</translation>
    </message>
    <message>
        <source>After reinforcement result</source>
        <translation type="vanished">加固后</translation>
    </message>
    <message>
        <source>Technical support：Hunan KylinSec Technology Co. Ltd.,  Telephone：400-012-6606</source>
        <translation type="vanished">技术支持：湖南麒麟信安科技股份有限公司，联系电话：400-012-6606</translation>
    </message>
</context>
<context>
    <name>KS::BR::Result</name>
    <message>
        <location filename="../src/ui/br/reports/result.cpp" line="79"/>
        <location filename="../src/ui/br/reports/result.cpp" line="117"/>
        <source>Unknown</source>
        <translation>未知</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/result.cpp" line="82"/>
        <location filename="../src/ui/br/reports/result.cpp" line="113"/>
        <source>Conformity</source>
        <translation>符合</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/result.cpp" line="85"/>
        <location filename="../src/ui/br/reports/result.cpp" line="115"/>
        <source>Inconformity</source>
        <translation>不符合</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/result.cpp" line="88"/>
        <source>Not Scanned</source>
        <translation>未扫描</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/result.cpp" line="91"/>
        <source>Scannig</source>
        <translation>正在扫描...</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/result.cpp" line="94"/>
        <source>Scan Failed</source>
        <translation>扫描失败</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/result.cpp" line="97"/>
        <source>Scan Complete</source>
        <translation>扫描完成</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/result.cpp" line="100"/>
        <source>Unreinforcement</source>
        <translation>未加固</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/result.cpp" line="103"/>
        <source>Reinforcing</source>
        <translation>正在加固...</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/result.cpp" line="106"/>
        <source>Reinforcement Failure</source>
        <translation>加固失败</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/result.cpp" line="109"/>
        <source>Reinforced</source>
        <translation>加固成功</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/result.cpp" line="188"/>
        <source>No master file</source>
        <translation>无属主文件</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/result.cpp" line="194"/>
        <source>Files with 777 permissions</source>
        <translation>文件具有777权限</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/result.cpp" line="205"/>
        <source>Files with sgid permission</source>
        <translation>文件具有sgid权限</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/result.cpp" line="207"/>
        <source>Files with suid permission</source>
        <translation>文件具有suid权限</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/result.cpp" line="342"/>
        <source>Unactivated</source>
        <translation>未激活</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/result.cpp" line="342"/>
        <source>Activated</source>
        <translation>已激活</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/result.cpp" line="471"/>
        <source>Vulnerability exists in version %1</source>
        <translation>版本号%1存在漏洞</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/result.cpp" line="535"/>
        <source>KylinSecHostReinforcementReport_%1_%2.pdf</source>
        <translation>麒麟信安主机安全加固报告_%1_%2.pdf</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/result.cpp" line="536"/>
        <source>Open File</source>
        <translation>打开文件</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/result.cpp" line="536"/>
        <source>PDF(*.pdf)</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>KS::BR::RoundProgressBar</name>
    <message>
        <location filename="../src/ui/br/reports/round-progressbar.cpp" line="119"/>
        <source>Total: %1 conform: %2 inconform: %3</source>
        <translation>总计：%1 符合：%2 不符合：%3</translation>
    </message>
</context>
<context>
    <name>KS::BR::Scan</name>
    <message>
        <location filename="../src/ui/br/scan.cpp" line="138"/>
        <source>Files</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../src/ui/br/scan.cpp" line="138"/>
        <source>strategy(*.xml)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/br/scan.cpp" line="147"/>
        <source>Please check the file name and whether you have write permission!</source>
        <translation>请检查文件名以及您是否具有写入权限！</translation>
    </message>
    <message>
        <location filename="../src/ui/br/scan.cpp" line="157"/>
        <source>Open RA file failed!</source>
        <translation>打开内部文件失败！</translation>
    </message>
    <message>
        <location filename="../src/ui/br/scan.cpp" line="164"/>
        <source>Export successed!</source>
        <translation>导出成功！</translation>
    </message>
    <message>
        <location filename="../src/ui/br/scan.cpp" line="164"/>
        <source>Export failed!</source>
        <translation>导出失败！</translation>
    </message>
    <message>
        <location filename="../src/ui/br/scan.cpp" line="345"/>
        <source>Please select the item to export!</source>
        <translation>请选择要导出的加固项！</translation>
    </message>
    <message>
        <location filename="../src/ui/br/scan.cpp" line="395"/>
        <location filename="../src/ui/br/scan.cpp" line="434"/>
        <location filename="../src/ui/br/scan.cpp" line="456"/>
        <source>Fallback is in progress, please wait.</source>
        <translation>回退正在进行中，请等待。</translation>
    </message>
    <message>
        <location filename="../src/ui/br/scan.cpp" line="413"/>
        <source>Please check the reinforcement items to be scanned or reinforcement classification for scanning.</source>
        <translation>请勾选需要扫描的加固项。</translation>
    </message>
    <message>
        <location filename="../src/ui/br/scan.cpp" line="428"/>
        <source>Please check the content to be reinforced.</source>
        <translation>请勾选要加固的项。</translation>
    </message>
    <message>
        <location filename="../src/ui/br/scan.cpp" line="485"/>
        <source>Export succeeded!</source>
        <translation>导出成功！</translation>
    </message>
</context>
<context>
    <name>KS::BR::Table</name>
    <message>
        <location filename="../src/ui/br/reports/table.cpp" line="37"/>
        <source>Scan Item</source>
        <translation>扫描项</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/table.cpp" line="39"/>
        <source>Scan Type</source>
        <translation>扫描类型</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/table.cpp" line="41"/>
        <location filename="../src/ui/br/reports/table.cpp" line="51"/>
        <location filename="../src/ui/br/reports/table.cpp" line="61"/>
        <source>Remarks</source>
        <translation>备注</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/table.cpp" line="47"/>
        <source>Scan rpm name</source>
        <translation>rpm包名</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/table.cpp" line="49"/>
        <source>Scan results</source>
        <translation>扫描结果</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/table.cpp" line="57"/>
        <source>Test Item</source>
        <translation>检测项</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/table.cpp" line="58"/>
        <source>Before reinforcement result</source>
        <translation>加固前</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/table.cpp" line="60"/>
        <source>After reinforcement result</source>
        <translation>加固后</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/table.cpp" line="64"/>
        <source>Technical support：Hunan KylinSec Technology Co. Ltd.,  Telephone：400-012-6606</source>
        <translation>技术支持：湖南麒麟信安科技股份有限公司，联系电话：400-012-6606</translation>
    </message>
</context>
<context>
    <name>KS::BRDaemon::BRDBus</name>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="147"/>
        <source>Failed to set standard type to %1, invalid args</source>
        <translation>设置加固标准为%1标准失败，非法参数</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="148"/>
        <location filename="../src/daemon/br/br-dbus.cpp" line="160"/>
        <location filename="../src/daemon/br/br-dbus.cpp" line="167"/>
        <location filename="../src/daemon/br/br-dbus.cpp" line="204"/>
        <location filename="../src/daemon/br/br-dbus.cpp" line="219"/>
        <source>system</source>
        <translation>系统</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="148"/>
        <location filename="../src/daemon/br/br-dbus.cpp" line="160"/>
        <location filename="../src/daemon/br/br-dbus.cpp" line="167"/>
        <location filename="../src/daemon/br/br-dbus.cpp" line="204"/>
        <location filename="../src/daemon/br/br-dbus.cpp" line="219"/>
        <source>custom</source>
        <translation>自定义</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="159"/>
        <source>Failed to set standard type to %1, Internal error</source>
        <translation>设置加固标准为%1标准失败，内部错误</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="166"/>
        <source>Set standard type to %1</source>
        <translation>设置加固标准为%1标准</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="180"/>
        <source>Failed to import custom reinforce standard.</source>
        <translation>导入自定义加固标准失败。</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="185"/>
        <source>Import custom reinforce standard. encoded standard: %1</source>
        <translation>导入自定义加固标准成功。</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="196"/>
        <location filename="../src/daemon/br/br-dbus.cpp" line="213"/>
        <source>Failed to set strategy type</source>
        <translation>设置加固策略失败</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="203"/>
        <location filename="../src/daemon/br/br-dbus.cpp" line="218"/>
        <source>Set strategy type to %1</source>
        <translation>设置加固策略为%1策略</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="230"/>
        <location filename="../src/daemon/br/br-dbus.cpp" line="243"/>
        <source>Set time scan to %1</source>
        <translation>设置定时扫描时间为%1小时</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="238"/>
        <source>Failed to set time scan</source>
        <translation>设置定时扫描时间失败</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="256"/>
        <location filename="../src/daemon/br/br-dbus.cpp" line="273"/>
        <source>Failed to set notification status</source>
        <translation>设置通知状态失败</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="263"/>
        <location filename="../src/daemon/br/br-dbus.cpp" line="279"/>
        <source>Set notification status to %1</source>
        <translation>%1气泡通知</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="314"/>
        <source>Failed to import custom reinforce strategy. error msg: </source>
        <translation>导入加固策略失败，错误信息为：</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="324"/>
        <source>Failed to import custom reinforce strategy.</source>
        <translation>导入自定义加固策略失败。</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="329"/>
        <source>Import custom reinforce strategy.</source>
        <translation>导入加固策略。</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="345"/>
        <location filename="../src/daemon/br/br-dbus.cpp" line="362"/>
        <source>Failed to set resource monitor switch</source>
        <translation>设置资源监控开关失败</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="353"/>
        <location filename="../src/daemon/br/br-dbus.cpp" line="379"/>
        <source>Set resource monitor switch to %1</source>
        <translation>%1资源监控开关</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="459"/>
        <source>Reset all reinforcement parameters.</source>
        <translation>重置所有加固参数。</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="513"/>
        <source>Reset reinforcement parameters. name is %1</source>
        <translation>重置%1的加固参数</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="527"/>
        <location filename="../src/daemon/br/br-dbus.cpp" line="546"/>
        <location filename="../src/daemon/br/br-dbus.cpp" line="558"/>
        <location filename="../src/daemon/br/br-dbus.cpp" line="588"/>
        <location filename="../src/daemon/br/br-dbus.cpp" line="602"/>
        <source>Failed to scan.</source>
        <translation>启动扫描任务失败。</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="623"/>
        <location filename="../src/daemon/br/br-dbus.cpp" line="641"/>
        <location filename="../src/daemon/br/br-dbus.cpp" line="653"/>
        <location filename="../src/daemon/br/br-dbus.cpp" line="692"/>
        <source>Failed to reinforcement.</source>
        <translation>启动加固任务失败。</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="804"/>
        <source>init</source>
        <translation>初始状态</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="804"/>
        <source>pre</source>
        <translation>加固前状态</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="829"/>
        <source>Scan success %1, safe %2, unsafe %3.</source>
        <translation>扫描完成%1项，%2项符合，%3项不符合。</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="856"/>
        <source>Reinforcement fail %1</source>
        <translation>加固失败%1项</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="863"/>
        <source>Reinforcement success %1, fail %2</source>
        <translation>加固成功 %1项，失败 %2项</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="870"/>
        <source>Reinforcement success %1</source>
        <translation>加固成功%1项</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="732"/>
        <source>Failed to cancel progress.</source>
        <translation>忽略进程失败。</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="263"/>
        <location filename="../src/daemon/br/br-dbus.cpp" line="279"/>
        <location filename="../src/daemon/br/br-dbus.cpp" line="353"/>
        <location filename="../src/daemon/br/br-dbus.cpp" line="379"/>
        <source>open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="263"/>
        <location filename="../src/daemon/br/br-dbus.cpp" line="279"/>
        <location filename="../src/daemon/br/br-dbus.cpp" line="353"/>
        <location filename="../src/daemon/br/br-dbus.cpp" line="379"/>
        <source>close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="738"/>
        <source>Cancel. job id: %1</source>
        <translation>忽略进程，运行id为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="746"/>
        <source>export strategy</source>
        <translation>导出策略</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="754"/>
        <source>export report</source>
        <translation>导出报告</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="784"/>
        <location filename="../src/daemon/br/br-dbus.cpp" line="796"/>
        <source>Failed to set fallback.</source>
        <translation>回退失败。</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/br-dbus.cpp" line="804"/>
        <source>Set fallback. snapshot status is %1</source>
        <translation>回退至%1</translation>
    </message>
</context>
<context>
    <name>KS::Box</name>
    <message>
        <source>Lock</source>
        <translation type="vanished">上锁</translation>
    </message>
    <message>
        <source>Unlock</source>
        <translation type="vanished">解锁</translation>
    </message>
    <message>
        <source>Modify password</source>
        <translation type="vanished">修改密码</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">删除</translation>
    </message>
    <message>
        <source>Retrieve the password</source>
        <translation type="vanished">找回密码</translation>
    </message>
    <message>
        <source>Please confirm whether the password is consistent.</source>
        <translation type="vanished">请确认两次密码是否一致。</translation>
    </message>
    <message>
        <source>The input cannot be empty, please improve the information.</source>
        <translation type="vanished">输入不能为空，请输入正确的信息。</translation>
    </message>
    <message>
        <source>Del box</source>
        <translation type="vanished">删除保险箱</translation>
    </message>
    <message>
        <source>Retrieve password</source>
        <translation type="vanished">找回密码</translation>
    </message>
    <message>
        <source>Modify success!</source>
        <translation type="vanished">修改成功！</translation>
    </message>
    <message>
        <source>Your box password is %1</source>
        <translation type="vanished">您的保险箱密码为：%1</translation>
    </message>
    <message>
        <source>Unlock success!</source>
        <translation type="vanished">解锁成功！</translation>
    </message>
    <message>
        <source>Delete success!</source>
        <translation type="vanished">删除成功！</translation>
    </message>
</context>
<context>
    <name>KS::Box::Box</name>
    <message>
        <source>Lock</source>
        <translation type="vanished">上锁</translation>
    </message>
    <message>
        <source>Unlock</source>
        <translation type="vanished">解锁</translation>
    </message>
    <message>
        <source>Modify password</source>
        <translation type="vanished">修改密码</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">删除</translation>
    </message>
    <message>
        <source>Retrieve the password</source>
        <translation type="vanished">找回密码</translation>
    </message>
    <message>
        <source>Please confirm whether the password is consistent.</source>
        <translation type="vanished">请确认两次密码是否一致。</translation>
    </message>
    <message>
        <source>The input cannot be empty, please improve the information.</source>
        <translation type="vanished">输入不能为空，请输入正确的信息。</translation>
    </message>
    <message>
        <source>Del box</source>
        <translation type="vanished">删除保险箱</translation>
    </message>
    <message>
        <source>Retrieve password</source>
        <translation type="vanished">找回密码</translation>
    </message>
    <message>
        <source>Modify success!</source>
        <translation type="vanished">修改成功！</translation>
    </message>
    <message>
        <source>Your box password is %1</source>
        <translation type="vanished">您的保险箱密码为：%1</translation>
    </message>
    <message>
        <source>Unlock success!</source>
        <translation type="vanished">解锁成功！</translation>
    </message>
    <message>
        <source>Delete success!</source>
        <translation type="vanished">删除成功！</translation>
    </message>
</context>
<context>
    <name>KS::Box::BoxPage</name>
    <message>
        <source>Private box</source>
        <translation type="vanished">私密保险箱</translation>
    </message>
    <message>
        <source>Create box</source>
        <translation type="vanished">创建保险箱</translation>
    </message>
    <message>
        <source>Please confirm whether the password is consistent.</source>
        <translation type="vanished">请确认两次密码是否一致。</translation>
    </message>
    <message>
        <source>The input cannot be empty, please improve the information.</source>
        <translation type="vanished">输入不能为空，请输入正确的信息。</translation>
    </message>
    <message>
        <source>Please remember this box passphrase : %1, Can be used to retrieve passwords.</source>
        <translation type="vanished">请记住您的保险箱口令：%1，可用于找回密码。</translation>
    </message>
</context>
<context>
    <name>KS::BoxPage</name>
    <message>
        <source>Create box</source>
        <translation type="vanished">创建保险箱</translation>
    </message>
    <message>
        <source>Please confirm whether the password is consistent.</source>
        <translation type="vanished">请确认两次密码是否一致。</translation>
    </message>
    <message>
        <source>The input cannot be empty, please improve the information.</source>
        <translation type="vanished">输入不能为空，请输入正确的信息。</translation>
    </message>
    <message>
        <source>Please remember this box passphrase : %1, Can be used to retrieve passwords.</source>
        <translation type="vanished">请记住您的保险箱口令：%1，可用于找回密码。</translation>
    </message>
</context>
<context>
    <name>KS::CalendarWidget</name>
    <message>
        <location filename="../src/ui/common/date-picker/calendar-widget.cpp" line="248"/>
        <source>confirm</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../src/ui/common/date-picker/calendar-widget.cpp" line="252"/>
        <source>now</source>
        <translation>现在</translation>
    </message>
    <message>
        <location filename="../src/ui/common/date-picker/calendar-widget.cpp" line="275"/>
        <source>%1year%2mouth</source>
        <translation>%1年%2月</translation>
    </message>
</context>
<context>
    <name>KS::DM::DBus</name>
    <message>
        <location filename="../src/daemon/dm/dbus.cpp" line="165"/>
        <location filename="../src/daemon/dm/dbus.cpp" line="177"/>
        <location filename="../src/daemon/dm/dbus.cpp" line="186"/>
        <source>Failed to change permissions. ID is %1</source>
        <translation>修改设备权限失败，id为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/dm/dbus.cpp" line="212"/>
        <source>Change permissions. ID is %1 permissions is %2</source>
        <translation>修改设备权限，id为%1，权限修改为%2</translation>
    </message>
    <message>
        <location filename="../src/daemon/dm/dbus.cpp" line="227"/>
        <source>Failed to enable device. ID is %1</source>
        <translation>启用设备失败，id为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/dm/dbus.cpp" line="237"/>
        <source>Enable device. ID is %1 !</source>
        <translation>启用设备，id为%1！</translation>
    </message>
    <message>
        <location filename="../src/daemon/dm/dbus.cpp" line="252"/>
        <source>Failed to disable device. ID is %1</source>
        <translation>禁用设备失败，id为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/dm/dbus.cpp" line="262"/>
        <source>Disable device. ID is %1</source>
        <translation>禁用设备，id为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/dm/dbus.cpp" line="277"/>
        <location filename="../src/daemon/dm/dbus.cpp" line="287"/>
        <source>Failed to %1 interface. type is %2</source>
        <translation>%1接口失败，类型为%2</translation>
    </message>
    <message>
        <location filename="../src/daemon/dm/dbus.cpp" line="278"/>
        <location filename="../src/daemon/dm/dbus.cpp" line="288"/>
        <location filename="../src/daemon/dm/dbus.cpp" line="299"/>
        <source>enabled</source>
        <translation>启用</translation>
    </message>
    <message>
        <location filename="../src/daemon/dm/dbus.cpp" line="278"/>
        <location filename="../src/daemon/dm/dbus.cpp" line="288"/>
        <location filename="../src/daemon/dm/dbus.cpp" line="299"/>
        <source>disabled</source>
        <translation>禁用</translation>
    </message>
    <message>
        <location filename="../src/daemon/dm/dbus.cpp" line="298"/>
        <source>%1 interface. type is %2</source>
        <translation>%1接口，类型为%2</translation>
    </message>
    <message>
        <source>Failed to enable interface. type is %1</source>
        <translation type="vanished">启用接口失败，类型为%1</translation>
    </message>
    <message>
        <source>Enable interface. type is %1</source>
        <translation type="vanished">启用接口，类型为%1</translation>
    </message>
</context>
<context>
    <name>KS::DM::DeviceList</name>
    <message>
        <source>Device List</source>
        <translation type="vanished">设备列表</translation>
    </message>
    <message>
        <source>A total of %1 records</source>
        <translation type="vanished">总共%1条记录</translation>
    </message>
    <message>
        <source>Device management</source>
        <translation type="vanished">设备管理</translation>
    </message>
</context>
<context>
    <name>KS::DM::DeviceListDelegate</name>
    <message>
        <location filename="../src/ui/dm/device-list-table.cpp" line="93"/>
        <source>Edit</source>
        <translation>编辑</translation>
    </message>
</context>
<context>
    <name>KS::DM::DeviceListPage</name>
    <message>
        <source>Device List</source>
        <translation type="vanished">设备列表</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-list-page.cpp" line="37"/>
        <source>Control peripheral permissions to prevent unauthorized device access</source>
        <translation>管理外设权限，防止未授权设备接入</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-list-page.cpp" line="74"/>
        <source>A total of %1 records</source>
        <translation>共%1条记录</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-list-page.cpp" line="80"/>
        <source>Device management</source>
        <translation>设备管理</translation>
    </message>
</context>
<context>
    <name>KS::DM::DeviceListTable</name>
    <message>
        <location filename="../src/ui/dm/device-list-table.cpp" line="289"/>
        <source>Number</source>
        <translation>序号</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-list-table.cpp" line="290"/>
        <source>Device Name</source>
        <translation>设备名称</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-list-table.cpp" line="326"/>
        <source>Device Type</source>
        <translation>设备类型</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-list-table.cpp" line="292"/>
        <source>Device Id</source>
        <translation>设备ID</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-list-table.cpp" line="293"/>
        <source>Interface</source>
        <translation>接口</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-list-table.cpp" line="328"/>
        <location filename="../src/ui/dm/device-list-table.cpp" line="341"/>
        <source>Storage</source>
        <translation>存储设备</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-list-table.cpp" line="329"/>
        <location filename="../src/ui/dm/device-list-table.cpp" line="341"/>
        <source>CD</source>
        <translation>CD</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-list-table.cpp" line="330"/>
        <location filename="../src/ui/dm/device-list-table.cpp" line="341"/>
        <source>Mouse</source>
        <translation>鼠标</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-list-table.cpp" line="331"/>
        <location filename="../src/ui/dm/device-list-table.cpp" line="341"/>
        <source>Keyboard</source>
        <translation>键盘</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-list-table.cpp" line="332"/>
        <location filename="../src/ui/dm/device-list-table.cpp" line="341"/>
        <source>Network card</source>
        <translation>网卡</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-list-table.cpp" line="333"/>
        <location filename="../src/ui/dm/device-list-table.cpp" line="341"/>
        <source>Wireless network card</source>
        <translation>无线网卡</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-list-table.cpp" line="334"/>
        <location filename="../src/ui/dm/device-list-table.cpp" line="341"/>
        <source>Video</source>
        <translation>视频</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-list-table.cpp" line="335"/>
        <location filename="../src/ui/dm/device-list-table.cpp" line="342"/>
        <source>Audio</source>
        <translation>音频</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-list-table.cpp" line="336"/>
        <location filename="../src/ui/dm/device-list-table.cpp" line="342"/>
        <source>Printer</source>
        <translation>打印机</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-list-table.cpp" line="337"/>
        <location filename="../src/ui/dm/device-list-table.cpp" line="342"/>
        <source>Hub</source>
        <translation>集线器</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-list-table.cpp" line="338"/>
        <location filename="../src/ui/dm/device-list-table.cpp" line="342"/>
        <source>Communications</source>
        <translation>通信设备类</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-list-table.cpp" line="339"/>
        <location filename="../src/ui/dm/device-list-table.cpp" line="343"/>
        <source>Bluetooth</source>
        <translation>蓝牙</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-list-table.cpp" line="340"/>
        <location filename="../src/ui/dm/device-list-table.cpp" line="343"/>
        <source>Other</source>
        <translation>其它</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-list-table.cpp" line="501"/>
        <source>Unknown device</source>
        <translation>未知设备</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="vanished">未知</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-list-table.cpp" line="366"/>
        <source>Status</source>
        <translation>设备状态</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-list-table.cpp" line="295"/>
        <source>Permission</source>
        <translation>权限</translation>
    </message>
</context>
<context>
    <name>KS::DM::DeviceLog</name>
    <message>
        <source>Device Log</source>
        <translation type="vanished">设备日志</translation>
    </message>
    <message>
        <source>A total of %1 records</source>
        <translation type="vanished">总共%1条记录</translation>
    </message>
    <message>
        <source>Device management</source>
        <translation type="vanished">设备管理</translation>
    </message>
</context>
<context>
    <name>KS::DM::DeviceLogPage</name>
    <message>
        <location filename="../src/ui/dm/device-log-page.cpp" line="35"/>
        <location filename="../src/ui/dm/device-log-page.cpp" line="87"/>
        <source>Device Log</source>
        <translation>设备日志</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-log-page.cpp" line="76"/>
        <source>A total of %1 records</source>
        <translation>共%1条记录</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-log-page.cpp" line="82"/>
        <source>Device management</source>
        <translation>设备管理</translation>
    </message>
</context>
<context>
    <name>KS::DM::DeviceLogTable</name>
    <message>
        <location filename="../src/ui/dm/device-log-table.cpp" line="144"/>
        <source>Device Name</source>
        <translation>设备名称</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-log-table.cpp" line="145"/>
        <source>Device Type</source>
        <translation>设备类型</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-log-table.cpp" line="146"/>
        <source>Time</source>
        <translation>时间</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-log-table.cpp" line="147"/>
        <source>Device Status</source>
        <translation>状态</translation>
    </message>
</context>
<context>
    <name>KS::DM::DeviceManager</name>
    <message>
        <location filename="../src/daemon/dm/device-manager.cpp" line="274"/>
        <source>Device access, name is %1, type is %2</source>
        <translation>接入设备，设备名为%1,类型为%2</translation>
    </message>
    <message>
        <location filename="../src/daemon/dm/device-manager.cpp" line="275"/>
        <source>Unknown device</source>
        <translation>未知设备</translation>
    </message>
    <message>
        <location filename="../src/daemon/dm/device-manager.cpp" line="361"/>
        <source>Other</source>
        <translation>其它</translation>
    </message>
    <message>
        <source>CD</source>
        <translation type="vanished">CD</translation>
    </message>
    <message>
        <location filename="../src/daemon/dm/device-manager.cpp" line="373"/>
        <source>Mouse</source>
        <translation>鼠标</translation>
    </message>
    <message>
        <location filename="../src/daemon/dm/device-manager.cpp" line="371"/>
        <source>Keyboard</source>
        <translation>键盘</translation>
    </message>
    <message>
        <location filename="../src/daemon/dm/device-manager.cpp" line="363"/>
        <source>USB</source>
        <translation>USB</translation>
    </message>
    <message>
        <location filename="../src/daemon/dm/device-manager.cpp" line="365"/>
        <source>Bluetooth</source>
        <translation>蓝牙</translation>
    </message>
    <message>
        <location filename="../src/daemon/dm/device-manager.cpp" line="367"/>
        <source>Network card</source>
        <translation>网卡</translation>
    </message>
    <message>
        <location filename="../src/daemon/dm/device-manager.cpp" line="369"/>
        <source>HDMI</source>
        <translation>HDMI</translation>
    </message>
    <message>
        <source>Wireless network card</source>
        <translation type="vanished">无线网卡</translation>
    </message>
    <message>
        <source>Video</source>
        <translation type="vanished">视频</translation>
    </message>
</context>
<context>
    <name>KS::DM::DevicePermission</name>
    <message>
        <location filename="../src/ui/dm/device-permission.cpp" line="43"/>
        <source>enable</source>
        <translation>启用</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-permission.cpp" line="44"/>
        <source>disable</source>
        <translation>禁用</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-permission.cpp" line="76"/>
        <source>Please select device status</source>
        <translation>请选择设备状态</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/device-permission.cpp" line="150"/>
        <source>Please select at least one permission.</source>
        <translation>请至少选择一种权限。</translation>
    </message>
</context>
<context>
    <name>KS::DM::Utils</name>
    <message>
        <location filename="../src/ui/dm/utils.cpp" line="25"/>
        <source>Storage</source>
        <translation>存储设备</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/utils.cpp" line="27"/>
        <source>CD</source>
        <translation>CD</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/utils.cpp" line="29"/>
        <location filename="../src/ui/dm/utils.cpp" line="69"/>
        <source>Mouse</source>
        <translation>鼠标</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/utils.cpp" line="31"/>
        <location filename="../src/ui/dm/utils.cpp" line="71"/>
        <source>Keyboard</source>
        <translation>键盘</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/utils.cpp" line="33"/>
        <source>Network card</source>
        <translation>网卡</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/utils.cpp" line="35"/>
        <source>Wireless network card</source>
        <translation>无线网卡</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/utils.cpp" line="37"/>
        <source>Video</source>
        <translation>视频</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/utils.cpp" line="39"/>
        <source>Audio</source>
        <translation>音频</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/utils.cpp" line="41"/>
        <source>Printer</source>
        <translation>打印机</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/utils.cpp" line="43"/>
        <source>Hub</source>
        <translation>集线器</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/utils.cpp" line="45"/>
        <source>Communications</source>
        <translation>通信设备类</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/utils.cpp" line="47"/>
        <location filename="../src/ui/dm/utils.cpp" line="63"/>
        <source>Bluetooth</source>
        <translation>蓝牙</translation>
    </message>
    <message>
        <source>OTHER</source>
        <translation type="vanished">其他</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="vanished">未知</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/utils.cpp" line="61"/>
        <source>USB</source>
        <translation>USB</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/utils.cpp" line="49"/>
        <location filename="../src/ui/dm/utils.cpp" line="73"/>
        <source>Other</source>
        <translation>其它</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/utils.cpp" line="65"/>
        <source>Network</source>
        <translation>网络</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/utils.cpp" line="67"/>
        <source>HDMI</source>
        <translation>HDMI</translation>
    </message>
</context>
<context>
    <name>KS::DeviceControl</name>
    <message>
        <source> (Effective after restart)</source>
        <translation type="vanished"> (重启后生效)</translation>
    </message>
    <message>
        <source>Are you sure to close the %1 interface</source>
        <translation type="vanished">是否要关闭%1接口？</translation>
    </message>
</context>
<context>
    <name>KS::DeviceList</name>
    <message>
        <source>Device List</source>
        <translation type="vanished">设备列表</translation>
    </message>
    <message>
        <source>A total of %1 records</source>
        <translation type="vanished">总共%1条记录</translation>
    </message>
</context>
<context>
    <name>KS::DeviceListDelegate</name>
    <message>
        <source>Edit</source>
        <translation type="vanished">编辑</translation>
    </message>
</context>
<context>
    <name>KS::DeviceListTable</name>
    <message>
        <source>Number</source>
        <translation type="vanished">序号</translation>
    </message>
    <message>
        <source>Device Name</source>
        <translation type="vanished">设备名称</translation>
    </message>
    <message>
        <source>Device Type</source>
        <translation type="vanished">设备类型</translation>
    </message>
    <message>
        <source>Device Id</source>
        <translation type="vanished">设备ID</translation>
    </message>
    <message>
        <source>Interface</source>
        <translation type="vanished">接口</translation>
    </message>
    <message>
        <source>Status</source>
        <translation type="vanished">当前状态</translation>
    </message>
    <message>
        <source>Permission</source>
        <translation type="vanished">权限</translation>
    </message>
</context>
<context>
    <name>KS::DeviceLog</name>
    <message>
        <source>Device Log</source>
        <translation type="vanished">设备日志</translation>
    </message>
    <message>
        <source>A total of %1 records</source>
        <translation type="vanished">总共%1条记录</translation>
    </message>
</context>
<context>
    <name>KS::DeviceLogTable</name>
    <message>
        <source>Device Name</source>
        <translation type="vanished">设备名称</translation>
    </message>
    <message>
        <source>Device Type</source>
        <translation type="vanished">设备类型</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">时间</translation>
    </message>
    <message>
        <source>Device Status</source>
        <translation type="vanished">状态</translation>
    </message>
</context>
<context>
    <name>KS::DevicePage</name>
    <message>
        <source>Device List</source>
        <translation type="vanished">设备列表</translation>
    </message>
    <message>
        <source>Device Log</source>
        <translation type="vanished">设备日志</translation>
    </message>
</context>
<context>
    <name>KS::DevicePermission</name>
    <message>
        <source>enable</source>
        <translation type="vanished">启用</translation>
    </message>
    <message>
        <source>disable</source>
        <translation type="vanished">禁用</translation>
    </message>
    <message>
        <source>Please select device status</source>
        <translation type="vanished">请选择设备状态</translation>
    </message>
    <message>
        <source>Please select at least one permission.</source>
        <translation type="vanished">请至少选择一种权限。</translation>
    </message>
</context>
<context>
    <name>KS::DeviceUtils</name>
    <message>
        <source>Storage</source>
        <translation type="vanished">存储设备</translation>
    </message>
    <message>
        <source>CD</source>
        <translation type="vanished">CD</translation>
    </message>
    <message>
        <source>Mouse</source>
        <translation type="vanished">鼠标</translation>
    </message>
    <message>
        <source>Keyboard</source>
        <translation type="vanished">键盘</translation>
    </message>
    <message>
        <source>Network card</source>
        <translation type="vanished">网卡</translation>
    </message>
    <message>
        <source>Wireless network card</source>
        <translation type="vanished">无线网卡</translation>
    </message>
    <message>
        <source>Video</source>
        <translation type="vanished">视频</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation type="vanished">音频</translation>
    </message>
    <message>
        <source>Printer</source>
        <translation type="vanished">打印机</translation>
    </message>
    <message>
        <source>Hub</source>
        <translation type="vanished">集线器</translation>
    </message>
    <message>
        <source>Communications</source>
        <translation type="vanished">通信设备类</translation>
    </message>
    <message>
        <source>Bluetooth</source>
        <translation type="vanished">蓝牙</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="vanished">未知</translation>
    </message>
    <message>
        <source>USB</source>
        <translation type="vanished">USB</translation>
    </message>
    <message>
        <source>Network</source>
        <translation type="vanished">网络</translation>
    </message>
    <message>
        <source>HDMI</source>
        <translation type="vanished">HDMI</translation>
    </message>
</context>
<context>
    <name>KS::Dialog</name>
    <message>
        <source>Settings</source>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <source>Trusted protect</source>
        <translation type="vanished">可信保护</translation>
    </message>
    <message>
        <source>Interface Control</source>
        <translation type="vanished">接口控制</translation>
    </message>
</context>
<context>
    <name>KS::ExecuteProtected</name>
    <message>
        <source>A total of %1 records, Being tampered with %2</source>
        <translation type="vanished">总共%1条记录，被篡改%2条</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation type="vanished">打开文件</translation>
    </message>
    <message>
        <source>Please select the content that needs to be removed.</source>
        <translation type="vanished">请选择要删除的内容。</translation>
    </message>
</context>
<context>
    <name>KS::ExecuteProtectedModel</name>
    <message>
        <source>Certified</source>
        <translation type="vanished">已认证</translation>
    </message>
    <message>
        <source>Number</source>
        <translation type="vanished">序号</translation>
    </message>
    <message>
        <source>File path</source>
        <translation type="vanished">文件路径</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">文件类型</translation>
    </message>
    <message>
        <source>Status</source>
        <translation type="vanished">当前状态</translation>
    </message>
</context>
<context>
    <name>KS::ExecuteProtectedTable</name>
    <message>
        <source>Certified</source>
        <translation type="vanished">已认证</translation>
    </message>
</context>
<context>
    <name>KS::FP::FileProtection</name>
    <message>
        <source>A total of %1 records</source>
        <translation type="vanished">总共%1条记录</translation>
    </message>
    <message>
        <source>File protected</source>
        <translation type="vanished">文件保护</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation type="vanished">打开文件</translation>
    </message>
    <message>
        <source>Trusted data needs to be initialised,please wait a few minutes before trying.</source>
        <translation type="vanished">可信数据需要初始化，请等待几分钟后尝试。</translation>
    </message>
    <message>
        <source>Please select the content that needs to be removed.</source>
        <translation type="vanished">请选择要删除的内容。</translation>
    </message>
</context>
<context>
    <name>KS::FP::FileProtectionPage</name>
    <message>
        <location filename="../src/ui/fp/file-protection-page.cpp" line="42"/>
        <location filename="../src/ui/fp/file-protection-page.cpp" line="166"/>
        <source>A total of %1 records</source>
        <translation>共%1条记录</translation>
    </message>
    <message>
        <location filename="../src/ui/fp/file-protection-page.cpp" line="68"/>
        <source>File protected</source>
        <translation>文件保护</translation>
    </message>
    <message>
        <location filename="../src/ui/fp/file-protection-page.cpp" line="94"/>
        <source>Open file</source>
        <translation>打开文件</translation>
    </message>
    <message>
        <location filename="../src/ui/fp/file-protection-page.cpp" line="108"/>
        <source>Trusted data needs to be initialised,please wait a few minutes before trying.</source>
        <translation>可信数据需要初始化，请等待几分钟后尝试。</translation>
    </message>
    <message>
        <location filename="../src/ui/fp/file-protection-page.cpp" line="133"/>
        <source>Please select the content that needs to be removed.</source>
        <translation>请选择要删除的内容。</translation>
    </message>
    <message>
        <location filename="../src/ui/fp/file-protection-page.cpp" line="138"/>
        <source>Remove protection</source>
        <translation>解除保护</translation>
    </message>
    <message>
        <location filename="../src/ui/fp/file-protection-page.cpp" line="138"/>
        <source>The removal operation is irreversible.Do you confirm the removal of the selected record from the whitelist?</source>
        <translation>移除操作不可逆，您确认要从白名单中移除记录吗？</translation>
    </message>
</context>
<context>
    <name>KS::FP::FilesModel</name>
    <message>
        <location filename="../src/ui/fp/file-table.cpp" line="243"/>
        <source>Number</source>
        <translation>序号</translation>
    </message>
    <message>
        <location filename="../src/ui/fp/file-table.cpp" line="245"/>
        <source>File name</source>
        <translation>文件名</translation>
    </message>
    <message>
        <location filename="../src/ui/fp/file-table.cpp" line="247"/>
        <source>File path</source>
        <translation>文件路径</translation>
    </message>
    <message>
        <location filename="../src/ui/fp/file-table.cpp" line="249"/>
        <source>Add time</source>
        <translation>添加时间</translation>
    </message>
</context>
<context>
    <name>KS::FPPage</name>
    <message>
        <source>A total of %1 records</source>
        <translation type="vanished">总共%1条记录</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation type="vanished">打开文件</translation>
    </message>
    <message>
        <source>Trusted data needs to be initialised,please wait a few minutes before trying.</source>
        <translation type="vanished">可信数据需要初始化，请等待几分钟后尝试。</translation>
    </message>
    <message>
        <source>Please select the content that needs to be removed.</source>
        <translation type="vanished">请选择要删除的内容。</translation>
    </message>
</context>
<context>
    <name>KS::FilesModel</name>
    <message>
        <source>Number</source>
        <translation type="vanished">序号</translation>
    </message>
    <message>
        <source>File name</source>
        <translation type="vanished">文件名</translation>
    </message>
    <message>
        <source>File path</source>
        <translation type="vanished">文件路径</translation>
    </message>
    <message>
        <source>Add time</source>
        <translation type="vanished">添加时间</translation>
    </message>
</context>
<context>
    <name>KS::KSS::DBus</name>
    <message>
        <source>Failed to add kernel files.</source>
        <translation type="vanished">添加内核文件失败。</translation>
    </message>
    <message>
        <source>Failed to add execute files.</source>
        <translation type="vanished">添加执行文件失败。</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="333"/>
        <location filename="../src/daemon/kss/dbus.cpp" line="382"/>
        <source> file path is %1</source>
        <translation>文件路径为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="289"/>
        <source>Add kernel files successed. files path is %1</source>
        <translation>添加内核文件成功，文件路径为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="260"/>
        <location filename="../src/daemon/kss/dbus.cpp" line="274"/>
        <location filename="../src/daemon/kss/dbus.cpp" line="282"/>
        <source>Failed to add kernel files,</source>
        <translation>添加内核文件失败，</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="260"/>
        <location filename="../src/daemon/kss/dbus.cpp" line="274"/>
        <location filename="../src/daemon/kss/dbus.cpp" line="282"/>
        <source>Failed to add execute files,</source>
        <translation>添加执行文件失败，</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="260"/>
        <location filename="../src/daemon/kss/dbus.cpp" line="372"/>
        <source> file path is empty.</source>
        <translation> 文件路径为空。</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="274"/>
        <location filename="../src/daemon/kss/dbus.cpp" line="282"/>
        <location filename="../src/daemon/kss/dbus.cpp" line="317"/>
        <location filename="../src/daemon/kss/dbus.cpp" line="325"/>
        <source>Internal error!</source>
        <translation>内部错误！</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="289"/>
        <source>Add execute files successed. files path is %1</source>
        <translation>添加执行文件成功，文件路径为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="304"/>
        <location filename="../src/daemon/kss/dbus.cpp" line="317"/>
        <source>Failed to add kernel file list,</source>
        <translation>添加内核文件列表失败，</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="304"/>
        <location filename="../src/daemon/kss/dbus.cpp" line="317"/>
        <source>Failed to add execute file list,</source>
        <translation>添加执行文件列表失败，</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="304"/>
        <location filename="../src/daemon/kss/dbus.cpp" line="348"/>
        <source>file path is empty.</source>
        <translation>文件路径为空。</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="325"/>
        <source>Failed to add kernel file list.</source>
        <translation>添加内核文件列表失败。</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="325"/>
        <source>Failed to add execute file list.</source>
        <translation>添加执行文件列表失败。</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="333"/>
        <source>Add kernel files successed.</source>
        <translation>添加内核文件成功。</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="333"/>
        <source>Add execute files successed.</source>
        <translation>添加执行文件成功。</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="348"/>
        <source>Failed to remove kernel file,</source>
        <translation>移除内核文件失败，</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="348"/>
        <source>Failed to remove execute file,</source>
        <translation>移除执行文件失败，</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="382"/>
        <source>Remove kernel file successed.</source>
        <translation>移除内核文件成功。</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="382"/>
        <source>Remove execute file successed.</source>
        <translation>移除执行文件成功。</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="394"/>
        <source>Failed to prohibit unloading. file path is empty</source>
        <translation>设置防卸载开关失败，文件路径为空</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="401"/>
        <source>%1 prohibit unloading. file path is %2</source>
        <translation>%1防卸载功能，文件路径为%2</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="444"/>
        <source>Failed to add files protection, file path is empty.</source>
        <translation>添加文件至保护列表失败，文件路径为空。</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="454"/>
        <source>Add files protection, file path is %1</source>
        <translation>添加文件至保护列表，路径为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="466"/>
        <location filename="../src/daemon/kss/dbus.cpp" line="487"/>
        <source>Failed to remove files protection, file path is empty.</source>
        <translation>从保护列表中移除文件失败，文件路径为空。</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="497"/>
        <source>Remove files protection, file path is %1</source>
        <translation>从保护列表中移除文件，路径为%1</translation>
    </message>
    <message>
        <source>Failed to add files protection.</source>
        <translation type="vanished">添加文件保护文件失败。</translation>
    </message>
    <message>
        <source>Add files protection.</source>
        <translation type="vanished">添加文件保护文件。</translation>
    </message>
    <message>
        <source>Failed to remove files protection.</source>
        <translation type="vanished">移除文件保护文件失败。</translation>
    </message>
    <message>
        <source>Remove files protection.</source>
        <translation type="vanished">移除文件保护文件。</translation>
    </message>
    <message>
        <source>Add kernel files successed. Sum is %1</source>
        <translation type="vanished">添加内核文件列表成功，总数为%1</translation>
    </message>
    <message>
        <source>Add execute files successed. Sum is %1</source>
        <translation type="vanished">添加执行文件列表成功，总数为%1</translation>
    </message>
    <message>
        <source>Failed to remove kernel file.</source>
        <translation type="vanished">移除内核文件失败。</translation>
    </message>
    <message>
        <source>Failed to remove execute file.</source>
        <translation type="vanished">移除执行文件失败。</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="357"/>
        <source>Remove kernel file. files path is %1</source>
        <translation>移除内核文件成功，文件路径为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="357"/>
        <source>Remove execute file. files path is %1</source>
        <translation>移除执行文件成功，文件路径为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="372"/>
        <source>Failed to remove kernel file list.</source>
        <translation>移除内核文件列表失败。</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="372"/>
        <source>Failed to remove execute file list.</source>
        <translation>移除执行文件列表失败。</translation>
    </message>
    <message>
        <source>Remove kernel file successed. Sum is %1</source>
        <translation type="vanished">移除内核文件列表成功，总数为%1</translation>
    </message>
    <message>
        <source>Remove execute file successed. Sum is %1</source>
        <translation type="vanished">移除执行文件列表成功，总数为%1</translation>
    </message>
    <message>
        <source>Failed to prohibit unloading. file path is %1</source>
        <translation type="vanished">设置防卸载开关失败。文件路径为%1</translation>
    </message>
    <message>
        <source>Prohibit unloading. file path is %1</source>
        <translation type="vanished">设置防卸载开关，路径为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="413"/>
        <source>Failed to add files protection, file path is empty</source>
        <translation>添加文件至保护列表失败，文件路径为空</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="420"/>
        <source>Failed to add files protection. file path is %1</source>
        <translation>添加文件保护失败，文件路径为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="432"/>
        <source>Add files protection. file path is %1</source>
        <translation>添加文件至保护列表，路径为%1</translation>
    </message>
    <message>
        <source>Failed to add files protection. file list size is %1</source>
        <translation type="vanished">添加文件保护列表失败，列表大小为%1项</translation>
    </message>
    <message>
        <source>Add files protection. file list size is %1</source>
        <translation type="vanished">添加文件保护列表，列表大小为%1项</translation>
    </message>
    <message>
        <source>Failed to remove files protection. file path is %1</source>
        <translation type="vanished">移除文件保护失败，文件路径为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="475"/>
        <source>Remove files protection. file path is %1</source>
        <translation>从保护列表中移除文件，路径为%1</translation>
    </message>
    <message>
        <source>Failed to remove files protection. file list size is %1</source>
        <translation type="vanished">移除文件保护列表失败，列表大小为%1项</translation>
    </message>
    <message>
        <source>Remove files protection. file list size is %1</source>
        <translation type="vanished">移除文件保护列表，列表大小为%1项</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="509"/>
        <location filename="../src/daemon/kss/dbus.cpp" line="519"/>
        <source>Failed to set storage mode.</source>
        <translation>设置存储模式失败。</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="524"/>
        <source>Set storage mode. Status is %1</source>
        <translation>设置存储模式，类型为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="524"/>
        <source>soft storage</source>
        <translation>软存储</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="524"/>
        <source>hard storage</source>
        <translation>硬存储</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="535"/>
        <source>Set trusted status is %1</source>
        <translation>%1可信开关</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="401"/>
        <location filename="../src/daemon/kss/dbus.cpp" line="535"/>
        <source>open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../src/daemon/kss/dbus.cpp" line="401"/>
        <location filename="../src/daemon/kss/dbus.cpp" line="535"/>
        <source>close</source>
        <translation>关闭</translation>
    </message>
</context>
<context>
    <name>KS::KernelProtected</name>
    <message>
        <source>A total of %1 records, Being tampered with %2</source>
        <translation type="vanished">总共%1条记录，被篡改%2条</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation type="vanished">打开文件</translation>
    </message>
    <message>
        <source>Please select the content that needs to be removed.</source>
        <translation type="vanished">请选择要删除的内容。</translation>
    </message>
</context>
<context>
    <name>KS::KernelProtectedModel</name>
    <message>
        <source>Certified</source>
        <translation type="vanished">已认证</translation>
    </message>
    <message>
        <source>Number</source>
        <translation type="vanished">序号</translation>
    </message>
    <message>
        <source>File path</source>
        <translation type="vanished">文件路径</translation>
    </message>
    <message>
        <source>Status</source>
        <translation type="vanished">当前状态</translation>
    </message>
    <message>
        <source>Prohibt unload</source>
        <translation type="vanished">防卸载</translation>
    </message>
</context>
<context>
    <name>KS::KernelProtectedTable</name>
    <message>
        <source>Certified</source>
        <translation type="vanished">已认证</translation>
    </message>
</context>
<context>
    <name>KS::Loading</name>
    <message>
        <location filename="../src/ui/common/loading.cpp" line="54"/>
        <source>The data is being initialized, please wait a moment.</source>
        <translation>数据正在初始化，请稍等...</translation>
    </message>
</context>
<context>
    <name>KS::Log::LogModel</name>
    <message>
        <location filename="../src/ui/log/table.cpp" line="173"/>
        <source>Success</source>
        <translation>成功</translation>
    </message>
    <message>
        <location filename="../src/ui/log/table.cpp" line="173"/>
        <source>Failed</source>
        <translation>失败</translation>
    </message>
    <message>
        <location filename="../src/ui/log/table.cpp" line="199"/>
        <source>Number</source>
        <translation>序号</translation>
    </message>
    <message>
        <location filename="../src/ui/log/table.cpp" line="205"/>
        <source>Data time</source>
        <translation>时间</translation>
    </message>
    <message>
        <location filename="../src/ui/log/table.cpp" line="207"/>
        <source>Message</source>
        <translation>内容</translation>
    </message>
</context>
<context>
    <name>KS::Log::LogPage</name>
    <message>
        <location filename="../src/ui/log/log-page.cpp" line="53"/>
        <source>Log audit</source>
        <translation>日志审计</translation>
    </message>
    <message>
        <location filename="../src/ui/log/log-page.cpp" line="73"/>
        <location filename="../src/ui/log/log-page.cpp" line="144"/>
        <source>A total of %1 records</source>
        <translation>共%1条记录</translation>
    </message>
</context>
<context>
    <name>KS::Log::LogTable</name>
    <message>
        <location filename="../src/ui/log/table.cpp" line="404"/>
        <source>Log type</source>
        <translation>日志类型</translation>
    </message>
    <message>
        <location filename="../src/ui/log/table.cpp" line="405"/>
        <source>Device log</source>
        <translation>设备日志</translation>
    </message>
    <message>
        <location filename="../src/ui/log/table.cpp" line="406"/>
        <source>Tool box log</source>
        <translation>工具箱日志</translation>
    </message>
    <message>
        <location filename="../src/ui/log/table.cpp" line="407"/>
        <source>Baseline reinforcement log</source>
        <translation>基线加固日志</translation>
    </message>
    <message>
        <location filename="../src/ui/log/table.cpp" line="408"/>
        <source>Trusted protection log</source>
        <translation>可信保护日志</translation>
    </message>
    <message>
        <location filename="../src/ui/log/table.cpp" line="409"/>
        <source>Files protection log</source>
        <translation>文件保护日志</translation>
    </message>
    <message>
        <location filename="../src/ui/log/table.cpp" line="410"/>
        <source>Private box log</source>
        <translation>保险箱日志</translation>
    </message>
    <message>
        <location filename="../src/ui/log/table.cpp" line="411"/>
        <source>Account log</source>
        <translation>用户日志</translation>
    </message>
    <message>
        <location filename="../src/ui/log/table.cpp" line="412"/>
        <source>AVC log</source>
        <translation>强访问控制</translation>
    </message>
    <message>
        <location filename="../src/ui/log/table.cpp" line="435"/>
        <source>User name</source>
        <translation>用户</translation>
    </message>
    <message>
        <location filename="../src/ui/log/table.cpp" line="436"/>
        <source>Sysadm</source>
        <translation>sysadm</translation>
    </message>
    <message>
        <location filename="../src/ui/log/table.cpp" line="437"/>
        <source>Secadm</source>
        <translation>secadm</translation>
    </message>
    <message>
        <location filename="../src/ui/log/table.cpp" line="438"/>
        <source>Audadm</source>
        <translation>audadm</translation>
    </message>
    <message>
        <location filename="../src/ui/log/table.cpp" line="457"/>
        <source>Result</source>
        <translation>结果</translation>
    </message>
    <message>
        <location filename="../src/ui/log/table.cpp" line="458"/>
        <location filename="../src/ui/log/table.cpp" line="469"/>
        <location filename="../src/ui/log/table.cpp" line="477"/>
        <location filename="../src/ui/log/table.cpp" line="483"/>
        <source>Success</source>
        <translation>成功</translation>
    </message>
    <message>
        <location filename="../src/ui/log/table.cpp" line="459"/>
        <location filename="../src/ui/log/table.cpp" line="469"/>
        <location filename="../src/ui/log/table.cpp" line="477"/>
        <source>Failed</source>
        <translation>失败</translation>
    </message>
</context>
<context>
    <name>KS::Log::Utils</name>
    <message>
        <location filename="../src/ui/log/utils.cpp" line="30"/>
        <location filename="../src/ui/log/utils.cpp" line="53"/>
        <source>Device log</source>
        <translation>设备日志</translation>
    </message>
    <message>
        <location filename="../src/ui/log/utils.cpp" line="32"/>
        <location filename="../src/ui/log/utils.cpp" line="54"/>
        <source>Tool box log</source>
        <translation>工具箱日志</translation>
    </message>
    <message>
        <location filename="../src/ui/log/utils.cpp" line="34"/>
        <location filename="../src/ui/log/utils.cpp" line="55"/>
        <source>Baseline reinforcement log</source>
        <translation>基线加固日志</translation>
    </message>
    <message>
        <location filename="../src/ui/log/utils.cpp" line="36"/>
        <location filename="../src/ui/log/utils.cpp" line="56"/>
        <source>Trusted protection log</source>
        <translation>可信保护日志</translation>
    </message>
    <message>
        <location filename="../src/ui/log/utils.cpp" line="38"/>
        <location filename="../src/ui/log/utils.cpp" line="57"/>
        <source>Files protection log</source>
        <translation>文件保护日志</translation>
    </message>
    <message>
        <location filename="../src/ui/log/utils.cpp" line="40"/>
        <location filename="../src/ui/log/utils.cpp" line="58"/>
        <source>Private box log</source>
        <translation>保险箱日志</translation>
    </message>
    <message>
        <location filename="../src/ui/log/utils.cpp" line="42"/>
        <location filename="../src/ui/log/utils.cpp" line="59"/>
        <source>Account log</source>
        <translation>用户日志</translation>
    </message>
    <message>
        <location filename="../src/ui/log/utils.cpp" line="44"/>
        <location filename="../src/ui/log/utils.cpp" line="60"/>
        <source>AVC log</source>
        <translation>强访问控制</translation>
    </message>
    <message>
        <location filename="../src/ui/log/utils.cpp" line="69"/>
        <location filename="../src/ui/log/utils.cpp" line="81"/>
        <source>Sysadm</source>
        <translation>sysadm</translation>
    </message>
    <message>
        <location filename="../src/ui/log/utils.cpp" line="71"/>
        <location filename="../src/ui/log/utils.cpp" line="82"/>
        <source>Secadm</source>
        <translation>secadm</translation>
    </message>
    <message>
        <location filename="../src/ui/log/utils.cpp" line="73"/>
        <location filename="../src/ui/log/utils.cpp" line="83"/>
        <source>Audadm</source>
        <translation>audadm</translation>
    </message>
    <message>
        <location filename="../src/ui/log/utils.cpp" line="75"/>
        <source>Unknown</source>
        <translation>未知</translation>
    </message>
</context>
<context>
    <name>KS::MessageDialog</name>
    <message>
        <location filename="../src/ui/common/message-dialog.cpp" line="38"/>
        <source>Notify</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../src/ui/common/message-dialog.cpp" line="46"/>
        <source>ok</source>
        <translation>确认</translation>
    </message>
</context>
<context>
    <name>KS::Notify::Notify</name>
    <message>
        <location filename="../src/notify/notify.cpp" line="28"/>
        <source>Security reinforcement</source>
        <translation>主机安全加固</translation>
    </message>
</context>
<context>
    <name>KS::Pagination</name>
    <message>
        <location filename="../src/ui/common/table/pagination.cpp" line="52"/>
        <source>Input page</source>
        <translation>输入页数</translation>
    </message>
</context>
<context>
    <name>KS::PasswordModification</name>
    <message>
        <location filename="../src/ui/common/password-modification.cpp" line="46"/>
        <source>Modify Password - %1</source>
        <translation>修改密码 - %1</translation>
    </message>
    <message>
        <location filename="../src/ui/common/password-modification.cpp" line="97"/>
        <location filename="../src/ui/common/password-modification.cpp" line="105"/>
        <source>The password must contain two types of lowercase letters, uppercase letters, numbers, and special characters, with a length of 8-16.</source>
        <translation>密码必须包含大写字母、小写字母、数字、特殊字符中的其中两种，且长度为8-16位。</translation>
    </message>
    <message>
        <location filename="../src/ui/common/password-modification.cpp" line="119"/>
        <source>The input cannot be empty, please improve the information.</source>
        <translation>输入不能为空，请输入正确的信息。</translation>
    </message>
    <message>
        <location filename="../src/ui/common/password-modification.cpp" line="125"/>
        <source>Please confirm whether the password is consistent.</source>
        <translation>请确认两次密码是否一致。</translation>
    </message>
</context>
<context>
    <name>KS::PolkitProxy</name>
    <message>
        <location filename="../src/daemon/common/polkit-proxy.cpp" line="218"/>
        <source>Authorization failed.</source>
        <translation>认证失败。</translation>
    </message>
</context>
<context>
    <name>KS::PrivateBox::Box</name>
    <message>
        <location filename="../src/ui/private-box/box.cpp" line="144"/>
        <location filename="../src/ui/private-box/box.cpp" line="157"/>
        <source>Lock</source>
        <translation>上锁</translation>
    </message>
    <message>
        <location filename="../src/ui/private-box/box.cpp" line="144"/>
        <location filename="../src/ui/private-box/box.cpp" line="157"/>
        <location filename="../src/ui/private-box/box.cpp" line="206"/>
        <source>Unlock</source>
        <translation>解锁</translation>
    </message>
    <message>
        <location filename="../src/ui/private-box/box.cpp" line="147"/>
        <source>Modify password</source>
        <translation>修改密码</translation>
    </message>
    <message>
        <location filename="../src/ui/private-box/box.cpp" line="148"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../src/ui/private-box/box.cpp" line="149"/>
        <source>Retrieve the password</source>
        <translation>找回密码</translation>
    </message>
    <message>
        <source>Please confirm whether the password is consistent.</source>
        <translation type="vanished">请确认两次密码是否一致。</translation>
    </message>
    <message>
        <location filename="../src/ui/private-box/box.cpp" line="263"/>
        <source>The input cannot be empty, please improve the information.</source>
        <translation>输入不能为空，请输入正确的信息。</translation>
    </message>
    <message>
        <location filename="../src/ui/private-box/box.cpp" line="246"/>
        <source>Del box</source>
        <translation>删除保险箱</translation>
    </message>
    <message>
        <location filename="../src/ui/private-box/box.cpp" line="232"/>
        <source>Remove box</source>
        <translation>移除保险箱</translation>
    </message>
    <message>
        <location filename="../src/ui/private-box/box.cpp" line="232"/>
        <source>The operation will delete the content inside the box.Are you sure you want to delete it?</source>
        <translation>此操作将删除保险箱以及保险箱里面的内容，您确认要删除吗？</translation>
    </message>
    <message>
        <location filename="../src/ui/private-box/box.cpp" line="259"/>
        <source>Retrieve password</source>
        <translation>找回密码</translation>
    </message>
    <message>
        <location filename="../src/ui/private-box/box.cpp" line="282"/>
        <source>Modify success!</source>
        <translation>修改成功！</translation>
    </message>
    <message>
        <location filename="../src/ui/private-box/box.cpp" line="293"/>
        <source>Your box password is %1</source>
        <translation>您的保险箱密码为：%1</translation>
    </message>
    <message>
        <location filename="../src/ui/private-box/box.cpp" line="304"/>
        <source>Unlock success!</source>
        <translation>解锁成功！</translation>
    </message>
    <message>
        <location filename="../src/ui/private-box/box.cpp" line="315"/>
        <source>Delete success!</source>
        <translation>删除成功！</translation>
    </message>
</context>
<context>
    <name>KS::PrivateBox::BoxCreation</name>
    <message>
        <location filename="../src/ui/private-box/box-creation.cpp" line="69"/>
        <location filename="../src/ui/private-box/box-creation.cpp" line="77"/>
        <source>The password must contain two types of lowercase letters, uppercase letters, numbers, and special characters, with a length of 8-16.</source>
        <translation>密码必须包含大写字母、小写字母、数字、特殊字符中的其中两种，且长度为8-16位。</translation>
    </message>
</context>
<context>
    <name>KS::PrivateBox::BoxManager</name>
    <message>
        <location filename="../src/daemon/private-box/box-manager.cpp" line="61"/>
        <location filename="../src/daemon/private-box/box-manager.cpp" line="86"/>
        <location filename="../src/daemon/private-box/box-manager.cpp" line="97"/>
        <source>Failed to create box. name is %1</source>
        <translation>创建保险箱失败，保险箱名为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/private-box/box-manager.cpp" line="74"/>
        <source>Fail to create box. name is %1</source>
        <translation>创建保险箱失败，保险箱名为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/private-box/box-manager.cpp" line="107"/>
        <source>Create box. name is %1</source>
        <translation>创建保险箱，保险箱名为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/private-box/box-manager.cpp" line="119"/>
        <source>Failed to delete box. box ID is %1</source>
        <translation>删除保险箱失败，保险箱id为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/private-box/box-manager.cpp" line="129"/>
        <source>Failed to delete box. box name is %1</source>
        <translation>删除保险箱失败，保险箱名为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/private-box/box-manager.cpp" line="137"/>
        <source>Delete box. box name is %1</source>
        <translation>删除保险箱，保险箱名为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/private-box/box-manager.cpp" line="209"/>
        <location filename="../src/daemon/private-box/box-manager.cpp" line="219"/>
        <location filename="../src/daemon/private-box/box-manager.cpp" line="228"/>
        <location filename="../src/daemon/private-box/box-manager.cpp" line="235"/>
        <source>Failed to modfify box password. box ID is %1</source>
        <translation>修改保险箱密码失败，保险箱id为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/private-box/box-manager.cpp" line="242"/>
        <source>Fail to modfify box password. box name is %1</source>
        <translation>修改保险箱密码失败，保险箱名为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/private-box/box-manager.cpp" line="248"/>
        <source>Modfify box password. box name is %1</source>
        <translation>修改保险箱密码，保险箱名为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/private-box/box-manager.cpp" line="261"/>
        <source>Failed to mount box. box ID is %1</source>
        <translation>解锁保险箱失败，保险箱id为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/private-box/box-manager.cpp" line="271"/>
        <source>Failed to mount box. box name is %1</source>
        <translation>解锁保险箱失败，保险箱名为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/private-box/box-manager.cpp" line="278"/>
        <source>Mount box. box name is %1</source>
        <translation>解锁保险箱，保险箱名为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/private-box/box-manager.cpp" line="288"/>
        <location filename="../src/daemon/private-box/box-manager.cpp" line="300"/>
        <source>Failed to retrieve box password. box ID is %1</source>
        <translation>找回保险箱密码失败，保险箱id为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/private-box/box-manager.cpp" line="312"/>
        <source>Failed to retrieve box password. box name is %1</source>
        <translation>找回保险箱密码失败，保险箱名为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/private-box/box-manager.cpp" line="319"/>
        <source>Retrieve box password. box name is %1</source>
        <translation>找回保险箱密码，保险箱名为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/private-box/box-manager.cpp" line="332"/>
        <source>Failed to unmount box. box ID is %1</source>
        <translation>上锁保险箱失败，保险箱id为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/private-box/box-manager.cpp" line="343"/>
        <source>Failed to unmount box. box name is %1</source>
        <translation>上锁保险箱失败，保险箱名为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/private-box/box-manager.cpp" line="348"/>
        <source>Unmount box. box name is %1</source>
        <translation>上锁保险箱，保险箱名为%1</translation>
    </message>
</context>
<context>
    <name>KS::PrivateBox::BoxPage</name>
    <message>
        <location filename="../src/ui/private-box/box-page.cpp" line="62"/>
        <source>Private box</source>
        <translation>私密保险箱</translation>
    </message>
    <message>
        <location filename="../src/ui/private-box/box-page.cpp" line="171"/>
        <source>Create box</source>
        <translation>创建保险箱</translation>
    </message>
    <message>
        <location filename="../src/ui/private-box/box-page.cpp" line="176"/>
        <source>Please confirm whether the password is consistent.</source>
        <translation>请确认两次密码是否一致。</translation>
    </message>
    <message>
        <location filename="../src/ui/private-box/box-page.cpp" line="180"/>
        <source>The input cannot be empty, please improve the information.</source>
        <translation>输入不能为空，请输入正确的信息。</translation>
    </message>
    <message>
        <location filename="../src/ui/private-box/box-page.cpp" line="209"/>
        <source>Please remember this box passphrase : %1, Can be used to retrieve passwords.</source>
        <translation>请记住您的保险箱口令：%1，可用于找回密码。</translation>
    </message>
</context>
<context>
    <name>KS::QRCodeDialog</name>
    <message>
        <source>QR code</source>
        <translation type="vanished">二维码</translation>
    </message>
</context>
<context>
    <name>KS::RespondDialog</name>
    <message>
        <source>Notify</source>
        <translation type="vanished">通知</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">确认</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
</context>
<context>
    <name>KS::Settings::BaselineReinforcement</name>
    <message>
        <source>Safety reinforcement</source>
        <translation type="vanished">主机安全加固</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.cpp" line="63"/>
        <source>Resource monitor open!</source>
        <translation>资源监控已开启！</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.cpp" line="72"/>
        <source>Resource monitor close!</source>
        <translation>资源监控已关闭！</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.cpp" line="80"/>
        <source>Notify open!</source>
        <translation>气泡通知已开启！</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.cpp" line="86"/>
        <source>Notify close!</source>
        <translation>气泡通知已关闭！</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.cpp" line="98"/>
        <location filename="../src/ui/settings/baseline-reinforcement.cpp" line="116"/>
        <source>Fallback</source>
        <translation>回退</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.cpp" line="98"/>
        <source>Are you sure you want to go back to the initialization state.</source>
        <translation>您确认要回退到初始状态？</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.cpp" line="116"/>
        <source>Are you sure you want to go back to the previous state.</source>
        <translation>您确认要回退到加固前的状态？</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.cpp" line="133"/>
        <source>home free space less than 10%</source>
        <translation>家目录空间不足10%</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.cpp" line="142"/>
        <source>root free space less than 10%</source>
        <translation>根目录空间不足10%</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.cpp" line="151"/>
        <source>The average load of a single core CPU exceeds 1</source>
        <translation>单核CPU负载超过1</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.cpp" line="161"/>
        <source>Memory space less than 10%</source>
        <translation>内存空间不足10%</translation>
    </message>
    <message>
        <source>Memory space remaining</source>
        <translation type="vanished">内存空间不足</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.cpp" line="222"/>
        <source>Files</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.cpp" line="222"/>
        <source>strategy(*.xml)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.cpp" line="229"/>
        <source>Open files failed!</source>
        <translation>打开文件失败！</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.cpp" line="233"/>
        <source>Failed to import strategy file. Please whether the file is valid!</source>
        <translation>导入策略失败，请检查策略文件的合法性！</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.cpp" line="233"/>
        <source>Import succeeded!</source>
        <translation>导入成功！</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.cpp" line="245"/>
        <source>Scheduled scanning task has been closed!</source>
        <translation>定时扫描已关闭！</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.cpp" line="253"/>
        <source>Scheduled scanning task has been started, every interval %1 scan once every hour.</source>
        <translation>定时扫描任务开启，每间隔%1小时扫描一次。</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.cpp" line="284"/>
        <source>Timed scan finished, Scaned %1, %2 conform, %3 inconform!</source>
        <translation>定时扫描任务完成，已扫描 %1项， %2项符合, %3项不符合！</translation>
    </message>
    <message>
        <source>Timed scan finied, Scaned %1, %2 conform, %3 inconform!</source>
        <translation type="vanished">定时扫描任务完成，共扫描%1项，%2项符合，%3项不符合！</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.cpp" line="316"/>
        <source>Open resource monitoring</source>
        <translation>资源监控已打开。</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.cpp" line="316"/>
        <source>Close resource monitoring</source>
        <translation>资源监控已关闭。</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.cpp" line="94"/>
        <location filename="../src/ui/settings/baseline-reinforcement.cpp" line="112"/>
        <source>Fallback is in progress, please wait.</source>
        <translation>回退正在进行中，请等待。</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/baseline-reinforcement.cpp" line="333"/>
        <source>Fallback finished!</source>
        <translation>回退完成！</translation>
    </message>
</context>
<context>
    <name>KS::Settings::DeviceControl</name>
    <message>
        <location filename="../src/ui/settings/device-control.cpp" line="102"/>
        <source> (Effective after restart)</source>
        <translation> (重启后生效)</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/device-control.cpp" line="191"/>
        <source>Switch</source>
        <translation>开关</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/device-control.cpp" line="211"/>
        <source>Are you sure to close the %1 interface</source>
        <translation>是否要关闭%1接口？</translation>
    </message>
</context>
<context>
    <name>KS::Settings::Dialog</name>
    <message>
        <location filename="../src/ui/settings/dialog.cpp" line="86"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/dialog.cpp" line="96"/>
        <source>Baseline reinforcement</source>
        <translation>基线加固</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/dialog.cpp" line="103"/>
        <source>Trusted protect</source>
        <translation>可信保护</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/dialog.cpp" line="108"/>
        <source>Interface Control</source>
        <translation>接口控制</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/dialog.cpp" line="113"/>
        <source>Identity authentication</source>
        <translation>身份鉴别</translation>
    </message>
</context>
<context>
    <name>KS::Settings::IdentityAuthentication</name>
    <message>
        <location filename="../src/ui/settings/identity-authentication.cpp" line="52"/>
        <source>Two-factor authentication</source>
        <translation>双因子认证</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/identity-authentication.cpp" line="52"/>
        <source>After turning on the switch, please make sure to enable the device in the peripheral management and bind the UKEY device in the control center before restarting the host to make the function effective.</source>
        <translation>开启开关后，请务必在外设管控中启用设备，并在控制中心绑定ukey设备后重启主机，使功能生效。</translation>
    </message>
</context>
<context>
    <name>KS::Settings::RespondDialog</name>
    <message>
        <source>Notify</source>
        <translation type="vanished">通知</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">确认</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
</context>
<context>
    <name>KS::Settings::TrustedProtected</name>
    <message>
        <location filename="../src/ui/settings/trusted-protected.cpp" line="79"/>
        <source>Trusted data needs to be initialised,please wait a few minutes before trying.</source>
        <translation>可信数据需要初始化，请等待几分钟后尝试。</translation>
    </message>
</context>
<context>
    <name>KS::Settings::TrustedUserPin</name>
    <message>
        <location filename="../src/ui/settings/trusted-user-pin.cpp" line="64"/>
        <source>Input pin code</source>
        <translation>输入pin码</translation>
    </message>
</context>
<context>
    <name>KS::TP::ExecuteProtected</name>
    <message>
        <source>A total of %1 records, Being tampered with %2</source>
        <translation type="vanished">总共%1条记录，被篡改%2条</translation>
    </message>
    <message>
        <source>Trusted protected</source>
        <translation type="vanished">可信保护</translation>
    </message>
    <message>
        <source>Execute protecked</source>
        <translation type="vanished">执行保护</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation type="vanished">打开文件</translation>
    </message>
    <message>
        <source>Please select the content that needs to be removed.</source>
        <translation type="vanished">请选择要删除的内容。</translation>
    </message>
</context>
<context>
    <name>KS::TP::ExecuteProtectedModel</name>
    <message>
        <location filename="../src/ui/tp/execute-protected-table.cpp" line="168"/>
        <source>Certified</source>
        <translation>已认证</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/execute-protected-table.cpp" line="204"/>
        <source>Number</source>
        <translation>序号</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/execute-protected-table.cpp" line="206"/>
        <source>File path</source>
        <translation>文件路径</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">文件类型</translation>
    </message>
    <message>
        <source>Status</source>
        <translation type="vanished">当前状态</translation>
    </message>
</context>
<context>
    <name>KS::TP::ExecuteProtectedPage</name>
    <message>
        <location filename="../src/ui/tp/execute-protected-page.cpp" line="50"/>
        <location filename="../src/ui/tp/execute-protected-page.cpp" line="114"/>
        <source>A total of %1 records, Being tampered with %2</source>
        <translation>共%1条记录，被篡改%2条</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/execute-protected-page.cpp" line="93"/>
        <source>Trusted protected</source>
        <translation>可信保护</translation>
    </message>
    <message>
        <source>Execute protecked</source>
        <translation type="vanished">执行文件保护</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/execute-protected-page.cpp" line="98"/>
        <source>Execute files protected</source>
        <translation>执行文件保护</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/execute-protected-page.cpp" line="137"/>
        <source>Open file</source>
        <translation>打开文件</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/execute-protected-page.cpp" line="143"/>
        <source>Added file types are not supported.</source>
        <translation>不允许添加不支持的文件类型。</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/execute-protected-page.cpp" line="168"/>
        <source>Please select the content that needs to be operated.</source>
        <translation>请选择需要操作的内容。</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/execute-protected-page.cpp" line="179"/>
        <source>Please select the content that needs to be removed.</source>
        <translation>请选择要删除的内容。</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/execute-protected-page.cpp" line="184"/>
        <source>Remove protection</source>
        <translation>移除保护</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/execute-protected-page.cpp" line="184"/>
        <source>The removal operation is irreversible.Do you confirm the removal of the selected record from the whitelist?</source>
        <translation>移除操作不可逆，您确认要从白名单中移除记录吗？</translation>
    </message>
</context>
<context>
    <name>KS::TP::ExecuteProtectedTable</name>
    <message>
        <location filename="../src/ui/tp/execute-protected-table.cpp" line="348"/>
        <location filename="../src/ui/tp/execute-protected-table.cpp" line="449"/>
        <location filename="../src/ui/tp/execute-protected-table.cpp" line="451"/>
        <source>Certified</source>
        <translation>已认证</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/execute-protected-table.cpp" line="418"/>
        <source>Type</source>
        <translation>文件类型</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/execute-protected-table.cpp" line="420"/>
        <location filename="../src/ui/tp/execute-protected-table.cpp" line="423"/>
        <source>Executable file</source>
        <translation>可执行程序</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/execute-protected-table.cpp" line="421"/>
        <location filename="../src/ui/tp/execute-protected-table.cpp" line="423"/>
        <source>Executable script</source>
        <translation>可执行脚本</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/execute-protected-table.cpp" line="422"/>
        <location filename="../src/ui/tp/execute-protected-table.cpp" line="423"/>
        <source>Dynamic library</source>
        <translation>动态库</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/execute-protected-table.cpp" line="447"/>
        <source>Status</source>
        <translation>当前状态</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/execute-protected-table.cpp" line="450"/>
        <location filename="../src/ui/tp/execute-protected-table.cpp" line="451"/>
        <source>Being tampered with</source>
        <translation>被篡改</translation>
    </message>
</context>
<context>
    <name>KS::TP::KernelProtected</name>
    <message>
        <source>A total of %1 records, Being tampered with %2</source>
        <translation type="vanished">总共%1条记录，被篡改%2条</translation>
    </message>
    <message>
        <source>Trusted protected</source>
        <translation type="vanished">可信保护</translation>
    </message>
    <message>
        <source>Kernel protecked</source>
        <translation type="vanished">内核保护</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation type="vanished">打开文件</translation>
    </message>
    <message>
        <source>Please select the content that needs to be removed.</source>
        <translation type="vanished">请选择要删除的内容。</translation>
    </message>
</context>
<context>
    <name>KS::TP::KernelProtectedModel</name>
    <message>
        <location filename="../src/ui/tp/kernel-protected-table.cpp" line="167"/>
        <source>Certified</source>
        <translation>已认证</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/kernel-protected-table.cpp" line="203"/>
        <source>Number</source>
        <translation>序号</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/kernel-protected-table.cpp" line="205"/>
        <source>File path</source>
        <translation>文件路径</translation>
    </message>
    <message>
        <source>Status</source>
        <translation type="vanished">当前状态</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/kernel-protected-table.cpp" line="209"/>
        <source>Prohibt unload</source>
        <translation>防卸载</translation>
    </message>
</context>
<context>
    <name>KS::TP::KernelProtectedPage</name>
    <message>
        <location filename="../src/ui/tp/kernel-protected-page.cpp" line="48"/>
        <location filename="../src/ui/tp/kernel-protected-page.cpp" line="108"/>
        <source>A total of %1 records, Being tampered with %2</source>
        <translation>共%1条记录，被篡改%2条</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/kernel-protected-page.cpp" line="87"/>
        <source>Trusted protected</source>
        <translation>可信保护</translation>
    </message>
    <message>
        <source>Kernel protecked</source>
        <translation type="vanished">内核模块保护</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/kernel-protected-page.cpp" line="92"/>
        <source>Kernel model protected</source>
        <translation>内核模块保护</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/kernel-protected-page.cpp" line="132"/>
        <source>Open file</source>
        <translation>打开文件</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/kernel-protected-page.cpp" line="137"/>
        <source>Added file types are not supported.</source>
        <translation>不允许添加不支持的文件类型。</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/kernel-protected-page.cpp" line="161"/>
        <source>Please select the content that needs to be operated.</source>
        <translation>请选择需要操作的内容。</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/kernel-protected-page.cpp" line="173"/>
        <source>Please select the content that needs to be removed.</source>
        <translation>请选择要删除的内容。</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/kernel-protected-page.cpp" line="178"/>
        <source>Remove protection</source>
        <translation>移除保护</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/kernel-protected-page.cpp" line="178"/>
        <source>The removal operation is irreversible.Do you confirm the removal of the selected record from the whitelist?</source>
        <translation>移除操作不可逆，您确认要从白名单中移除记录吗？</translation>
    </message>
</context>
<context>
    <name>KS::TP::KernelProtectedTable</name>
    <message>
        <location filename="../src/ui/tp/kernel-protected-table.cpp" line="358"/>
        <location filename="../src/ui/tp/kernel-protected-table.cpp" line="429"/>
        <location filename="../src/ui/tp/kernel-protected-table.cpp" line="431"/>
        <source>Certified</source>
        <translation>已认证</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/kernel-protected-table.cpp" line="427"/>
        <source>Status</source>
        <translation>当前状态</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/kernel-protected-table.cpp" line="430"/>
        <location filename="../src/ui/tp/kernel-protected-table.cpp" line="431"/>
        <source>Being tampered with</source>
        <translation>被篡改</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/kernel-protected-table.cpp" line="485"/>
        <source>%1</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>KS::TP::Utils</name>
    <message>
        <location filename="../src/ui/tp/utils.cpp" line="34"/>
        <source>Unknown file</source>
        <translation>未知文件</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/utils.cpp" line="37"/>
        <source>Executable file</source>
        <translation>可执行程序</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/utils.cpp" line="40"/>
        <source>Dynamic library</source>
        <translation>动态库</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/utils.cpp" line="43"/>
        <source>Kernel file</source>
        <translation>内核模块</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/utils.cpp" line="46"/>
        <source>Executable script</source>
        <translation>可执行脚本</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/utils.cpp" line="59"/>
        <source>Certified</source>
        <translation>已认证</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/utils.cpp" line="63"/>
        <source>Being tampered with</source>
        <translation>被篡改</translation>
    </message>
</context>
<context>
    <name>KS::TPPage</name>
    <message>
        <source>Execute protecked</source>
        <translation type="vanished">执行保护</translation>
    </message>
    <message>
        <source>Kernel protecked</source>
        <translation type="vanished">内核保护</translation>
    </message>
</context>
<context>
    <name>KS::TPUtils</name>
    <message>
        <source>Unknown file</source>
        <translation type="vanished">未知文件</translation>
    </message>
    <message>
        <source>Executable file</source>
        <translation type="vanished">可执行文件</translation>
    </message>
    <message>
        <source>Dynamic library</source>
        <translation type="vanished">动态库</translation>
    </message>
    <message>
        <source>Kernel file</source>
        <translation type="vanished">内核模块</translation>
    </message>
    <message>
        <source>Executable script</source>
        <translation type="vanished">可执行脚本</translation>
    </message>
    <message>
        <source>Certified</source>
        <translation type="vanished">已认证</translation>
    </message>
    <message>
        <source>Being tampered with</source>
        <translation type="vanished">被篡改</translation>
    </message>
</context>
<context>
    <name>KS::TableDeleteNotify</name>
    <message>
        <source>Remove protection</source>
        <translation type="vanished">移除保护</translation>
    </message>
    <message>
        <source>The removal operation is irreversible.Do you confirm the removal of the selected record from the whitelist?</source>
        <translation type="vanished">移除操作不可逆，您确认要从白名单中移除记录吗？</translation>
    </message>
</context>
<context>
    <name>KS::ToolBox::AccessControlModel</name>
    <message>
        <location filename="../src/ui/tool-box/access-control/access-control-table.cpp" line="147"/>
        <source>User name</source>
        <translation>用户</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/access-control/access-control-table.cpp" line="149"/>
        <source>User function</source>
        <translation>权限类型</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/access-control/access-control-table.cpp" line="178"/>
        <source>sysadm(system admin)</source>
        <translation>sysadm（系统管理员）</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/access-control/access-control-table.cpp" line="179"/>
        <source>Use, Network, Soft, Password, Device, Time, Drive, Resource, Log manager</source>
        <translation>用户管理、网络管理、软件管理、密码管理、设备管理、时间管理、驱动管理、资源管理、日志管理</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/access-control/access-control-table.cpp" line="181"/>
        <source>secadm(secure admin)</source>
        <translation>secadm（安全管理员）</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/access-control/access-control-table.cpp" line="182"/>
        <source>User role, Access policy, CAP authorzation modification, Role attribute, files secure manager</source>
        <translation>用户角色管理、访问策略管理、CAP权限修改、角色属性管理、文件安全属性管理</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/access-control/access-control-table.cpp" line="184"/>
        <source>audadm(audit admin)</source>
        <translation>audadm（审计管理员）</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/access-control/access-control-table.cpp" line="185"/>
        <source>Secure audit, Audit policy manager, Audit log manager</source>
        <translation>安全审计、审计策略管理、审计日志管理</translation>
    </message>
</context>
<context>
    <name>KS::ToolBox::AccessControlPage</name>
    <message>
        <location filename="../src/ui/tool-box/access-control/access-control-page.cpp" line="43"/>
        <source>Tool Box</source>
        <translation>安全工具箱</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/access-control/access-control-page.cpp" line="48"/>
        <source>Access Control</source>
        <translation>访问控制</translation>
    </message>
</context>
<context>
    <name>KS::ToolBox::AddUserDialog</name>
    <message>
        <source>Input user name</source>
        <translation type="vanished">输入用户名</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-sign/add-user-dialog.cpp" line="47"/>
        <source>Input user names</source>
        <translation>输入用户名</translation>
    </message>
</context>
<context>
    <name>KS::ToolBox::FileShredModel</name>
    <message>
        <location filename="../src/ui/tool-box/file-shred/file-shred-table.cpp" line="226"/>
        <source>Number</source>
        <translation>序号</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-shred/file-shred-table.cpp" line="228"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-shred/file-shred-table.cpp" line="230"/>
        <source>Path</source>
        <translation>路径</translation>
    </message>
    <message>
        <source>File name</source>
        <translation type="vanished">文件名称</translation>
    </message>
    <message>
        <source>File path</source>
        <translation type="vanished">文件路径</translation>
    </message>
</context>
<context>
    <name>KS::ToolBox::FileShredPage</name>
    <message>
        <location filename="../src/ui/tool-box/file-shred/file-shred-page.cpp" line="55"/>
        <source>Tool Box</source>
        <translation>安全工具箱</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-shred/file-shred-page.cpp" line="60"/>
        <source>File Shred</source>
        <translation>文件粉碎</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-shred/file-shred-page.cpp" line="129"/>
        <location filename="../src/ui/tool-box/file-shred/file-shred-page.cpp" line="139"/>
        <source>Please selecte files or dirs.</source>
        <translation>请选择文件或目录。</translation>
    </message>
    <message>
        <source>Please selecte files.</source>
        <translation type="vanished">请选择文件。</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-shred/file-shred-page.cpp" line="143"/>
        <source>File shred</source>
        <translation>文件粉碎</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-shred/file-shred-page.cpp" line="143"/>
        <source>After crushing, files or folders cannot be recovered.Are you sure you want to crush them?</source>
        <translation>粉碎文件或目录操作不可逆，您确认要粉碎吗？</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation type="vanished">打开文件</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-shred/file-shred-page.cpp" line="108"/>
        <location filename="../src/ui/tool-box/file-shred/file-shred-page.cpp" line="158"/>
        <source>A total of %1 records</source>
        <translation>共%1条记录</translation>
    </message>
</context>
<context>
    <name>KS::ToolBox::FileShredTable</name>
    <message>
        <source>Please selecte files.</source>
        <translation type="vanished">请选择文件。</translation>
    </message>
</context>
<context>
    <name>KS::ToolBox::FileSign</name>
    <message>
        <location filename="../src/ui/tool-box/file-sign/file-sign-page.cpp" line="115"/>
        <source>NULL</source>
        <translation>无</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-sign/file-sign-page.cpp" line="151"/>
        <source>Please select items.</source>
        <translation>请选择要操作的对象。</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-sign/file-sign-page.cpp" line="157"/>
        <location filename="../src/ui/tool-box/file-sign/file-sign-page.cpp" line="160"/>
        <source>A total of %1 records</source>
        <translation>共%1条记录</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-sign/file-sign-page.cpp" line="79"/>
        <source>Security Sign</source>
        <translation>安全标记</translation>
    </message>
    <message>
        <source>modify security context</source>
        <translation type="vanished">编辑安全上下文</translation>
    </message>
    <message>
        <source>Please enter a new security context</source>
        <translation type="vanished">请输入新的安全上下文</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-sign/file-sign-page.cpp" line="74"/>
        <source>Tool Box</source>
        <translation>安全工具箱</translation>
    </message>
    <message>
        <source>File Sign</source>
        <translation type="vanished">文件标记</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-sign/file-sign-page.cpp" line="89"/>
        <source>Open files</source>
        <translation>打开文件</translation>
    </message>
</context>
<context>
    <name>KS::ToolBox::FileSignDelegate</name>
    <message>
        <location filename="../src/ui/tool-box/file-sign/file-sign-table.cpp" line="470"/>
        <source>Edit</source>
        <translation>编辑</translation>
    </message>
</context>
<context>
    <name>KS::ToolBox::FileSignModel</name>
    <message>
        <source>File Name</source>
        <translation type="vanished">文件名</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-sign/file-sign-table.cpp" line="139"/>
        <source>Number</source>
        <translation>序号</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-sign/file-sign-table.cpp" line="141"/>
        <source>Object Name</source>
        <translation>对象名称</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-sign/file-sign-table.cpp" line="143"/>
        <source>Security Context</source>
        <translation>安全上下文</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-sign/file-sign-table.cpp" line="145"/>
        <source>Complete Label</source>
        <translation>完整性标签</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-sign/file-sign-table.cpp" line="147"/>
        <source>Operate</source>
        <translation>操作</translation>
    </message>
</context>
<context>
    <name>KS::ToolBox::FileSignTable</name>
    <message>
        <source>Please select the content that needs to be removed.</source>
        <translation type="vanished">请选择要删除的内容。</translation>
    </message>
</context>
<context>
    <name>KS::ToolBox::Manager</name>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="169"/>
        <source>Failed to set access control status, permission denied</source>
        <translation>没有权限设置访问控制开关</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="191"/>
        <source>Failed to set access control status</source>
        <translation>设置访问控制失败</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="197"/>
        <source>set access control status to %1</source>
        <translation>%1访问控制</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="415"/>
        <source>Failed to shred file, permission denied</source>
        <translation>没有权限粉碎文件</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="498"/>
        <source>Failed to shred file: %1</source>
        <translation>粉碎文件失败：%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="505"/>
        <source>Shred files %1</source>
        <translation>粉碎文件，文件路径为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="533"/>
        <source>Failed to remove user, permission denied</source>
        <translation>删除用户失败，权限禁止</translation>
    </message>
    <message>
        <source>Remove users %1</source>
        <translation type="vanished">删除%1用户</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="640"/>
        <source>Failed to add files to SignFile list, permission denied</source>
        <translation>添加文件到安全标记列表失败</translation>
    </message>
    <message>
        <source>Failed to get mls label, permission denied</source>
        <translation type="vanished">获取上下文失败</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="217"/>
        <location filename="../src/daemon/tool-box/manager.cpp" line="350"/>
        <source>Get files %1 mls label</source>
        <translation>获取%1的上下文</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="221"/>
        <location filename="../src/daemon/tool-box/manager.cpp" line="223"/>
        <source>Failed to get files %1 mls label, error msg %2</source>
        <translation></translation>
    </message>
    <message>
        <source>Failed to get kic label, permission denied</source>
        <translation type="vanished">获取完整性标签失败</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="248"/>
        <source>Get files %1 kic label</source>
        <translation>获取%1的完整性标签</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="252"/>
        <location filename="../src/daemon/tool-box/manager.cpp" line="254"/>
        <source>Failed to get files %1 kic label, error msg %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="275"/>
        <source>Failed to set mls label, permission denied</source>
        <translation>设置安全上下文失败</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="287"/>
        <location filename="../src/daemon/tool-box/manager.cpp" line="391"/>
        <source>Failed to set %1 mls label, error msg: %2</source>
        <translation>设置%1的安全等级失败，错误消息为%2</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="294"/>
        <source>Set %1 mls label to: %2</source>
        <translation>设置%1的上下文为%2</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="311"/>
        <source>Failed to set kic label, permission denied</source>
        <translation>设置完整性标签失败</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="323"/>
        <source>Failed to set %1 kic label, error msg: %2</source>
        <translation>设置%1的完整性标签失败，错误消息为%2</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="329"/>
        <source>Set %1 kic label to: %2</source>
        <translation>设置%1的完整性标签为%2</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="355"/>
        <location filename="../src/daemon/tool-box/manager.cpp" line="357"/>
        <source>Failed to get user %1 mls label, error msg %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="379"/>
        <source>Failed to set user mls label, permission denied</source>
        <translation>设置用户上下文失败</translation>
    </message>
    <message>
        <source>Failed to set user mls label, invalid parameter</source>
        <translation type="vanished">设置用户上下文失败</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="397"/>
        <source>Set %1 user mls label to: %2</source>
        <translation>设置%1用户的上下文为%2</translation>
    </message>
    <message>
        <source>Failed to Shred files: %1, see log for more details</source>
        <translation type="vanished">粉碎%1文件失败，详情请查看日志</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="551"/>
        <source>Failed to remove user %1, error msg: %2</source>
        <translation>删除%1用户失败，错误消息为：%2</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="571"/>
        <source>Remove users: %1</source>
        <translation>删除用户：%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="662"/>
        <source>Failed to add user to object list, unknown user %1</source>
        <translation>添加用户失败，未知用户%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="677"/>
        <source>Failed to add object to object list, database error</source>
        <translation>添加对象到数据库失败</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="683"/>
        <source>Add obj to object list: %1</source>
        <translation>添加对象成功，对象名为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="699"/>
        <source>Failed to remove obj from obj list, permission denied</source>
        <translation>移除对象失败</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="707"/>
        <source>Failed to remove file from object list, database error</source>
        <translation>从数据库中移除对象失败</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="713"/>
        <source>Remove file from object list: %1</source>
        <translation>移除对象：%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="755"/>
        <source>Failed to add files to ShredFile list, permission denied</source>
        <translation>添加文件粉碎列表失败</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="765"/>
        <source>Failed to add files to ShredFile list, database error</source>
        <translation>添加文件粉碎列表失败，数据库错误</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="771"/>
        <source>Add file to ShredFile list: %1</source>
        <translation>添加文件粉碎列表：%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="787"/>
        <source>Failed to remove file from ShredFile list, permission denied</source>
        <translation>移除文件粉碎列表失败</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="795"/>
        <source>Failed to remove file from ShredFile list, database error</source>
        <translation>移除文件粉碎列表失败，数据库错误</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="801"/>
        <source>Remove file from ShredFile list: %1</source>
        <translation>移除文件粉碎列表：%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/manager.cpp" line="911"/>
        <source> failed, exitCode %1, error msg: %2</source>
        <translation>失败，退出码为%1，错误消息为%2</translation>
    </message>
</context>
<context>
    <name>KS::ToolBox::ModifySecurityContext</name>
    <message>
        <source>modify security context</source>
        <translation type="vanished">编辑安全上下文</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-sign/modify-security-context.cpp" line="86"/>
        <source>modify security sign</source>
        <translation>编辑安全标记</translation>
    </message>
</context>
<context>
    <name>KS::ToolBox::PrivacyCleanupModel</name>
    <message>
        <location filename="../src/ui/tool-box/privacy-cleanup/privacy-cleanup-table.cpp" line="228"/>
        <source>Number</source>
        <translation>序号</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/privacy-cleanup/privacy-cleanup-table.cpp" line="230"/>
        <source>User name</source>
        <translation>用户名称</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/privacy-cleanup/privacy-cleanup-table.cpp" line="232"/>
        <source>User type</source>
        <translation>用户类型</translation>
    </message>
</context>
<context>
    <name>KS::ToolBox::PrivacyCleanupPage</name>
    <message>
        <location filename="../src/ui/tool-box/privacy-cleanup/privacy-cleanup-page.cpp" line="41"/>
        <source>Tool Box</source>
        <translation>安全工具箱</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/privacy-cleanup/privacy-cleanup-page.cpp" line="45"/>
        <source>Privacy Cleanup</source>
        <translation>隐私清理</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/privacy-cleanup/privacy-cleanup-page.cpp" line="59"/>
        <location filename="../src/ui/tool-box/privacy-cleanup/privacy-cleanup-page.cpp" line="98"/>
        <source>A total of %1 records</source>
        <translation>共%1条记录</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/privacy-cleanup/privacy-cleanup-page.cpp" line="80"/>
        <source>Please select items.</source>
        <translation>请选择要操作的对象。</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/privacy-cleanup/privacy-cleanup-page.cpp" line="84"/>
        <source>Privacy cleanup</source>
        <translation>隐私清理</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/privacy-cleanup/privacy-cleanup-page.cpp" line="84"/>
        <source>The user privacy cleaning operation is irreversible. Are you sure you want to continue?</source>
        <translation>用户隐私清理不可逆，您确认要清理吗？</translation>
    </message>
</context>
<context>
    <name>KS::ToolBox::PrivacyCleanupTable</name>
    <message>
        <source>Please select items.</source>
        <translation type="vanished">请选择要清除的用户。</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/privacy-cleanup/privacy-cleanup-table.cpp" line="370"/>
        <source>Delete success!</source>
        <translation>删除成功！</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/privacy-cleanup/privacy-cleanup-table.cpp" line="453"/>
        <source>Manager user</source>
        <translation>管理员用户</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/privacy-cleanup/privacy-cleanup-table.cpp" line="453"/>
        <source>Normal user</source>
        <translation>普通用户</translation>
    </message>
</context>
<context>
    <name>KS::ToolBox::RealTimeAlert</name>
    <message>
        <source>Detect nmap attack, attacker ips: %1</source>
        <translation type="vanished">检测到nmap攻击，攻击者ip为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/realtime-alert.cpp" line="219"/>
        <source>Detect nmap attack from %1</source>
        <translation>检测到主机被 %1 探测</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/realtime-alert.cpp" line="225"/>
        <source>Detected nmap attack! attacker ip:%1</source>
        <translation>检测到nmap攻击，攻击者ip为%1</translation>
    </message>
    <message>
        <location filename="../src/daemon/tool-box/realtime-alert.cpp" line="296"/>
        <source>Detected hazard behavior! msg:%1</source>
        <translation>检测到危险行为！%1</translation>
    </message>
</context>
<context>
    <name>KS::TrustedProtected</name>
    <message>
        <source>Trusted data needs to be initialised,please wait a few minutes before trying.</source>
        <translation type="vanished">可信数据需要初始化，请等待几分钟后尝试。</translation>
    </message>
</context>
<context>
    <name>KS::TrustedUserPin</name>
    <message>
        <source>Input pin code</source>
        <translation type="vanished">输入pin码</translation>
    </message>
</context>
<context>
    <name>KS::Window</name>
    <message>
        <location filename="../src/ui/window.cpp" line="108"/>
        <location filename="../src/ui/window.cpp" line="536"/>
        <source>Fallback is in progress, please wait.</source>
        <translation>回退正在进行中，请等待。</translation>
    </message>
    <message>
        <source>Safety reinforcement</source>
        <translation type="vanished">主机安全加固</translation>
    </message>
    <message>
        <location filename="../src/ui/window.cpp" line="139"/>
        <location filename="../src/ui/window.cpp" line="156"/>
        <source>Security reinforcement</source>
        <translation>主机安全加固</translation>
    </message>
    <message>
        <source>Unactivated</source>
        <translation type="vanished">未激活</translation>
    </message>
    <message>
        <location filename="../src/ui/window.cpp" line="186"/>
        <source>Modify password</source>
        <translation>修改密码</translation>
    </message>
    <message>
        <location filename="../src/ui/window.cpp" line="190"/>
        <source>Logout</source>
        <translation>注销</translation>
    </message>
    <message>
        <location filename="../src/ui/window.cpp" line="203"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../src/ui/window.cpp" line="206"/>
        <source>Activation</source>
        <translation>软件激活</translation>
    </message>
    <message>
        <location filename="../src/ui/window.cpp" line="207"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="../src/ui/window.cpp" line="215"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../src/ui/window.cpp" line="270"/>
        <location filename="../src/ui/window.cpp" line="315"/>
        <location filename="../src/ui/window.cpp" line="333"/>
        <location filename="../src/ui/window.cpp" line="346"/>
        <source>Baseline reinforcement</source>
        <translation>基线加固</translation>
    </message>
    <message>
        <location filename="../src/ui/window.cpp" line="274"/>
        <location filename="../src/ui/window.cpp" line="276"/>
        <location filename="../src/ui/window.cpp" line="495"/>
        <source>Trusted protected</source>
        <translation>可信保护</translation>
    </message>
    <message>
        <location filename="../src/ui/window.cpp" line="278"/>
        <location filename="../src/ui/window.cpp" line="280"/>
        <source>File protected</source>
        <translation>文件保护</translation>
    </message>
    <message>
        <location filename="../src/ui/window.cpp" line="282"/>
        <location filename="../src/ui/window.cpp" line="284"/>
        <source>Private box</source>
        <translation>私密保险箱</translation>
    </message>
    <message>
        <location filename="../src/ui/window.cpp" line="286"/>
        <location filename="../src/ui/window.cpp" line="288"/>
        <source>Device management</source>
        <translation>设备管理</translation>
    </message>
    <message>
        <location filename="../src/ui/window.cpp" line="290"/>
        <location filename="../src/ui/window.cpp" line="292"/>
        <source>Tool Box</source>
        <translation>安全工具箱</translation>
    </message>
    <message>
        <location filename="../src/ui/window.cpp" line="294"/>
        <location filename="../src/ui/window.cpp" line="296"/>
        <source>Log audit</source>
        <translation>日志审计</translation>
    </message>
    <message>
        <location filename="../src/ui/window.cpp" line="315"/>
        <source>Interface Control</source>
        <translation>接口控制</translation>
    </message>
    <message>
        <location filename="../src/ui/window.cpp" line="319"/>
        <source>Trusted protect</source>
        <translation>可信保护</translation>
    </message>
    <message>
        <location filename="../src/ui/window.cpp" line="319"/>
        <source>Identity authentication</source>
        <translation>身份鉴别</translation>
    </message>
</context>
<context>
    <name>KernelProtected</name>
    <message>
        <source>Manage kernel driver modules to prevent illegal loading and uninstallation</source>
        <translation type="vanished">管理内核驱动模块，阻止非法加载、卸载</translation>
    </message>
    <message>
        <source>Please enter keyword search</source>
        <translation type="vanished">请输入关键字搜索</translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="vanished">添加</translation>
    </message>
    <message>
        <source>Recertification</source>
        <translation type="vanished">重新认证</translation>
    </message>
    <message>
        <source>Unprotect</source>
        <translation type="vanished">移除</translation>
    </message>
</context>
<context>
    <name>KernelProtectedPage</name>
    <message>
        <location filename="../src/ui/tp/kernel-protected-page.ui" line="14"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_kernel-protected-page.h" line="144"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/tp/kernel-protected-page.ui" line="49"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_kernel-protected-page.h" line="145"/>
        <source>Manage kernel driver modules to prevent illegal loading and uninstallation</source>
        <translation>管理内核驱动模块，阻止非法加载、卸载</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/kernel-protected-page.ui" line="69"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_kernel-protected-page.h" line="146"/>
        <source>0 records in total</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/tp/kernel-protected-page.ui" line="114"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_kernel-protected-page.h" line="147"/>
        <source>Please enter keyword search</source>
        <translation>请输入关键字搜索</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/kernel-protected-page.ui" line="146"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_kernel-protected-page.h" line="148"/>
        <source>Add</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/kernel-protected-page.ui" line="165"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_kernel-protected-page.h" line="149"/>
        <source>Recertification</source>
        <translation>重新认证</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/kernel-protected-page.ui" line="184"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_kernel-protected-page.h" line="150"/>
        <source>Unprotect</source>
        <translation>移除</translation>
    </message>
    <message>
        <location filename="../src/ui/tp/kernel-protected-page.ui" line="200"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_kernel-protected-page.h" line="151"/>
        <source>...</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Log::LogTable</name>
    <message>
        <source>Log type</source>
        <translation type="vanished">日志类型</translation>
    </message>
    <message>
        <source>Device log</source>
        <translation type="vanished">设备日志</translation>
    </message>
    <message>
        <source>Tool box log</source>
        <translation type="vanished">工具箱日志</translation>
    </message>
    <message>
        <source>Baseline reinforcement log</source>
        <translation type="vanished">基线加固日志</translation>
    </message>
    <message>
        <source>Trusted protection log</source>
        <translation type="vanished">可信保护日志</translation>
    </message>
    <message>
        <source>Files protection log</source>
        <translation type="vanished">可信保护日志</translation>
    </message>
    <message>
        <source>Private box log</source>
        <translation type="vanished">保险箱日志</translation>
    </message>
    <message>
        <source>Account log</source>
        <translation type="vanished">账户日志</translation>
    </message>
    <message>
        <source>User name</source>
        <translation type="vanished">用户</translation>
    </message>
    <message>
        <source>Sysadm</source>
        <translation type="vanished">sysadm</translation>
    </message>
    <message>
        <source>Secadm</source>
        <translation type="vanished">secadm</translation>
    </message>
    <message>
        <source>Audadm</source>
        <translation type="vanished">audadm</translation>
    </message>
    <message>
        <source>Result</source>
        <translation type="vanished">结果</translation>
    </message>
    <message>
        <source>Success</source>
        <translation type="vanished">成功</translation>
    </message>
    <message>
        <source>Failed</source>
        <translation type="vanished">失败</translation>
    </message>
</context>
<context>
    <name>LogPage</name>
    <message>
        <location filename="../src/ui/log/log-page.ui" line="14"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_log-page.h" line="124"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>System user behavior audit, timely understranding of potential secutify risks</source>
        <translation type="vanished">系统用户行为审计，及时了解潜在安全隐患</translation>
    </message>
    <message>
        <location filename="../src/ui/log/log-page.ui" line="46"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_log-page.h" line="125"/>
        <source>User behavior audit, timely understranding of potential secutify risks</source>
        <translation>用户行为审计，及时了解潜在安全隐患</translation>
    </message>
    <message>
        <location filename="../src/ui/log/log-page.ui" line="66"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_log-page.h" line="126"/>
        <source>0 records in total</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/log/log-page.ui" line="134"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_log-page.h" line="127"/>
        <source>Please enter keyword search</source>
        <translation>请输入关键字搜索</translation>
    </message>
    <message>
        <location filename="../src/ui/log/log-page.ui" line="153"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_log-page.h" line="128"/>
        <source>Centigrade</source>
        <translation>查询</translation>
    </message>
</context>
<context>
    <name>Login</name>
    <message>
        <location filename="../src/ui/account/login.ui" line="14"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_login.h" line="130"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/account/login.ui" line="58"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_login.h" line="131"/>
        <source>User name:</source>
        <translation>用户名：</translation>
    </message>
    <message>
        <location filename="../src/ui/account/login.ui" line="79"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_login.h" line="132"/>
        <source>Password:</source>
        <translation>密码：</translation>
    </message>
    <message>
        <location filename="../src/ui/account/login.ui" line="125"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_login.h" line="133"/>
        <source>login</source>
        <translation>登录</translation>
    </message>
    <message>
        <location filename="../src/ui/account/login.ui" line="144"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_login.h" line="134"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>ModifySecurityContext</name>
    <message>
        <location filename="../src/ui/tool-box/file-sign/modify-security-context.ui" line="14"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_modify-security-context.h" line="123"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-sign/modify-security-context.ui" line="68"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_modify-security-context.h" line="124"/>
        <source>Please input new security context :</source>
        <translation>请输入新的安全上下文:</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-sign/modify-security-context.ui" line="97"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_modify-security-context.h" line="125"/>
        <source>Please input new integrity label :</source>
        <translation>请输入新的完整性标签：</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-sign/modify-security-context.ui" line="144"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_modify-security-context.h" line="127"/>
        <source>ok</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/file-sign/modify-security-context.ui" line="163"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_modify-security-context.h" line="128"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>PDF</name>
    <message>
        <location filename="../src/ui/br/reports/pdf.ui" line="72"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_pdf.h" line="262"/>
        <source>Operating system:</source>
        <translation>操作系统：</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/pdf.ui" line="85"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_pdf.h" line="263"/>
        <source>IP:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/pdf.ui" line="98"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_pdf.h" line="264"/>
        <source>MAC:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/pdf.ui" line="111"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_pdf.h" line="265"/>
        <source>System kernel version:</source>
        <translation>系统内核版本：</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/pdf.ui" line="124"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_pdf.h" line="266"/>
        <source>Software activation status:</source>
        <translation>软件激活状态：</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/pdf.ui" line="259"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_pdf.h" line="273"/>
        <source>Device Information</source>
        <translation>设备信息</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reports/pdf.ui" line="347"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_pdf.h" line="275"/>
        <source>Pie chart of test results</source>
        <translation>检测结果饼图</translation>
    </message>
</context>
<context>
    <name>Pagination</name>
    <message>
        <location filename="../src/ui/common/table/pagination.ui" line="26"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_pagination.h" line="99"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/common/table/pagination.ui" line="72"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_pagination.h" line="100"/>
        <source>&lt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/common/table/pagination.ui" line="117"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_pagination.h" line="102"/>
        <source>&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/common/table/pagination.ui" line="133"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_pagination.h" line="103"/>
        <source>goto</source>
        <translation>前往</translation>
    </message>
    <message>
        <source>Prev</source>
        <translation type="vanished">上一页</translation>
    </message>
    <message>
        <source>Next</source>
        <translation type="vanished">下一页</translation>
    </message>
    <message>
        <source>Go to</source>
        <translation type="vanished">前往</translation>
    </message>
</context>
<context>
    <name>PasswordModification</name>
    <message>
        <location filename="../src/ui/common/password-modification.ui" line="14"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_password-modification.h" line="199"/>
        <source>Modify password</source>
        <translation>修改密码</translation>
    </message>
    <message>
        <location filename="../src/ui/common/password-modification.ui" line="74"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_password-modification.h" line="200"/>
        <source>Current password:</source>
        <translation>当前密码：</translation>
    </message>
    <message>
        <source>New password:       </source>
        <translation type="vanished">新密码：</translation>
    </message>
    <message>
        <location filename="../src/ui/common/password-modification.ui" line="100"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_password-modification.h" line="201"/>
        <source>New password:</source>
        <translation>新密码：</translation>
    </message>
    <message>
        <location filename="../src/ui/common/password-modification.ui" line="113"/>
        <location filename="../src/ui/common/password-modification.ui" line="170"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_password-modification.h" line="203"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_password-modification.h" line="208"/>
        <source>The password must contain two types of lowercase letters, uppercase letters, numbers, and special characters, with a length of 8-16.</source>
        <translation>密码必须包含大写字母、小写字母、数字、特殊字符中的其中两种，且长度为8-16位。</translation>
    </message>
    <message>
        <location filename="../src/ui/common/password-modification.ui" line="157"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_password-modification.h" line="206"/>
        <source>Confirm password:</source>
        <translation>确认密码：</translation>
    </message>
    <message>
        <location filename="../src/ui/common/password-modification.ui" line="234"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_password-modification.h" line="211"/>
        <source>OK</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../src/ui/common/password-modification.ui" line="253"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_password-modification.h" line="212"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>PrivacyCleanupPage</name>
    <message>
        <location filename="../src/ui/tool-box/privacy-cleanup/privacy-cleanup-page.ui" line="14"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_privacy-cleanup-page.h" line="116"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/privacy-cleanup/privacy-cleanup-page.ui" line="46"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_privacy-cleanup-page.h" line="117"/>
        <source>Clear users and their privacy information</source>
        <translation>清除用户及用户隐私信息</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/privacy-cleanup/privacy-cleanup-page.ui" line="66"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_privacy-cleanup-page.h" line="118"/>
        <source>0 records in total</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/privacy-cleanup/privacy-cleanup-page.ui" line="111"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_privacy-cleanup-page.h" line="119"/>
        <source>Please enter keyword search</source>
        <translation>请输入关键字搜索</translation>
    </message>
    <message>
        <location filename="../src/ui/tool-box/privacy-cleanup/privacy-cleanup-page.ui" line="143"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_privacy-cleanup-page.h" line="120"/>
        <source>clean</source>
        <translation>清除</translation>
    </message>
</context>
<context>
    <name>Progress</name>
    <message>
        <location filename="../src/ui/br/progress.ui" line="14"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_progress.h" line="117"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/br/progress.ui" line="25"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_progress.h" line="118"/>
        <source>Security reinforcement is protecting your computer</source>
        <translation>安全加固正在保护您的电脑</translation>
    </message>
    <message>
        <location filename="../src/ui/br/progress.ui" line="37"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_progress.h" line="119"/>
        <source>KylinSec Host Security Reinforcement Software Detects Risks in Advance to Ensure Asset Security</source>
        <translation>麒麟信安主机安全加固软件提起发现风险，保障资产安全</translation>
    </message>
    <message>
        <location filename="../src/ui/br/progress.ui" line="47"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_progress.h" line="120"/>
        <source>Generate report</source>
        <translation>生成报表</translation>
    </message>
    <message>
        <location filename="../src/ui/br/progress.ui" line="105"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_progress.h" line="121"/>
        <source>Scan</source>
        <translation>开始扫描</translation>
    </message>
    <message>
        <location filename="../src/ui/br/progress.ui" line="118"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_progress.h" line="122"/>
        <source>return</source>
        <translation>返回</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../lib/base/error.cpp" line="32"/>
        <source>Success.</source>
        <translation>成功。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="35"/>
        <source>Invalid args.</source>
        <translation>非法参数。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="38"/>
        <source>Added file types are not supported.</source>
        <translation>不允许添加不支持的文件类型。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="41"/>
        <source>The file is already in the list, and there is no need to add it repeatedly.</source>
        <translation>文件已在列表中，无需重复添加。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="44"/>
        <source>There is no trusted card or the trusted card is not supported.</source>
        <translation>可信卡未接入或不支持可信卡。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="47"/>
        <source>The pin code is wrong!</source>
        <translation>pin码错误！</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="50"/>
        <source>Failed to delete box.</source>
        <translation>删除保险箱失败。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="53"/>
        <source>Failed to create box.</source>
        <translation>创建保险箱失败。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="56"/>
        <source>Insufficient free space or unknown error, box creation failed.</source>
        <translation>可用空间不足或未知错误，保险箱创建失败。</translation>
    </message>
    <message>
        <source>Box not found!</source>
        <translation type="vanished">保险箱未找到！</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="59"/>
        <source>PrivateBox not found!</source>
        <translation>保险箱未找到！</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="62"/>
        <source>The box is exist!</source>
        <translation>保险箱已存在！</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="65"/>
        <source>The password set to the same as the current password is not supported.</source>
        <translation>新密码不能与当前密码相同。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="68"/>
        <source>Busy resources!</source>
        <translation>资源忙碌！</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="71"/>
        <source>Failed to change the password, please check whether the password is correct.</source>
        <translation>修改密码失败，请检查密码是否正确。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="75"/>
        <source>The password complexity does not meet the requirements.</source>
        <translation>不满足密码复杂度的要求！</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="78"/>
        <source>Password error!</source>
        <translation>密码错误！</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="81"/>
        <source>Passphrase error!</source>
        <translation>口令错误！</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="84"/>
        <source>Internal error!</source>
        <translation>内部错误！</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="87"/>
        <source>Invalid device.</source>
        <translation>无效的设备。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="90"/>
        <source>Invalid device permissions.</source>
        <translation>无效的设备权限。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="93"/>
        <source>Invalid device interface type.</source>
        <translation>无效的设备接口类型。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="96"/>
        <source>The graphics card does not support HDMI interface shutdown.</source>
        <translation>显卡不支持关闭HDMI接口。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="99"/>
        <source>Password error.</source>
        <translation>密码错误！</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="102"/>
        <source>This account has been freeze.</source>
        <translation>此用户已被冻结。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="105"/>
        <source>New password must be different from old password.</source>
        <translation>新密码必须不同于当前密码。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="108"/>
        <source>Permission denied.</source>
        <translation>权限拒绝。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="111"/>
        <source>Unknown account.</source>
        <translation>未知用户。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="114"/>
        <source>Failed to Change Multi-Factor authentication state.</source>
        <translation>修改多因子认证状态失败。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="117"/>
        <source>per page limit must less than 100 and page index must greater than 0.</source>
        <translation>每页数量必须小于 100 且页码必须大于 0 。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="120"/>
        <source>Failed to set selinux status.</source>
        <translation>设置 selinux 失败。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="123"/>
        <source>Failed to set mls Context, see log for more details.</source>
        <translation>设置安全上下文失败，详情请查看日志信息。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="126"/>
        <source>Failed to get mls Context, see log for more details.</source>
        <translation>获取安全上下文失败，详情请查看日志信息。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="129"/>
        <source>Failed to set kic Context, see log for more details.</source>
        <translation>设置完整性标签失败，详情请查看日志信息。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="132"/>
        <source>Failed to get kic Context, see log for more details.</source>
        <translation>获取完整性标签失败，详情请查看日志信息。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="135"/>
        <source>Failed to add obj to securitySign list, see log for more details.</source>
        <translation>添加对象至安全标记列表失败，详情请查看日志信息。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="138"/>
        <source>Failed to remove users, see log for more details.</source>
        <translation>删除用户失败，详情请查看日志信息。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="141"/>
        <source>Failed to shred files, see log for more details.</source>
        <translation>粉碎文件失败，详情请查看日志信息。</translation>
    </message>
    <message>
        <source>Failed to set Security Context.</source>
        <translation type="vanished">设置安全上下文失败。</translation>
    </message>
    <message>
        <source>Failed to get Security Context.</source>
        <translation type="vanished">获取安全上下文失败。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="145"/>
        <location filename="../lib/base/error.cpp" line="235"/>
        <source>Unknown error.</source>
        <translation>未知错误。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="164"/>
        <source>The standard type is invalid.</source>
        <translation>标准类型不合法。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="167"/>
        <source>The strategy type is invalid.</source>
        <translation>策略类型不合法。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="170"/>
        <source>The notification status is invalid.</source>
        <translation>通知状态不合法。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="173"/>
        <source>The resource monitor is invalid.</source>
        <translation>资源监控不合法。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="178"/>
        <source>Error format for reinforcement standard.</source>
        <translation>加固标准格式错误。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="181"/>
        <source>Reinforcement item &apos;{0}&apos; is not found.</source>
        <translation>加固项&apos;{0}&apos;未找到。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="185"/>
        <source>The job is running, please don&apos;t repeat the operation.</source>
        <translation>任务正在运行，请不要重复操作。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="188"/>
        <source>The fallback is can&apos;t running, please wait for the reinforcement to be completed.</source>
        <translation>回退暂无法运行，请在进行加固完成后操作。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="191"/>
        <source>The standard reinforcement configuration is not found.</source>
        <translation>加固标准配置未找到。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="194"/>
        <source>Machine code error.</source>
        <translation>机器码错误。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="197"/>
        <source>Activation code error.</source>
        <translation>激活码错误。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="200"/>
        <source>There is no historical state, please reinforce it and operation.</source>
        <translation>历史状态不存在，请在加固后进行操作。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="203"/>
        <source>The subsequest configuration item scan progress has been cancelled.</source>
        <translation>已取消后续配置项扫描进程。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="207"/>
        <source>The subsequest configuration item reinforcement progress has been cancelled.</source>
        <translation>已取消后续配置项加固进程。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="229"/>
        <source>Internel error.</source>
        <translation>内部错误。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="232"/>
        <source>The software is not activated.</source>
        <translation>软件未激活。</translation>
    </message>
    <message>
        <location filename="../lib/base/error.cpp" line="239"/>
        <source> (error code: 0x%1)</source>
        <translation> （错误码：0x%1）</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/utils.h" line="20"/>
        <source>Enable</source>
        <translation>启用</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/utils.h" line="21"/>
        <source>Disable</source>
        <translation>禁用</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/utils.h" line="22"/>
        <source>Unauthoried</source>
        <translation>未授权</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/utils.h" line="23"/>
        <source>Successful</source>
        <translation>成功</translation>
    </message>
    <message>
        <location filename="../src/ui/dm/utils.h" line="24"/>
        <source>Failed</source>
        <translation>失败</translation>
    </message>
    <message>
        <location filename="../src/ui/license/activation.cpp" line="27"/>
        <source>machine code</source>
        <translation>机器码</translation>
    </message>
    <message>
        <location filename="../src/ui/license/activation.cpp" line="28"/>
        <source>activation code</source>
        <translation>激活码</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/python/plugin-python.cpp" line="166"/>
        <source>The return value of %1 isn&apos;t tuple type</source>
        <translation>返回值 %1 不是个 tuple 类型</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/python/plugin-python.cpp" line="174"/>
        <source>The number of tuple returned by %1 is less than 2.</source>
        <translation>%1 返回的 tuple 编号小于2。</translation>
    </message>
    <message>
        <location filename="../src/daemon/br/python/plugin-python.cpp" line="191"/>
        <source>The type of tuple item returned by %1 is invalid.</source>
        <translation>%1 所返回的 tuple 类型是非法的。</translation>
    </message>
    <message>
        <location filename="../src/tool/config/cmd-parser.cpp" line="46"/>
        <source>The configuration file type</source>
        <translation>配置文件类型</translation>
    </message>
    <message>
        <location filename="../src/tool/config/cmd-parser.cpp" line="49"/>
        <source>The Operation method</source>
        <translation>操作方法</translation>
    </message>
    <message>
        <location filename="../src/tool/config/cmd-parser.cpp" line="52"/>
        <source>Specify the key or rule to get value</source>
        <translation>指定键或者规则用于获取对应的值</translation>
    </message>
    <message>
        <location filename="../src/tool/config/cmd-parser.cpp" line="55"/>
        <source>Specify the set value</source>
        <translation>指定设置的值</translation>
    </message>
    <message>
        <location filename="../src/tool/config/cmd-parser.cpp" line="58"/>
        <source>Specify regular expression to match the line. If many lines is matched, then the first matched line is used only</source>
        <translation>指定一个正则表达式用于匹配行。如果多行被匹配，则值由第一个被匹配的行被使用</translation>
    </message>
    <message>
        <location filename="../src/tool/config/cmd-parser.cpp" line="61"/>
        <source>Specify regular expression to split line</source>
        <translation>指定查找时用于划分列的正则表达式</translation>
    </message>
    <message>
        <location filename="../src/tool/config/cmd-parser.cpp" line="64"/>
        <source>Specify string for joining fields to line</source>
        <translation>指定用于拼接列的字符串</translation>
    </message>
    <message>
        <location filename="../src/tool/config/cmd-parser.cpp" line="67"/>
        <source>Specify comment string</source>
        <translation>指定注释字符串</translation>
    </message>
    <message>
        <location filename="../src/tool/config/cmd-parser.cpp" line="70"/>
        <source>Add new line when the speficied line pattern is dismatch in PAM</source>
        <translation>指定未匹配到模式行时添加的新行</translation>
    </message>
    <message>
        <location filename="../src/tool/config/cmd-parser.cpp" line="73"/>
        <source>Specifies a regular expression to match the next row of the inserted row. If multiple rows are matched, the value is used by the first matched row</source>
        <translation>指定一个正则表达式用于匹配行。如果多行被匹配，则值由第一个被匹配的行被使用</translation>
    </message>
    <message>
        <location filename="../src/tool/config/cmd-parser.cpp" line="75"/>
        <source>the configuration&apos;s path</source>
        <translation>配置文件的路径</translation>
    </message>
    <message>
        <location filename="../src/tool/config/cmd-parser.cpp" line="83"/>
        <source>The file path is not specified</source>
        <translation>文件路径未指定</translation>
    </message>
    <message>
        <location filename="../src/tool/config/cmd-parser.cpp" line="102"/>
        <source>No specify file type</source>
        <translation>文件类型未指定</translation>
    </message>
    <message>
        <location filename="../src/tool/config/cmd-parser.cpp" line="120"/>
        <source>Unknown file type</source>
        <translation>未知文件类型</translation>
    </message>
    <message>
        <location filename="../src/tool/config/cmd-parser.cpp" line="156"/>
        <location filename="../src/tool/config/cmd-parser.cpp" line="205"/>
        <location filename="../src/tool/config/cmd-parser.cpp" line="253"/>
        <source>Exec method %1 failed</source>
        <translation>执行方法 %1 失败</translation>
    </message>
    <message>
        <location filename="../src/tool/config/main.cpp" line="43"/>
        <source>Command &apos;ks-br-config&apos; can only be run as root!</source>
        <translation>命令 &apos;ks-br-config&apos; 只能在root用户下运行！</translation>
    </message>
    <message>
        <location filename="../src/tool/crypto/cmd-parser.cpp" line="43"/>
        <source>Generate public and private keys for RSA.</source>
        <translation>生成RSA公钥和私钥。</translation>
    </message>
    <message>
        <location filename="../src/tool/crypto/cmd-parser.cpp" line="45"/>
        <source>Decrypt a file.</source>
        <translation>解密文件。</translation>
    </message>
    <message>
        <location filename="../src/tool/crypto/cmd-parser.cpp" line="47"/>
        <source>Encrypt a file.</source>
        <translation>加密文件。</translation>
    </message>
    <message>
        <location filename="../src/tool/crypto/cmd-parser.cpp" line="49"/>
        <source>RSA public file path.</source>
        <translation>RSA公钥路径。</translation>
    </message>
    <message>
        <location filename="../src/tool/crypto/cmd-parser.cpp" line="51"/>
        <source>RSA private file path.</source>
        <translation>RSA私钥路径。</translation>
    </message>
    <message>
        <location filename="../src/tool/crypto/cmd-parser.cpp" line="53"/>
        <source>Output file path.</source>
        <translation>输出文件路径。</translation>
    </message>
    <message>
        <location filename="../src/ui/br/utils.cpp" line="60"/>
        <location filename="../src/ui/br/utils.cpp" line="100"/>
        <source>Unknown</source>
        <translation>未知</translation>
    </message>
    <message>
        <location filename="../src/ui/br/utils.cpp" line="63"/>
        <location filename="../src/ui/br/utils.cpp" line="94"/>
        <source>Conformity</source>
        <translation>符合</translation>
    </message>
    <message>
        <location filename="../src/ui/br/utils.cpp" line="66"/>
        <location filename="../src/ui/br/utils.cpp" line="96"/>
        <source>Inconformity</source>
        <translation>不符合</translation>
    </message>
    <message>
        <location filename="../src/ui/br/utils.cpp" line="69"/>
        <source>Not Scanned</source>
        <translation>未扫描</translation>
    </message>
    <message>
        <location filename="../src/ui/br/utils.cpp" line="72"/>
        <source>Scannig</source>
        <translation>正在扫描...</translation>
    </message>
    <message>
        <location filename="../src/ui/br/utils.cpp" line="75"/>
        <source>Scan Failed</source>
        <translation>扫描失败</translation>
    </message>
    <message>
        <location filename="../src/ui/br/utils.cpp" line="78"/>
        <source>Scan Complete</source>
        <translation>扫描完成</translation>
    </message>
    <message>
        <location filename="../src/ui/br/utils.cpp" line="81"/>
        <source>Unreinforcement</source>
        <translation>未加固</translation>
    </message>
    <message>
        <location filename="../src/ui/br/utils.cpp" line="84"/>
        <source>Reinforcing</source>
        <translation>正在加固...</translation>
    </message>
    <message>
        <location filename="../src/ui/br/utils.cpp" line="87"/>
        <source>Reinforcement Failure</source>
        <translation>加固失败</translation>
    </message>
    <message>
        <location filename="../src/ui/br/utils.cpp" line="90"/>
        <source>Reinforced</source>
        <translation>加固成功</translation>
    </message>
    <message>
        <location filename="../src/ui/br/utils.cpp" line="98"/>
        <source>Unscan</source>
        <translation>未扫描</translation>
    </message>
    <message>
        <location filename="../src/ui/common/table/pagination.cpp" line="73"/>
        <source>PREV</source>
        <translation>上一页</translation>
    </message>
    <message>
        <location filename="../src/ui/common/table/pagination.cpp" line="77"/>
        <source>NEXT</source>
        <translation>下一页</translation>
    </message>
</context>
<context>
    <name>QRCodeDialog</name>
    <message>
        <location filename="../src/ui/license/qrcode-dialog.ui" line="20"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_qrcode-dialog.h" line="80"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ReinforcementArgs</name>
    <message>
        <source>reset</source>
        <translation type="vanished">重置</translation>
    </message>
    <message>
        <source>ok</source>
        <translation type="vanished">确认</translation>
    </message>
    <message>
        <source>Set reinforment args</source>
        <translation type="vanished">加固参数设置</translation>
    </message>
    <message>
        <source>%1 not less than %2</source>
        <translation type="vanished">%1不小于%2</translation>
    </message>
    <message>
        <source>%1 input format is incorrect, please input the correct content according to the prompt</source>
        <translation type="vanished">%1输入格式不正确，请根据提示输入正确的内容</translation>
    </message>
</context>
<context>
    <name>ReinforcementArgsDialog</name>
    <message>
        <location filename="../src/ui/br/reinforcement-items/reinforcement-args-dialog.ui" line="14"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_reinforcement-args-dialog.h" line="101"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/br/reinforcement-items/reinforcement-args-dialog.ui" line="103"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_reinforcement-args-dialog.h" line="102"/>
        <source>reset</source>
        <translation>重置</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reinforcement-items/reinforcement-args-dialog.ui" line="122"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_reinforcement-args-dialog.h" line="103"/>
        <source>ok</source>
        <translation>确认</translation>
    </message>
</context>
<context>
    <name>RetrieveBoxPassword</name>
    <message>
        <source>Create security box</source>
        <translation type="vanished">创建保险箱</translation>
    </message>
    <message>
        <source>Please input passphrase:</source>
        <translation type="vanished">请输入口令：</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">确认</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
</context>
<context>
    <name>Scan</name>
    <message>
        <location filename="../src/ui/br/scan.ui" line="20"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_scan.h" line="63"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SidebarItem</name>
    <message>
        <location filename="../src/ui/sidebar.ui" line="26"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_sidebar.h" line="74"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/sidebar.ui" line="66"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_sidebar.h" line="76"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Table</name>
    <message>
        <location filename="../src/ui/br/reports/table.ui" line="97"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_table.h" line="183"/>
        <source>Safety Reinforcement Chart</source>
        <translation>安全加固图表</translation>
    </message>
</context>
<context>
    <name>TableDeleteNotify</name>
    <message>
        <source>ok</source>
        <translation type="vanished">确认</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation type="vanished">取消</translation>
    </message>
</context>
<context>
    <name>TrustedProtected</name>
    <message>
        <location filename="../src/ui/settings/trusted-protected.ui" line="20"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_trusted-protected.h" line="148"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/settings/trusted-protected.ui" line="37"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_trusted-protected.h" line="149"/>
        <source>Trusted protected(Effective after reboot):</source>
        <translation>可信保护 （重启后生效）：</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/trusted-protected.ui" line="70"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_trusted-protected.h" line="151"/>
        <source>Root key storage method:</source>
        <translation>根密钥存储方式：</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/trusted-protected.ui" line="109"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_trusted-protected.h" line="153"/>
        <source>Soft storage</source>
        <translation>软存储</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/trusted-protected.ui" line="151"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_trusted-protected.h" line="155"/>
        <source>Hard storage</source>
        <translation>硬存储</translation>
    </message>
</context>
<context>
    <name>TrustedUserPin</name>
    <message>
        <location filename="../src/ui/settings/trusted-user-pin.ui" line="14"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_trusted-user-pin.h" line="107"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/settings/trusted-user-pin.ui" line="68"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_trusted-user-pin.h" line="108"/>
        <source>Please input the pin code:</source>
        <translation>请输入pin码：</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/trusted-user-pin.ui" line="112"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_trusted-user-pin.h" line="109"/>
        <source>ok</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../src/ui/settings/trusted-user-pin.ui" line="131"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_trusted-user-pin.h" line="110"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>UserPromptDialog</name>
    <message>
        <location filename="../src/ui/common/user-prompt-dialog.ui" line="14"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_user-prompt-dialog.h" line="109"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/common/user-prompt-dialog.ui" line="65"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_user-prompt-dialog.h" line="110"/>
        <source>text</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/common/user-prompt-dialog.ui" line="114"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_user-prompt-dialog.h" line="111"/>
        <source>ok</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../src/ui/common/user-prompt-dialog.ui" line="133"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_user-prompt-dialog.h" line="112"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>Window</name>
    <message>
        <location filename="../src/ui/window.ui" line="20"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/src/ui/ui_window.h" line="119"/>
        <source>Security reinforcement</source>
        <translation>主机安全加固</translation>
    </message>
</context>
<context>
    <name>about</name>
    <message>
        <location filename="../src/ui/about.ui" line="14"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_about.h" line="93"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/ui/about.ui" line="72"/>
        <location filename="../src/ui/about.ui" line="88"/>
        <location filename="../src/ui/about.ui" line="104"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_about.h" line="95"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_about.h" line="96"/>
        <location filename="../build/src/ui/ks-ssr-gui_autogen/include/ui_about.h" line="97"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ini</name>
    <message>
        <location filename="../src/ui/br/utils.cpp" line="33"/>
        <source>configuration class</source>
        <translation>配置类</translation>
    </message>
    <message>
        <location filename="../src/ui/br/utils.cpp" line="34"/>
        <source>network class</source>
        <translation>网络类</translation>
    </message>
    <message>
        <location filename="../src/ui/br/utils.cpp" line="35"/>
        <source>audit class</source>
        <translation>审计类</translation>
    </message>
    <message>
        <location filename="../src/ui/br/utils.cpp" line="36"/>
        <source>external class</source>
        <translation>接入类</translation>
    </message>
</context>
<context>
    <name>python</name>
    <message>
        <location filename="../src/ui/br/reinforcement-items/plugins-translation.h" line="45"/>
        <source>Device busy, please pop up!</source>
        <translation>设备忙碌，请弹出设备！</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reinforcement-items/plugins-translation.h" line="46"/>
        <source>Please contact the admin.</source>
        <translation>请联系管理员。</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reinforcement-items/plugins-translation.h" line="47"/>
        <source>Unable to stop service!</source>
        <translation>服务无法停止！</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reinforcement-items/plugins-translation.h" line="48"/>
        <source>Abnormal service!</source>
        <translation>服务异常！</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reinforcement-items/plugins-translation.h" line="49"/>
        <source>Please close SELinux and use it!</source>
        <translation>请关闭selinux使用！</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reinforcement-items/plugins-translation.h" line="50"/>
        <source>No such file or directory.</source>
        <translation>文件或目录未找到。</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reinforcement-items/plugins-translation.h" line="51"/>
        <source>Failed to execute command. Please check the log information for details.</source>
        <translation>执行命令失败，详情请查看日志信息。</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reinforcement-items/plugins-translation.h" line="52"/>
        <source>UsePAM is not recommended to be closed,
which will cause many problems!</source>
        <translation>不建议关闭UsePAM，这将导致许多问题！</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reinforcement-items/plugins-translation.h" line="53"/>
        <source>Unable to stop firewalld service!</source>
        <translation>firewalld服务无法关闭！</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reinforcement-items/plugins-translation.h" line="54"/>
        <source>Unable to stop bluetooth service!</source>
        <translation>蓝牙服务无法关闭！</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reinforcement-items/plugins-translation.h" line="55"/>
        <source>Unable to stop cups service!</source>
        <translation>cups服务无法关闭！</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reinforcement-items/plugins-translation.h" line="56"/>
        <source>Unable to stop avahi service!</source>
        <translation>avahi服务无法关闭！</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reinforcement-items/plugins-translation.h" line="57"/>
        <source>Unable to stop rpcbind service!</source>
        <translation>rpcbind服务无法关闭！</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reinforcement-items/plugins-translation.h" line="58"/>
        <source>Unable to stop smb service!</source>
        <translation>smb服务无法关闭！</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reinforcement-items/plugins-translation.h" line="59"/>
        <source>sshd.services is not running!</source>
        <translation>sshd 后台服务没有运行!</translation>
    </message>
    <message>
        <location filename="../src/ui/br/reinforcement-items/plugins-translation.h" line="60"/>
        <source>Abnormal service! Please check the log information for details.</source>
        <translation>服务异常！详情请查看日志信息。</translation>
    </message>
</context>
<context>
    <name>xml</name>
    <message>
        <location filename="../plugins/python/br-plugin-network.xml.in" line="5"/>
        <source>network settings</source>
        <translation>网络设置</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-network.xml.in" line="10"/>
        <source>Firewall configuration</source>
        <translation>防火墙配置</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-network.xml.in" line="11"/>
        <source>Configure system firewall rules</source>
        <translation>配置系统防火墙规则</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-network.xml.in" line="15"/>
        <source>ICMP redirection</source>
        <translation>ICMP重定向</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-network.xml.in" line="16"/>
        <source>Protect against ICMP redirection attacks</source>
        <translation>防止受到ICMP重定向攻击</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-network.xml.in" line="20"/>
        <source>Syn flood attacks</source>
        <translation>Syn flood攻击</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-network.xml.in" line="21"/>
        <source>Protect against syn flood attacks</source>
        <translation>防止受到Syn flood攻击</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-network.xml.in" line="25"/>
        <source>IP source routing</source>
        <translation>IP源路由</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-network.xml.in" line="26"/>
        <source>Prevent source address spoofing</source>
        <translation>防止源地址欺骗</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-network.xml.in" line="30"/>
        <source>ICMP timestamp query</source>
        <translation>ICMP时间戳请求</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-network.xml.in" line="31"/>
        <source>Prevent attackers from attacking some time authentication based protocols</source>
        <translation>防止攻击者攻击一些基于时间认证的协议</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-network.xml.in" line="35"/>
        <source>Traceroute probe</source>
        <translation>Traceroute探测</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-network.xml.in" line="36"/>
        <source>Preventing threats to hosts detected by traceroute</source>
        <translation>防止主机被Traceroute探测威胁</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-network.xml.in" line="40"/>
        <source>High risk vulnerability</source>
        <translation>禁止高危漏洞服务</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-network.xml.in" line="41"/>
        <source>Disable high risk vulnerability services</source>
        <translation>预防高危漏洞服务的潜在危险</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="5"/>
        <source>external settings</source>
        <translation>接入设置</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="10"/>
        <source>CD-ROM device restrictions</source>
        <translation>光驱设备限制</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="11"/>
        <source>Restrict read and write operations of CD-ROM devices</source>
        <translation>限制光驱设备的读写操作</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="15"/>
        <source>USB storage device restrictions</source>
        <translation>USB存储设备限制</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="16"/>
        <source>Restrict read and write operations of USB storage devices</source>
        <translation>限制USB存储设备的读写操作</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="20"/>
        <source>Serial port device restrictions</source>
        <translation>串口设备限制</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="21"/>
        <source>Restrict read and write operations of Serial port devices</source>
        <translation>限制串口设备的读写操作</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="25"/>
        <source>User login limit</source>
        <translation>用户登录限制</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="26"/>
        <location filename="../data/br-system-rs.xml.in" line="1002"/>
        <source>Restrict system independent user login</source>
        <translation>限制系统无关用户的登录</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="30"/>
        <source>Null password detection</source>
        <translation>检测空密码</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="31"/>
        <source>Avoid null password user in the system</source>
        <translation>避免系统中存在空密码用户</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="35"/>
        <source>Delete redundant users</source>
        <translation>删除多余用户</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="36"/>
        <source>Delete lp, games, operator, adm by default and add and delete by user</source>
        <translation>默认删除多余用户与自定义删除的用户</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="40"/>
        <source>Remote login restrictions for root user</source>
        <translation>root用户远程登录限制</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="41"/>
        <source>Restrict remote login of root user through SSH connection</source>
        <translation>限制通过ssh远程登录root用户</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="45"/>
        <source>SSH password free login</source>
        <translation>ssh免密登录</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="46"/>
        <source>Password free login by storing public key</source>
        <translation>通过存放公钥的方式进行免密登录</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="50"/>
        <source>SSH weak encryption algorithm</source>
        <translation>ssh弱加密算法</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="51"/>
        <source>Enhance the security of SSH data transmission and prevent data from being cracked</source>
        <translation>增强ssh数据传输的安全性，防止数据被破解</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="55"/>
        <source>SSH login banner information</source>
        <translation>ssh登录Banner信息</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="56"/>
        <source>Avoid the leakage of operating system version information through SSH</source>
        <translation>避免操作系统版本信息通过ssh泄漏</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="60"/>
        <source>Sshd service reinforcement</source>
        <translation>sshd服务加固</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="61"/>
        <source>Restricted security protocol, empty password user login, SSH port, PAM</source>
        <translation>限制安全协议、空密码登录、ssh端口、pam登录</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="65"/>
        <source>su command limit</source>
        <translation>su命令限制</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="66"/>
        <location filename="../data/br-system-rs.xml.in" line="1187"/>
        <source>Users outside the wheel group are prohibited from using the &apos;su root&apos; command</source>
        <translation>禁止wheel组之外的用户使用su命令</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="70"/>
        <source>Sudo command permissions</source>
        <translation>sudo命令限制</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="71"/>
        <source>Restrict sudo command permissions</source>
        <translation>限制sudo命令使用权限</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="75"/>
        <source>Remote IP access restrictions</source>
        <translation>远程ip访问限制</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="76"/>
        <source>Set the remote IP that can and cannot access this machine</source>
        <translation>设置远程ip能够访问本机</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="80"/>
        <source>Session login timeout limit</source>
        <translation>会话登录超时限制</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="81"/>
        <source>If the session does not operate within the specified time after login, it will exit automatically</source>
        <translation>如果登录后在指定时间内，会话未操作将自动退出</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="85"/>
        <source>Limit SFTP</source>
        <translation>限制sftp</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="86"/>
        <source>Restrict SFTP users&apos; upward cross directory access</source>
        <translation>限制sftp用户向上跨目录访问</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="90"/>
        <location filename="../data/br-system-rs.xml.in" line="1276"/>
        <source>Disable radio network</source>
        <translation>禁用无线网络</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-external.xml.in" line="91"/>
        <source>Restrict wireless network connections</source>
        <translation>限制无线网络连接</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-config.xml.in" line="5"/>
        <source>config settings</source>
        <translation>配置设置</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-config.xml.in" line="10"/>
        <location filename="../data/br-system-rs.xml.in" line="336"/>
        <source>Turn on sendmail service</source>
        <translation>开启发送邮件服务</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-config.xml.in" line="11"/>
        <source>Avoid receiving malware via email and illegal attacks</source>
        <translation>防止通过电邮接收到恶意软件和受到非法攻击</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-config.xml.in" line="15"/>
        <location filename="../data/br-system-rs.xml.in" line="354"/>
        <source>Number of historical commands</source>
        <translation>历史命令条数</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-config.xml.in" line="16"/>
        <source>Restrict bash history commands</source>
        <translation>限制bash历史命令条数</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-config.xml.in" line="20"/>
        <source>Important profile permissions</source>
        <translation>重要文件权限</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-config.xml.in" line="21"/>
        <source>Permission restrictions on important configuration files in the system</source>
        <translation>系统内重要配置文件权限限制</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-config.xml.in" line="25"/>
        <source>Important directory permissions</source>
        <translation>重要目录权限</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-config.xml.in" line="26"/>
        <source>Restrict system key directory permissions</source>
        <translation>限制系统关键目录权限</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-config.xml.in" line="30"/>
        <location filename="../data/br-system-rs.xml.in" line="404"/>
        <source>Turn on composite key reboot</source>
        <translation>开启组合键重启</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-config.xml.in" line="31"/>
        <source>Set whether the system can be restarted through key combination</source>
        <translation>设置系统是否可以通过组合键进行重启</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-config.xml.in" line="35"/>
        <source>Login failure lock</source>
        <translation>登录失败锁定</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-config.xml.in" line="36"/>
        <source>Restrict the number of login failures and increase the cost of password cracking</source>
        <translation>限制登录失败次数，提高密码破解成本</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-config.xml.in" line="40"/>
        <source>Account password validity period</source>
        <translation>用户密码有效期</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-config.xml.in" line="41"/>
        <source>Restrict the maximum validity of passwords and add expiration reminders</source>
        <translation>限制密码的最大有效期限，添加过期提示</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-config.xml.in" line="45"/>
        <source>Password complexity</source>
        <translation>密码复杂度</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-config.xml.in" line="46"/>
        <source>Enhance password security and prevent passwords from being cracked(Suitable for new users and existing users changing passwords)</source>
        <translation>增强密码安全性，防止密码被破解（适用于新建用户和现有用户修改密码情形）</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-config.xml.in" line="50"/>
        <source>System critical resource constraints</source>
        <translation>系统关键资源限制</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-config.xml.in" line="51"/>
        <source>Restrict the size of system critical resources</source>
        <translation>限制系统关键资源大小</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-config.xml.in" line="55"/>
        <location filename="../data/br-system-rs.xml.in" line="723"/>
        <source>Turn on SAK key</source>
        <translation>开启SAK键</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-config.xml.in" line="56"/>
        <source>When the Linux system cannot normally respond to user requests, you can use SAK key to control Linux</source>
        <translation>当Linux系统不能正常响应用户请求时，可以使用魔术键控制Linux</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-config.xml.in" line="60"/>
        <source>Dmesg information</source>
        <translation>dmesg信息</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-config.xml.in" line="61"/>
        <source>Reinforce dmesg information</source>
        <translation>限制dmesg信息权限</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-config.xml.in" line="65"/>
        <location filename="../data/br-system-rs.xml.in" line="757"/>
        <source>File directory default permission</source>
        <translation>文件目录缺省权限</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-config.xml.in" line="66"/>
        <source>Restrict file directory default permissions</source>
        <translation>限制文件目录缺省权限</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-config.xml.in" line="70"/>
        <source>Scan for non owned master files</source>
        <translation>扫描无属主文件</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-config.xml.in" line="71"/>
        <source>Check whether there are files in the important directory that the user does not belong to</source>
        <translation>检查重要目录中的无属主文件</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-config.xml.in" line="75"/>
        <source>Scan 777 permission file</source>
        <translation>扫描777权限文件</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-config.xml.in" line="76"/>
        <source>Check whether there is a file with permission 777 under the important directory</source>
        <translation>检查重要目录中权限为777的文件</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-config.xml.in" line="80"/>
        <source>Scan suid-sgid permission file</source>
        <translation>扫描suid-sgid权限的文件</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-config.xml.in" line="81"/>
        <source>Check whether there is a file with suid-sgid permission under the important directory</source>
        <translation>检查重要目录中有suid-sgid权限的文件</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-config.xml.in" line="85"/>
        <source>Vulnerability scanning</source>
        <translation>漏洞扫描</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-config.xml.in" line="86"/>
        <source>Scanning system CVE vulnerability</source>
        <translation>扫描系统CVE漏洞</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-audit.xml.in" line="5"/>
        <source>audit settings</source>
        <translation>审计设置</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-audit.xml.in" line="10"/>
        <source>Auditd service</source>
        <translation>审计服务</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-audit.xml.in" line="11"/>
        <source>Write audit records to the log file. This includes recording system calls and file access. Administrator can check these logs to determine whether there are security vulnerabilities.</source>
        <translation>将审计记录写入日志文件，包括记录系统调用的文件访问，管理员可以检查这些日志，确认是否存在安全漏洞。</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-audit.xml.in" line="15"/>
        <source>Auditd rules</source>
        <translation>审计规则</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-audit.xml.in" line="16"/>
        <source>Audit records for changes in reading, writing, execution, and attributes.</source>
        <translation>审计读取、写入、执行和属性更改的记录</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-audit.xml.in" line="20"/>
        <source>Log save cycle</source>
        <translation>日志保存周期</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-audit.xml.in" line="21"/>
        <source>Log files are rotated before being removed</source>
        <translation>日志文件在被删除之前进行滚动</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-audit.xml.in" line="25"/>
        <source>Log file permissions</source>
        <translation>日志文件权限</translation>
    </message>
    <message>
        <location filename="../plugins/python/br-plugin-audit.xml.in" line="26"/>
        <source>Avoid changes to log files at will</source>
        <translation>避免随意改动日志文件</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="5"/>
        <source>Level I safety standard</source>
        <translation>一级安全标准</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="22"/>
        <location filename="../src/ui/br/utils.cpp" line="243"/>
        <source>Turn on ICMP redirection</source>
        <translation>开启ICMP重定向</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="38"/>
        <source>Prevent syn flood attacks</source>
        <translation>防止syn flood攻击</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="54"/>
        <source>Turn on IP source routing</source>
        <translation>开启ip源路由</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="70"/>
        <source>Allow ICMP timestamp query</source>
        <translation>允许ICMP时间戳请求</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="86"/>
        <source>Prohibit the host from being detected by traceroute</source>
        <translation>禁止主机被traceroute探测</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="102"/>
        <source>Turn on system firewall service</source>
        <translation>开启系统防火墙服务</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="116"/>
        <source>transport protocol</source>
        <translation>传输协议</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="129"/>
        <source>Disable ports that may threaten the system by default</source>
        <translation>默认禁用可能威胁系统的端口</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="142"/>
        <source>clear iptables configuration</source>
        <translation>清空iptables配置</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="144"/>
        <source>If this configuration is enabled, all firewall configurations will be cleared, and the firewall rules reinforced this time will not take effect</source>
        <translation>此条配置开启，将清空所有防火墙配置，此次加固防火墙规则不生效</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="153"/>
        <source>allow network segment (Split with comma)</source>
        <translation>允许输入网段（逗号分隔）</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="163"/>
        <source>disable network segment (Split with comma)</source>
        <translation>禁止输入网段（逗号分隔）</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="173"/>
        <source>Opened ports (Split with semicolon)</source>
        <translation>开放端口（分号分隔）</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="183"/>
        <source>disable ports (Split with semicolon)</source>
        <translation>禁用端口（分号分隔）</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="198"/>
        <source>Limit input port maximum connections(0 means unlimited)</source>
        <translation>限制输入端口最大连接数（0为不限制）</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="208"/>
        <source>allow output network segment (Split with comma)</source>
        <translation>允许输出网段（逗号分隔）</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="218"/>
        <source>disable output network segment (Split with comma)</source>
        <translation>禁止输出网段（逗号分隔）</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="228"/>
        <source>Opened output ports (Split with semicolon)</source>
        <translation>开放输出端口（分号分隔）</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="238"/>
        <source>disable output ports (Split with semicolon)</source>
        <translation>禁用输出端口（分号分隔）</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="251"/>
        <source>disable icmp ping</source>
        <translation>ICMP禁用ping传入请求</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="267"/>
        <source>Turn on system bluetooth service</source>
        <translation>开启系统蓝牙服务</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="280"/>
        <source>Turn on system cups service</source>
        <translation>开启系统打印服务</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="293"/>
        <source>Turn on system avahi-daemon service</source>
        <translation>开启系统avahi-daemon服务</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="306"/>
        <source>Turn on system rpcbind service</source>
        <translation>开启系统RPC端口映射功能</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="319"/>
        <source>Turn on system smb service</source>
        <translation>开启Web连接和客户与服务器之间的信息沟通功能</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="370"/>
        <source>The maximum permissions of important configuration files in the system is set to 0644</source>
        <translation>系统中重要配置文件的最大权限设置为0644</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="372"/>
        <source>The maximum permissions for configuration files /etc/passwd, /etc/bashrc/etc/shadow, /etc/group, /etc/security/limits.conf, /etc/pam.d/system-auth-ac, /etc/fstab are set to 0644</source>
        <translation>配置文件/etc/passwd, /etc/bashrc, /etc/shadow, /etc/group, /etc/security/limits.conf, /etc/pam.d/system-auth-ac, /etc/fstab的最大权限设置为0644</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="387"/>
        <source>The maximum permission of key directories in the system is 0755</source>
        <translation>系统中目录的权限设置为0755</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="389"/>
        <source>The permissions for the/usr,/usr/bin,/usr/sbin,/usr/lib,/usr/lib64,/etc,/var directories in the system are set to 0755</source>
        <translation>系统中/usr,/usr/bin,/usr/sbin,/usr/lib,/usr/lib64,/etc,/var目录权限设置为0755</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="406"/>
        <source>The restart function is applied to the non graphical interface, and in the graphical interface, it acts as shutdown, and the function takes effect after logging out of the user.</source>
        <translation>重启功能作用于非图形界面，在图形界面中作用为关机，且功能需在注销用户后生效。</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="423"/>
        <source>Password error times</source>
        <translation>密码错误次数</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="438"/>
        <source>Automatic unlocking time after locking (Sec)</source>
        <translation>非root用户锁定后自动解锁时间（秒）</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="451"/>
        <source>Root login failed lock</source>
        <translation>root用户登录失败锁定</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="466"/>
        <source>Unlock time after root user locks (Sec)</source>
        <translation>root用户锁定后自动解锁时间（秒）</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="484"/>
        <source>Maximum number of valid days for new users to use passwords</source>
        <translation>新用户可使用密码的最大有效天数</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="499"/>
        <source>Minimum number of days allowed between password changes</source>
        <translation>密码允许更改的最小间隔天数</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="514"/>
        <source>Minimum acceptable password length</source>
        <translation>可接受的最小密码长度</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="529"/>
        <source>Number of days warning given before a password expires</source>
        <translation>密码过期前发出警告的天数</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="542"/>
        <source>Whether to set the expiration time for the current existing account</source>
        <translation>是否设置现有用户的到期时间（三权及root用户除外）</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="557"/>
        <source>Existing users set expiration days</source>
        <translation>现有用户设置过期天数</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="575"/>
        <source>Minimum password length</source>
        <translation>最小密码长度</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="590"/>
        <source>Number of capital letters</source>
        <translation>大写字母个数</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="605"/>
        <source>Number of lowercase letters</source>
        <translation>小写字母个数</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="620"/>
        <source>Number of digits</source>
        <translation>数字个数</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="635"/>
        <source>Number of special characters</source>
        <translation>特殊字符个数</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="650"/>
        <source>Contains number of character types (special characters, numbers, letters)</source>
        <translation>包含字符类型数（特殊字符、数字、字母）</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="665"/>
        <source>Maximum number of consecutive characters</source>
        <translation>最大连续字符个数</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="678"/>
        <source>Allow user names</source>
        <translation>允许包含用户名</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="691"/>
        <source>Enable dictionary check</source>
        <translation>启用字典检查</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="707"/>
        <source>Stack and RSS sizes are limited (10240)</source>
        <translation>限制stack和rss的大小（10240）</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="739"/>
        <source>Allow administrators to view dmesg only</source>
        <translation>仅允许管理员查看dmesg</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="773"/>
        <source>No master file in the scanning system</source>
        <translation>扫描系统中的无属主文件</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="775"/>
        <source>Check if there are files in the /bin,/usr/bin,/sbin,/usr/sbin directories that do not belong to the user</source>
        <translation>扫描/bin,/usr/bin,/sbin,/usr/sbin关键目录中的无属主文件</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="790"/>
        <source>Scanning system files with 777 permissions</source>
        <translation>扫描系统中权限为777的文件</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="792"/>
        <source>Scan files with permissions of 777 in the /bin,/usr/bin,/sbin,/usr/sbin directory</source>
        <translation>扫描/bin,/usr/bin,/sbin,/usr/sbin关键目录中权限为777的文件</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="807"/>
        <source>Scan system important directory suid-sgid file</source>
        <translation>扫描系统重要目录中有suid-sgid权限的文件</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="809"/>
        <source>Scan files with SUID SGID permissions in the/bin,/usr/bin,/sbin, and/usr/sbin directories of the system</source>
        <translation>扫描系统/bin,/usr/bin,/sbin, /usr/sbin目录中具有suid-sgid权限的文件</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="824"/>
        <source>Built in common vulnerability version version number scanning</source>
        <translation>内置常见漏洞版本号扫描</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="837"/>
        <source>contents</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="854"/>
        <source>Turn on system auditd service</source>
        <translation>开启系统审计服务</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="867"/>
        <source>Add audit rules to path.</source>
        <translation>添加审计规则</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="877"/>
        <source>Delete audit rules to path.</source>
        <translation>删除审计规则</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="890"/>
        <source>Delete all audit rules</source>
        <translation>删除所有审计规则</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="908"/>
        <source>System log save cycle (weekly)</source>
        <translation>系统日志保存周期（周）</translation>
    </message>
    <message>
        <source>System log save cycle weekly</source>
        <translation type="vanished">系统日志保存周期（周）</translation>
    </message>
    <message>
        <source>System log save cycle(weekly)</source>
        <translation type="vanished">系统日志保存周期（周）</translation>
    </message>
    <message>
        <source>System log save cycle（weekly）</source>
        <translation type="vanished">系统日志保存周期（周）</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="924"/>
        <source>The maximum permission of log files and configurations is set to 0644</source>
        <translation>日志文件和配置的最大权限为0644</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="937"/>
        <source>Log files can only be appended</source>
        <translation>日志仅能被添加</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="954"/>
        <source>Allow to use CD-ROM device</source>
        <translation>允许使用光驱设备（首次加固时间长，请耐心等待）</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="970"/>
        <source>Allow to use USB device</source>
        <translation>允许使用USB设备</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="986"/>
        <source>Allow to use serial port device</source>
        <translation>允许使用串口设备</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="1012"/>
        <source>Allow login to account (separated by semicolons)</source>
        <translation>允许登录账号（分号分隔）</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="1028"/>
        <source>Disable null password user present</source>
        <translation>禁止空密码用户出现</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="1044"/>
        <source>Delete lp, games, operator and adm by default</source>
        <translation>默认删除lp，games，operator，adm</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="1054"/>
        <source>Add users to delete (Split with semicolon)</source>
        <translation>添加需删除用户（分号分隔）</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="1070"/>
        <source>Rrohibit remote login of root user via SSH</source>
        <translation>禁止通过ssh远程登录root用户</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="1086"/>
        <source>Allow SSH password free login</source>
        <translation>允许ssh免密登录</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="1102"/>
        <source>Allow to use SSH weak encryption algorithm</source>
        <translation>允许ssh弱加密算法</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="1119"/>
        <source>Hiding operating system version information</source>
        <translation>隐藏操作系统版本信息</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="1135"/>
        <source>Enforce V2 security protocol</source>
        <translation>强制使用V2安全协议</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="1148"/>
        <source>Prohibit SSH users with empty password from logging in</source>
        <translation>禁止主机密码为空的用户ssh登录</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="1158"/>
        <source>Modify the default SSH port</source>
        <translation>修改默认ssh端口</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="1171"/>
        <source>Enable PAM login mechanism</source>
        <translation>启用pam登录机制</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="1203"/>
        <source>Restrict user sudo command permission</source>
        <translation>限制用户sudo命令使用权限（开启selinux失效）</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="1216"/>
        <source>List of IP addresses that allow remote access (Split with comma)</source>
        <translation>允许远程访问的ip列表（逗号分隔）</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="1226"/>
        <source>List of IP addresses for which remote access is prohibited (Split with comma)</source>
        <translation>禁止远程访问的ip列表（逗号分隔）</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="1244"/>
        <source>If no operation is performed for more than the following time (seconds), the session exits</source>
        <translation>超过以下时间（秒）未操作则会话退出</translation>
    </message>
    <message>
        <location filename="../data/br-system-rs.xml.in" line="1260"/>
        <source>Whether to configure sftpuser special account</source>
        <translation>是否配置sftpuser用户（请自行设置密码）</translation>
    </message>
    <message>
        <source>configuration class</source>
        <translation type="vanished">配置类</translation>
    </message>
    <message>
        <source>network class</source>
        <translation type="vanished">网络类</translation>
    </message>
    <message>
        <source>audit class</source>
        <translation type="vanished">审计类</translation>
    </message>
    <message>
        <source>external class</source>
        <translation type="vanished">接入类</translation>
    </message>
</context>
</TS>
