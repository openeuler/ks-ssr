# -*- coding: utf-8 -*-

import json
import br.configuration
import br.utils
import os

SYSCTL_PATH = '/usr/sbin/sysctl'
# 修改schemas默认值
SCHEMAS_CONF_FILEPATH = "/usr/share/glib-2.0/schemas/98-br-config.gschema.override"
RELOAD_SCHEMAS_CMD = "glib-compile-schemas /usr/share/glib-2.0/schemas"

MODIFY_RULE_CLOSE = "[org.mate.SettingsDaemon.plugins.media-keys]\npower=\'\'"
MODIFY_RULE_OPEN = "[org.mate.SettingsDaemon.plugins.media-keys]\npower=\'<Control><Alt>Delete\'"

SAK_KEY_SWITCH_CONF_FILE = "/etc/sysctl.d/90-br-config.conf"
SAK_KEY_SWITCH_CONF_SYS_FILE = "/etc/sysctl.conf"
SAK_KEY_SWITCH_CONF_KEY_SYSRQ = "kernel.sysrq"
DMESG_SWITCH_CONF_KEY_SYSRQ = "kernel.dmesg_restrict"

COMPOSITE_KEY_REBOOT_STATUS_CMD = "systemctl   status  ctrl-alt-del.target"
COMPOSITE_KEY_REBOOT_DISABLE_CMD = "systemctl   mask   ctrl-alt-del.target"
COMPOSITE_KEY_REBOOT_ENABLE_CMD = "systemctl   unmask   ctrl-alt-del.target"


class SAKKey:
    def __init__(self):
        self.conf = br.configuration.KV(SAK_KEY_SWITCH_CONF_FILE, "=", "=")
        self.conf_sys = br.configuration.KV(
            SAK_KEY_SWITCH_CONF_SYS_FILE, "=", "=")

    def get(self):
        retdata = dict()
        value = self.conf.get_value(SAK_KEY_SWITCH_CONF_KEY_SYSRQ)
        sys_value = self.conf_sys.get_value(SAK_KEY_SWITCH_CONF_KEY_SYSRQ)
        if sys_value:
            value = sys_value
        retdata[SAK_KEY_SWITCH_CONF_KEY_SYSRQ] = "" if not value else value == "1"
        return (True, json.dumps(retdata))

    def set(self, args_json):
        args = json.loads(args_json)
        value = ""
        if str(args[SAK_KEY_SWITCH_CONF_KEY_SYSRQ]):
            value = "1" if bool(args[SAK_KEY_SWITCH_CONF_KEY_SYSRQ]) else "0"
        self.conf.set_value(SAK_KEY_SWITCH_CONF_KEY_SYSRQ, value)
        if len(self.conf_sys.get_value(SAK_KEY_SWITCH_CONF_KEY_SYSRQ)) != 0:
            self.conf_sys.set_value(SAK_KEY_SWITCH_CONF_KEY_SYSRQ, value)
        br.utils.subprocess_not_output('{0} --system'.format(SYSCTL_PATH))

        return (True, '')


class Dmesg:
    def __init__(self):
        self.conf = br.configuration.KV(SAK_KEY_SWITCH_CONF_FILE, "=", "=")
        self.conf_sys = br.configuration.KV(SAK_KEY_SWITCH_CONF_SYS_FILE, "=", "=")

    def get(self):
        retdata = dict()
        dmesg_value = self.conf.get_value(DMESG_SWITCH_CONF_KEY_SYSRQ)
        sys_value = self.conf_sys.get_value(DMESG_SWITCH_CONF_KEY_SYSRQ)
        if sys_value:
            dmesg_value = sys_value
        retdata[DMESG_SWITCH_CONF_KEY_SYSRQ] = "" if not dmesg_value else dmesg_value == "1"
        return (True, json.dumps(retdata))

    def set(self, args_json):
        args = json.loads(args_json)
        value = ""
        if str(args[DMESG_SWITCH_CONF_KEY_SYSRQ]):
            value = "1" if bool(args[DMESG_SWITCH_CONF_KEY_SYSRQ]) else "0"
        
        self.conf.set_value(DMESG_SWITCH_CONF_KEY_SYSRQ, value)
        if len(self.conf_sys.get_value(DMESG_SWITCH_CONF_KEY_SYSRQ)) != 0:
            self.conf_sys.set_value(DMESG_SWITCH_CONF_KEY_SYSRQ, value)
        br.utils.subprocess_not_output('{0} --system'.format(SYSCTL_PATH))

        return (True, '')


class KeyRebootSwitch:
    def __init__(self):
        self.conf = br.configuration.Table(SCHEMAS_CONF_FILEPATH, ",\\s+")

    def reload_schemas(self):
        cmd = '{0}'.format(RELOAD_SCHEMAS_CMD)
        br.utils.subprocess_not_output(cmd)

    # 判断文件是否存在
    def status_exist(self):
        command = "ls /usr/lib/systemd/system/ |grep -wx ctrl-alt-del.target"
        cmd = '{0}'.format(command)
        output = br.utils.subprocess_has_output(cmd)
        return len(output) != 0

    def status(self):
        command = '{0} | grep masked'.format(COMPOSITE_KEY_REBOOT_STATUS_CMD)
        output = br.utils.subprocess_has_output(command)
        return len(output) == 0

    # 判断.bak是否存在
    def status_bak(self):
        command = " ls /usr/lib/systemd/system/ |grep ctrl-alt-del.target.bak"
        cmd = '{0}'.format(command)
        output = br.utils.subprocess_has_output(cmd)
        return len(output) != 0

    def open(self):
        command = '{0}'.format(COMPOSITE_KEY_REBOOT_ENABLE_CMD)
        br.utils.subprocess_not_output(command)
        if not os.path.exists('/etc/systemd/system/ctrl-alt-del.target'):
            br.utils.subprocess_not_output(
                "ln -s /usr/lib/systemd/system/reboot.target /etc/systemd/system/ctrl-alt-del.target")
        rm_cmd = 'rm -rf {0}'.format(SCHEMAS_CONF_FILEPATH)
        br.utils.subprocess_not_output(rm_cmd)
        self.conf.set_value("1=[org.mate.SettingsDaemon.plugins.media-keys]\npower=\'\'", MODIFY_RULE_OPEN)
        self.reload_schemas()

    def close(self):
        if os.path.exists('/etc/systemd/system/ctrl-alt-del.target'):
            br.utils.subprocess_not_output(
                "rm -rf /etc/systemd/system/ctrl-alt-del.target")
        command = '{0}'.format(COMPOSITE_KEY_REBOOT_DISABLE_CMD)
        br.utils.subprocess_not_output(command)
        rm_cmd = 'rm -rf {0}'.format(SCHEMAS_CONF_FILEPATH)
        br.utils.subprocess_not_output(rm_cmd)
        self.conf.set_value("1=[org.mate.SettingsDaemon.plugins.media-keys]\npower=\'<Control><Alt>Delete\'", MODIFY_RULE_CLOSE)
        self.reload_schemas()

    def get(self):
        retdata = dict()
        if self.status_exist():
            retdata['enabled'] = self.status()
        if self.status_bak():
            retdata['enabled'] = False
        return (True, json.dumps(retdata))

    def set(self, args_json):
        args = json.loads(args_json)
        if self.status_exist():
            if not self.status() and args['enabled']:
                self.open()
            else:
                if args['enabled']:
                    self.open()
                else:
                    self.close()
        else:
            # 针对3.3-6的处理规则，文件不存在，开关为打开是，将.bak改为ctrl-alt-del.target
            if args['enabled'] and self.status_bak():
                command = "mv /usr/lib/systemd/system/ctrl-alt-del.target.bak /usr/lib/systemd/system/ctrl-alt-del.target"
                br.utils.subprocess_not_output(command)

        return (True, '')
