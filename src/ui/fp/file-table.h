/**
 * Copyright (c) 2023 ~ 2024 KylinSec Co., Ltd.
 * ks-ssr is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 *
 * Author:     tangjie02 <tangjie02@kylinos.com.cn>
 */

#pragma once

#include <QAbstractTableModel>
#include <QList>
#include <QSortFilterProxyModel>
#include <QStyledItemDelegate>
#include <QTableView>
#include <QWidget>
#include "src/ui/common/table/table-header-proxy.h"

class KSSDbusProxy;

namespace KS
{
namespace FP
{
struct FPFileInfo
{
    // 是否被选中
    bool selected;
    // 文件名
    QString fileName;
    // 文件路径
    QString filePath;
    // 添加事件
    QString addTime;
};

class FilesDelegate : public QStyledItemDelegate
{
    Q_OBJECT

public:
    FilesDelegate(QObject *parent = 0);
    virtual ~FilesDelegate();

    // QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    bool editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index) override;
};

class FilesFilterModel : public QSortFilterProxyModel
{
    Q_OBJECT

public:
    FilesFilterModel(QObject *parent = nullptr);
    virtual ~FilesFilterModel(){};

protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;
};

class FilesModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    FilesModel(QObject *parent = nullptr);
    virtual ~FilesModel(){};

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;
    //    bool setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role = Qt::EditRole) override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;

    QList<FPFileInfo> getFPFileInfos();

signals:
    void stateChanged(Qt::CheckState checkState);
    void filesUpdate(int total);

private:
    void checkSelectStatus();
    void updateRecord();

private:
    KSSDbusProxy *m_fileProtectedProxy;
    QList<FPFileInfo> m_filesInfo;
};

class FileTable : public QTableView
{
    Q_OBJECT

public:
    FileTable(QWidget *parent = nullptr);
    virtual ~FileTable(){};

    FilesFilterModel *getFilterProxy()
    {
        return m_filterProxy;
    };
    void searchTextChanged(const QString &text);
    QList<FPFileInfo> getFPFileInfos();

private:
    void mouseEnter(const QModelIndex &index);

signals:
    void filesUpdate(int total);

private slots:
    void checkedAllItem(Qt::CheckState checkState);

private:
    FilesFilterModel *m_filterProxy;
    FilesModel *m_model;
    TableHeaderProxy *m_headerViewProxy;
};
}  // namespace FP
}  // namespace KS
