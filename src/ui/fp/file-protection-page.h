/**
 * Copyright (c) 2023 ~ 2024 KylinSec Co., Ltd.
 * ks-ssr is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *          http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 *
 * Author:     tangjie02 <tangjie02@kylinos.com.cn>
 */

#pragma once

#include <QWidget>
#include "src/ui/common/page.h"

class KSSDbusProxy;

namespace Ui
{
class FileProtectionPage;
}  // namespace Ui

namespace KS
{
namespace FP
{
class FileProtectionPage : public Page
{
    Q_OBJECT
public:
    FileProtectionPage(QWidget *parent = nullptr);
    virtual ~FileProtectionPage();

    QString getNavigationUID() override;
    QString getSidebarUID() override;
    QString getSidebarIcon() override;
    QString getAccountRoleName() override;

private:
    bool checkTrustedLoadFinied(bool initialized);
    bool isExistSelectedItem();

private Q_SLOTS:
    void searchTextChanged(const QString &text);
    void addProtectedFiles(bool checked);
    void popDeleteNotify(bool checked);
    void removeProtectedFiles();
    void updateTips(int total);

private:
    Ui::FileProtectionPage *m_ui;

    KSSDbusProxy *m_fileProtectedProxy;
};
}  // namespace FP
}  // namespace KS
